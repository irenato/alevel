<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 05.02.17
 * Time: 10:56
 */

?>

<div id="modal-will-be-partners" class="modal-visible">
    <form id="form-will-be-partners" class="modal-block" action="">
        <input type="text" name="partner-name" placeholder="Контактное лицо ФИО *">
        <input type="text" name="company-name" placeholder="Организация *">
        <input type="text" class="phone-masked" name="partner-phone" placeholder="Телефон *">
        <input type="email" name="partner-mail" placeholder="E-Mail *">
        <div style="display: none">
            <input type="text" name="partner-city" placeholder="E-Mail *">
        </div>
        <input type="submit" value="Отправить" id="sbm-partner">
        <p>
            *Вы принимаете политику конфиденциальности. Ваш адрес электронной почты и ФИО ни в коем случае не
            будут переданы другим пользователям и никогда не будут открыты для просмотра. Мы не будем передавать
            эти данные третьим лицам и/или любым другим способом сообщать эти данные третьим лицам.
        </p>
    </form>
</div>
