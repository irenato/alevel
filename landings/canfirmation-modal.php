<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 04.02.17
 * Time: 16:31
 */

?>

<div id="modal-thanks-for-register">
    <div class="modal-block">
        <p>Добро пожаловать в мир IT!</p>
        <img src="<?= get_template_directory_uri() ?>/images/landing/thanks-modal.png" alt="A-level">
        <p id="confirmation-main-text">
            Подтвердите свое участие, перейдя по ссылке из письма, которое только что ушло к вам на почту
        </p>
        <p id="confirmation-sub-text">Расскажите о дне открытых дверей друзьям: </p>
        <ul>
            <li>
                <a href="<?= get_field('link_vk') ?>" target="_blank">
                    <img src="<?= get_template_directory_uri() ?>/images/landing/vk.png" alt="A-level Social links">
                </a>
            </li>
            <?php if (!empty(get_field('link_google'))): ?>
                <li>
                    <a href="<?= get_field('link_google') ?>" target="_blank">
                        <img src="<?= get_template_directory_uri() ?>/images/landing/google.png" alt="A-level Social links">
                    </a>
                </li>
            <?php endif; ?>
            <?php if (!empty(get_field('link_linked'))): ?>
            <li>
                <a href="<?= get_field('link_linked') ?>" target="_blank">
                    <img src="<?= get_template_directory_uri() ?>/images/landing/linkedin.png" alt="A-level Social links">
                </a>
            </li>
            <?php endif; ?>
            <li>
                <a href="<?= get_field('link_fb') ?>" target="_blank">
                    <img src="<?= get_template_directory_uri() ?>/images/landing/fb.png" alt="A-level Social links">
                </a>
            </li>
        </ul>
        <p id="confirmation-ps"><strong>P.S.</strong> Если письмо еще не пришло, повторите <a href="<?= get_page_link() ?>#register">регистрацию</a>.
        </p>
    </div>
</div>