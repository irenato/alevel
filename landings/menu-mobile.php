<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 06.02.17
 * Time: 22:00
 */

?>

<div id="modal-menu-mobile">
    <li><a href="<?= get_page_link() ?>#purpose">Цель</a></li>
    <li><a href="<?= get_page_link() ?>#why-we">Плюшки</a></li>
    <li><a href="<?= get_page_link() ?>#program">Программа</a></li>
    <li><a href="<?= get_page_link() ?>#partners">Партнеры</a></li>
    <li><a href="<?= get_page_link() ?>#contacts">Контакты</a></li>
</div>
