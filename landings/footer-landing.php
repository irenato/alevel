<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 03.02.17
 * Time: 1:19
 */

?>

<footer id="contacts">
    <div class="wrapper">
        <div class="footer-contacts">
            <p>Наши контакты</p>
            <ul>
                <?php if (get_field('link_vk')): ?>
                    <li><a href="<?= get_field('link_vk') ?>" target="_blank">VK</a></li>
                <?php endif; ?>
                <?php if (get_field('link_fb')): ?>
                    <li><a href="<?= get_field('link_fb') ?>" target="_blank">FB</a></li>
                <?php endif; ?>
            </ul>
            <?php if (get_field('address_buiding')): ?>
                <p><?= get_field('address_buiding') ?></p>
            <?php endif; ?>
        </div>
        <div class="ask-questions">
            <p>По вопросам:</p>
            <?php if (get_field('event_admin_name')): ?>
                <p><?= get_field('event_admin_name') ?></p>
            <?php endif; ?>
            <p>
                <a href="tel:<?= str_replace(array('(', ')', '-', ' '), '', get_field('event_phone_number')) ?>"><?= get_field('event_phone_number') ?></a>
            </p>
            <?php if (get_field('event_skype_pr')): ?>
                <p>Skype: <a href="skype:<?= get_field('event_skype_pr') ?>"><?= get_field('event_skype_pr') ?></a></p>
            <?php endif; ?>
        </div>
        <div class="links">
            <?php if (get_field('event_email')): ?>
                <p><a href="mailto:<?= get_field('event_email') ?>"><?= get_field('event_email') ?></a></p>
            <?php endif; ?>
            <?php if (get_field('event_link')): ?>
                <p><a href="<?= get_field('event_link') ?>"><?= get_field('event_link') ?></a></p>
            <?php endif; ?>
        </div>
        <?php if (get_field('address_image')): ?>
            <img src="<?= get_field('address_image') ?>" alt="<?= get_field('address_buiding') ?>">
        <?php endif; ?>
        <div class="circle lg"></div>
        <div class="circle lg2"></div>
    </div>
    <?php wp_footer(); ?>
</footer>
<div class="hidden">
    <?php get_template_part('landings/modal-for-partners'); ?>
    <?php get_template_part('landings/canfirmation-modal'); ?>
    <?php get_template_part('landings/menu-mobile'); ?>
</div>
</body>
</html>