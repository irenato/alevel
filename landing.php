<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 03.02.17
 * Time: 0:25
 */
/*
* Template name: landing
*/

get_header();
?>

    <header>
        <div class="wrapper">
            <ul>
                <li>
                    <a href="<?= get_home_url() ?>">
                        <img src="<?= get_template_directory_uri() ?>/images/landing/logo.png" alt="A-level">
                    </a>
                </li>
                <li><a href="<?= get_page_link() ?>#purpose">Цель</a></li>
                <li><a href="<?= get_page_link() ?>#why-we">Плюшки</a></li>
                <li><a href="<?= get_page_link() ?>#program">Ведущие</a></li>
                <li><a href="<?= get_page_link() ?>#partners">Партнеры</a></li>
                <li><a href="<?= get_page_link() ?>#contacts">Контакты</a></li>
            </ul>
            <ul class="mobile-header">
                <li><a href="/"><img src="<?= get_template_directory_uri() ?>/images/landing/logo.png"
                                     alt="A-level"></a></li>
                <li id="main-menu-mobile"></li>
            </ul>
        </div>
    </header>

    <section class="top-block">
        <div class="wrapper">
            <div class="circle lg">
                <h1><?= get_the_title() ?></h1>
                <?php include_once('settings/months.php');
                $date = explode('.', get_field('event_date'));
                ?>
                <p><?= $date[2] ?> <?= $months[$date[1] * 1] ?>, <?= get_field('event_begin') ?></p>
                <a href="<?= get_page_link() ?>#register" class="button" id="register-button">регистрация</a>
            </div>
            <img src="<?= get_template_directory_uri() ?>/images/landing/top-block-baner.png" alt="A-level">
        </div>
    </section>

    <section class="purpose" id="purpose">
        <div class="wrapper">
            <div class="description">
                <h1><?= get_field('event_target') ?></h1>

                <?php $descriptions = get_field('event_target_description'); ?>
                <?php foreach ($descriptions as $description): ?>
                    <p>
                        <?= $description['descripton'] ?>
                    </p>
                <?php endforeach; ?>
            </div>
            <?php $images = get_field('event_target_images'); ?>
            <?php foreach ($images as $image): ?>
                <img src="<?= $image['image'] ?>" alt="A-Level">
            <?php endforeach; ?>
        </div>
    </section>

    <section class="video">
        <div class="wrapper">
            <video id="videoPlayer" controls autoplay muted loop="loop">
                <source src="<?= get_template_directory_uri() ?>/images/landing/video.mp4" type="video/mp4">
            </video>
            <div class="video-front">
                <p>Видеоотчет <br/> с последнего ивента</p>
                <button id="play-video"><img src="<?= get_template_directory_uri() ?>/images/landing/triangula.png"
                                             alt="play"></button>
            </div>
        </div>
    </section>

    <section class="why-we" id="why-we">
        <div class="wrapper">
            <ul>
                <?php $advantages = get_field('cookies_el'); ?>
                <?php foreach ($advantages as $advantage): ?>
                    <li>
                        <img src="<?= $advantage['image'] ?>" alt="<?= $advantage['alt'] ?>">
                        <p><?= $advantage['description'] ?></p>
                    </li>
                <?php endforeach; ?>
            </ul>
            <h2>Не сомневайся, приходи!</h2>
            <a href="<?= get_page_link() ?>#register" class="button" id="register-button2">регистрация</a>
        </div>
    </section>


    <section class="timetable" id="program">
        <div class="wrapper">
            <h2>Расписание</h2>
            <?php $event_intervals = array(); ?>
            <?php $schedule = get_field('schedule'); ?>
            <?php foreach ($schedule as $item): ?>
                <div class="row">
                    <?php if (empty($item['lecturer'])): ?>
                        <div class="break"><?= $item['time_interval'] ?> - <?= $item['event_name'] ?></div>
                    <?php else: ?>
                        <?php array_push($event_intervals, $item['time_interval']) ?>
                        <div class="time"><?= $item['time_interval'] ?></div>
                        <p class="place"><?= $item['event_name'] ?></p>
                        <ul>
                            <?php foreach ($item['lecturer'] as $lecturer): ?>
                                <li>
                                    <img src="<?= $lecturer['image'] ?>" alt="<?= $lecturer['name'] ?>">
                                    <div class="about">
                                        <p class="name"><?= $lecturer['name'] ?></p>
                                        <p class="exp"><?= $lecturer['description'] ?></p>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>

        </div>
    </section>

    <section class="register">
        <div class="wrapper">
            <img src="<?= get_template_directory_uri() ?>/images/landing/reg-img.png" alt="A-level">
            <form action="" id="register" method="post">
                <h2>Регистрация</h2>
                <input type="text" name="username" placeholder="Фамилия и Имя *">
                <input type="email" name="email" placeholder="E-mail *">
                <input class="phone-masked" type="text" name="phone">
                <h3>Выберите уроки, котрые хотите посетить (минимум 1)*</h3>
                <div class="checked-block">
                    <?php $event_names = selectEvents(); ?>
                    <?php $i = -1; ?>
                    <?php $ii = -1; ?>
                    <?php foreach ($event_names as $event_name): ?>
                        <?php ++$i; ?>
                        <?php if ($i == 0 || $i % 3 == 0): ?>
                            <?php $selected = true; ?>
                            <?php ++$ii; ?>
                            <ul class="<?= $event_name['colour'] ?>-room">
                            <li><?= $event_intervals[$ii] ?></li>
                        <?php endif; ?>
                        <li class="select-course" data-id="<?= $event_name['id'] ?>" data-colour="<?= $event_name['colour'] ?>">
                            <?= $event_name['name'] ?>
                        </li>
                        <?php $selected = false; ?>
                        <?php if (($i + 1) % 3 == 0): ?>
                            </ul>
                        <?php endif; ?>
                    <?php endforeach; ?>
<!--                    <div class="g-recaptcha" data-sitekey="6LdiXRQUAAAAAP1EhMPNzwXiV779JSgc2bL38nXd"></div>-->
                </div>
                <p class="not-valid">Такой телефон или email уже зарегестрирован</p>
                <input type="submit" value="отправить">
            </form>
        </div>
    </section>

    <section class="partners" id="partners">
        <div class="wrapper">
            <h2>Партнеры</h2>
            <ul class="owl-carousel owl-theme owl-loaded">
                <?php $args = array(
                    'post_type' => 'partners',
                    'orderby' => 'ID',
                    'order' => 'ASC',
                    'posts_per_page' => -1); ?>
                <?php $i = 0; ?>
                <?php $post_partners = new WP_query($args); ?>
                <?php while ($post_partners->have_posts()) : $post_partners->the_post(); ?>
                    <?php ++$i; ?>
                    <?php if ($i % 2 != 0): ?>
                        <li>
                    <?php endif; ?>
                    <img src="<?= get_the_post_thumbnail_url(); ?>" alt="<?= get_the_title() ?>">
                    <?php if ($i % 2 == 0 || $i == $post_partners->post_count): ?>
                        </li>
                    <?php endif; ?>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            </ul>
            <p>Готовы стать партнером?</p>
            <button id="will-be-partner">свяжитесь с нами</button>
        </div>
    </section>
<?php
if (isset($_GET['path']) && !empty($_GET['path']))
    if (updateUserData($_GET['path']))
        echo '<script src="' . get_template_directory_uri() . '/js/landing/show-congratulations.js"</script>';

get_footer();




