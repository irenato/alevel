<?php

?>

<!--START MODAL-->
<section class="modal-container" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">

    <!--    --><? //= do_shortcode('[contact-form-7 id="28" title="alevel-main" html_class="enroll-in-a-course"]'); ?>
    <form action="#" class="enroll-in-a-course">

        <div class="hamburger is-active hamburger--spring js-hamburger">
            <div class="hamburger-box">
                <div class="hamburger-inner"></div>
            </div>
        </div>
        <p>Я,</p>
        <div class="input">
            <input type="text" placeholder="Имя и Фамилия" class="student-name">
            <label for="#"></label>
        </div>
        <p>, хочу записаться</p>
        <p>в <span>A</span>-Level <b>Ukraine</b> на курc</p>
        <div class="select">
            <select name="#" id="#" class="custom-select sources" placeholder="Хочу курс">
                <option value="#"></option>
                <?php foreach ($courses as $course) : ; ?>
                    <option value="#"><?= $course; ?></option>
                <?php endforeach; ?>
            </select>
            <label for=""></label>
        </div>
        <p>Свяжитесь со мной по телефону</p>
        <div class="input">
            <input type="tel" class="phone-mask">
            <label for="#"></label>
        </div>
        <p>или по e-mail</p>
        <div class="input">
            <input type="email" placeholder="e-mail" class="student-email">
            <label for="#"></label>
        </div>
        <!--        <input type="submit" class="button" value="отправить заявку">-->
        <span class="text-danger errors-on-main-form" style="display: none;"></span>
        <button class='button mobile-form wonna-study'>отправить заявку</button>
        <p class="min-text">*Вы принимаете политику конфиденциальности. Ваш адрес электронной почты и ФИО ни в коем случае не будут переданы другим пользователям и никогда не будут открыты для просмотра. Мы не будем передавать эти данные третьим лицам и/или любым другим способом сообщать эти данные третьим лицам.</p>
    </form>

    <form action="#" class="enroll-in-a-course-mobile">
        <div class="hamburger is-active hamburger--spring js-hamburger">
            <div class="hamburger-box">
                <div class="hamburger-inner"></div>
            </div>
        </div>
        <div class="input">
            <input type="text" placeholder="Имя и Фамилия" class="student-name">
            <label for="#"></label>
        </div>
        <div class="select">
            <select name="#" id="#" class="custom-select sources" placeholder="Курс">
                <option value="#"></option>
                <?php foreach ($courses as $course) : ; ?>
                    <option value="#"><?= $course; ?></option>
                <?php endforeach; ?>
            </select>
            <label for=""></label>
        </div>
        <div class="input">
            <input type="email" placeholder="E-mail" class="student-email">
            <label for="#"></label>
        </div>
        <div class="input">
            <input type="tel" placeholder="Телефон" class="phone-mask">
            <label for="#"></label>
        </div>
        <!--        <input type="submit" class="button mobile-form" value="отправить">-->
        <button class='button mobile-form wonna-study'>отправить</button>
        <p class="min-text">*Вы принимаете политику конфиденциальности. Ваш адрес электронной почты и ФИО ни в коем случае не будут переданы другим пользователям и никогда не будут открыты для просмотра. Мы не будем передавать эти данные третьим лицам и/или любым другим способом сообщать эти данные третьим лицам.</p>
    </form>
</section>

<section class="modal-container-teacher be-teacher">

    <form action="#" class="enroll-in-a-course">
        <div class="hamburger is-active hamburger--spring js-hamburger">
            <div class="hamburger-box">
                <div class="hamburger-inner"></div>
            </div>
        </div>
        <p>Я,</p>
        <div class="input">
            <input type="text" placeholder="Имя и Фамилия" class="student-name">
            <label for="#"></label>
        </div>
        <p>, хочу преподовать</p>
        <p>в <span>A</span>-Level <b>Ukraine</b> курc</p>
        <div class="select">
            <select name="#" id="#" class="custom-select sources" placeholder="Хочу курс">
                <option value="#"></option>
                <?php foreach ($courses as $course) : ; ?>
                    <option value="#"><?= $course; ?></option>
                <?php endforeach; ?>
            </select>
            <label for=""></label>
        </div>
        <p>Свяжитесь со мной по телефону</p>
        <div class="input">
            <input type="tel" class="phone-mask">
            <label for="#"></label>
        </div>
        <p>или по e-mail</p>
        <div class="input">
            <input type="email" placeholder="e-mail" class="student-email">
            <label for="#"></label>
        </div>
        <!--        <input type="submit" class="button" value="отправить заявку">-->
        <span class="text-danger errors-on-main-form" style="display: none;"></span>
        <button class='button mobile-form wonna-teach'>отправить заявку</button>
        <p class="min-text">*Вы принимаете политику конфиденциальности. Ваш адрес электронной почты и ФИО ни в коем случае не будут переданы другим пользователям и никогда не будут открыты для просмотра. Мы не будем передавать эти данные третьим лицам и/или любым другим способом сообщать эти данные третьим лицам.</p>
    </form>

    <form action="#" class="enroll-in-a-course-mobile">
        <div class="hamburger is-active hamburger--spring js-hamburger">
            <div class="hamburger-box">
                <div class="hamburger-inner"></div>
            </div>
        </div>
        <div class="input">
            <input type="text" placeholder="Имя и Фамилия" class="student-name">
            <label for="#"></label>
        </div>
        <div class="select">
            <select name="#" id="#" class="custom-select sources" placeholder="Курс">
                <option value="#"></option>
                <?php foreach ($courses as $course) : ; ?>
                    <option value="#"><?= $course; ?></option>
                <?php endforeach; ?>
            </select>
            <label for=""></label>
        </div>
        <div class="input">
            <input type="email" placeholder="E-mail" class="student-email">
            <label for="#"></label>
        </div>
        <div class="input">
            <input type="tel" placeholder="Телефон" class="phone-mask">
            <label for="#"></label>
        </div>
        <!--        <input type="submit" class="button mobile-form" value="отправить">-->
        <button class='button mobile-form wonna-teach'>отправить</button>
        <p class="min-text">*Вы принимаете политику конфиденциальности. Ваш адрес электронной почты и ФИО ни в коем случае не будут переданы другим пользователям и никогда не будут открыты для просмотра. Мы не будем передавать эти данные третьим лицам и/или любым другим способом сообщать эти данные третьим лицам.</p>
    </form>

</section>


<section class="modal-container-partner">

    <form action="#" class="enroll-in-a-course">
        <div class="hamburger is-active hamburger--spring js-hamburger">
            <div class="hamburger-box">
                <div class="hamburger-inner"></div>
            </div>
        </div>
        <p>Мы,</p>
        <div class="input">
            <input type="text" placeholder="Название компании" class="company-name">
            <label for="#"></label>
        </div>
        <p>хотим стать вашим партнером. Контактное лицо </p>
        <div class="input name">
            <input type="text" placeholder="Фамилия и Имя" class="partner-name">
            <label for="#"></label>
        </div>
        <p>Свяжитесь со нами по телефону</p>
        <div class="input">
            <input type="tel" class="phone-mask">
            <label for="#"></label>
        </div>
        <p>или по e-mail</p>
        <div class="input">
            <input type="email" placeholder="e-mail" class="partner-email">
            <label for="#"></label>
        </div>
        <!--        <input type="submit" class="button" value="отправить заявку">-->
        <button class='button mobile-form be-partner'>отправить заявку</button>
        <p class="min-text">*Вы принимаете политику конфиденциальности. Ваш адрес электронной почты и ФИО ни в коем случае не будут переданы другим пользователям и никогда не будут открыты для просмотра. Мы не будем передавать эти данные третьим лицам и/или любым другим способом сообщать эти данные третьим лицам.</p>
    </form>


    <form action="#" class="enroll-in-a-course-mobile">
        <div class="hamburger is-active hamburger--spring js-hamburger">
            <div class="hamburger-box">
                <div class="hamburger-inner"></div>
            </div>
        </div>
        <div class="input">
            <input type="text" placeholder="Название компании" class="company-name">
            <label for="#"></label>
        </div>
        <div class="input">
            <input type="text" placeholder="Имя и Фамилия" class="partner-name">
            <label for="#"></label>
        </div>
        <div class="input">
            <input type="email" placeholder="E-mail" class="partner-email">
            <label for="#"></label>
        </div>
        <div class="input">
            <input type="tel" placeholder="Телефон" class="phone-mask">
            <label for="#"></label>
        </div>
        <!--        <input type="submit" class="" value="отправить">-->
        <button class='button mobile-form be-partner'>отправить</button>
        <p class="min-text">*Вы принимаете политику конфиденциальности. Ваш адрес электронной почты и ФИО ни в коем случае не будут переданы другим пользователям и никогда не будут открыты для просмотра. Мы не будем передавать эти данные третьим лицам и/или любым другим способом сообщать эти данные третьим лицам.</p>
    </form>

</section>
<section class="modal-container-thank-you">
    <div class="content">
        <div class="hamburger is-active hamburger--spring js-hamburger">
            <div class="hamburger-box">
                <div class="hamburger-inner"></div>
            </div>
        </div>
        <h3 class="title">Спасибо, заявка принята!</h3>
        <a class="button">Вернуться назад</a>
    </div>
</section>

<section class="modal-container-recall">
    <div class="content">
        <form id="review-form" enctype="multipart/form-data">
            <div class="hamburger is-active hamburger--spring js-hamburger">
                <div class="hamburger-box">
                    <div class="hamburger-inner"></div>
                </div>
            </div>
            <div class="input">
                <input type="text" placeholder="Имя и Фамилия" class="review-username">
                <label for="#"></label>
            </div>
            <div class="input">
                <input type="email" placeholder="email" class="review-email">
                <label for="#"></label>
            </div>

            <div class="input">
                <input type="text" placeholder="Название курса " class="review-course-name">
                <label for="#"></label>
            </div>
            <div class="input">
            <textarea name="review-text"
                      placeholder="Расскажите,например что вам понравилось больше всего, о качестве преподавания, адекватности цен и удобстве расположения курсов..."></textarea>
            </div>
            <div class="add-file">
                <input id="file" type="file" name="photo" accept="image/*,image/jpeg">
                <label for="file"><i class="fa fa-paperclip" aria-hidden="true"></i></label>
            </div>
<!--            <div class="g-recaptcha" data-sitekey="6LdBxSkTAAAAAHS-HCdaFRAaLREurPvB0BRZsTFf"></div>-->
            <label for="button1" class="button"><input type="submit" id="button1" value="отправить отзыв"><i
                    class="ic-telegram"></i></label>
            <p class="min-text">*Вы принимаете политику конфиденциальности. Ваш адрес электронной почты и ФИО ни в коем случае не будут переданы другим пользователям и никогда не будут открыты для просмотра. Мы не будем передавать эти данные третьим лицам и/или любым другим способом сообщать эти данные третьим лицам.</p>        
        </form>
    </div>
</section>

<!--modal free study-->

<section class="modal-container-teacher free-study">
    <form action="#" class="enroll-in-a-course">
        <div class="hamburger is-active hamburger--spring js-hamburger">
            <div class="hamburger-box">
                <div class="hamburger-inner"></div>
            </div>
        </div>
        <p>Я,</p>
        <div class="input">
            <input type="text" placeholder="Имя и Фамилия" class="student-name">
            <label for="#"></label>
        </div>
        <p>, хочу записаться</p>
        <p>в <span>A</span>-Level <b>Ukraine</b> на курc</p>
        <div class="select">
            <select name="#" id="#" class="custom-select sources" placeholder="Хочу курс">
                <option value="#"></option>
                <?php foreach ($courses as $course) : ; ?>
                    <option value="#"><?= $course; ?></option>
                <?php endforeach; ?>
            </select>
            <label for=""></label>
        </div>
        <p>Свяжитесь со мной по телефону</p>
        <div class="input">
            <input type="tel" class="phone-mask">
            <label for="#"></label>
        </div>
        <p>или по e-mail</p>
        <div class="input">
            <input type="email" placeholder="e-mail" class="student-email">
            <label for="#"></label>
        </div>
        <!--        <input type="submit" class="button" value="отправить заявку">-->
        <span class="text-danger errors-on-main-form" style="display: none;"></span>
        <button class='button mobile-form wonna-study-free'>отправить заявку</button>
        <p class="min-text">*Вы принимаете политику конфиденциальности. Ваш адрес электронной почты и ФИО ни в коем случае не будут переданы другим пользователям и никогда не будут открыты для просмотра. Мы не будем передавать эти данные третьим лицам и/или любым другим способом сообщать эти данные третьим лицам.</p>
    </form>

    <form action="#" class="enroll-in-a-course-mobile">
        <div class="hamburger is-active hamburger--spring js-hamburger">
            <div class="hamburger-box">
                <div class="hamburger-inner"></div>
            </div>
        </div>
        <div class="input">
            <input type="text" placeholder="Имя и Фамилия" class="student-name">
            <label for="#"></label>
        </div>
        <div class="select">
            <select name="#" id="#" class="custom-select sources" placeholder="Курс">
                <option value="#"></option>
                <?php foreach ($courses as $course) : ; ?>
                    <option value="#"><?= $course; ?></option>
                <?php endforeach; ?>
            </select>
            <label for=""></label>
        </div>
        <div class="input">
            <input type="email" placeholder="E-mail" class="student-email">
            <label for="#"></label>
        </div>
        <div class="input">
            <input type="tel" placeholder="Телефон" class="phone-mask">
            <label for="#"></label>
        </div>
        <!--        <input type="submit" class="button mobile-form" value="отправить">-->
        <button class='button mobile-form wonna-study-free'>отправить</button>
        <p class="min-text">*Вы принимаете политику конфиденциальности. Ваш адрес электронной почты и ФИО ни в коем случае не будут переданы другим пользователям и никогда не будут открыты для просмотра. Мы не будем передавать эти данные третьим лицам и/или любым другим способом сообщать эти данные третьим лицам.</p>
    </form>
</section>

<!--END MODAL-->

<section class="application-form-for-free-training">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <form action="#">
                    <div class="hamburger is-active hamburger--spring js-hamburger">
                        <div class="hamburger-box">
                            <div class="hamburger-inner"></div>
                        </div>
                    </div>
                    <div class="input">
                        <input type="text" class="fs-username" placeholder="Имя и Фамилия">
                        <label for=""></label>
                    </div>
                    <div class="input">
                        <input type="text" class="fs-email" placeholder="E-mail">
                        <label for=""></label>
                    </div>
                    <div class="input">
                        <input type="text" class="phone-mask" placeholder="Телефон">
                        <label for=""></label>
                    </div>
                    <textarea class="fs-story" placeholder="Ваша история"></textarea>
                    <div class="add-file">
                        <input id="file" type="file" name="photo" accept="image/*,image/jpeg">
                        <label for="file"><i class="fa fa-paperclip" aria-hidden="true"></i></label>
                    </div>
                    <input type="submit" class="button" id="free-education" value="Отправить">
                    <p class="min-text">*Вы принимаете политику конфиденциальности. Ваш адрес электронной почты и ФИО ни в коем случае не будут переданы другим пользователям и никогда не будут открыты для просмотра. Мы не будем передавать эти данные третьим лицам и/или любым другим способом сообщать эти данные третьим лицам.</p>
                </form>
            </div>
        </div>
    </div>
</section>

<section class="become-a-sponsor">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <form action="#">
                    <div class="hamburger is-active hamburger--spring js-hamburger">
                        <div class="hamburger-box">
                            <div class="hamburger-inner"></div>
                        </div>
                    </div>
                    <div class="input">
                        <input type="text" class="sp-name" placeholder="Имя и Фамилия">
                        <label for=""></label>
                    </div>
                    <div class="input">
                        <input type="text" class="sp-mail" placeholder="E-mail">
                        <label for=""></label>
                    </div>
                    <input type="submit" id="became-sponsor" class="button" data-id="" value="Отправить">
					<p class="min-text">*Вы принимаете политику конфиденциальности. Ваш адрес электронной почты и ФИО ни в коем случае не будут переданы другим пользователям и никогда не будут открыты для просмотра. Мы не будем передавать эти данные третьим лицам и/или любым другим способом сообщать эти данные третьим лицам.</p>
                </form>
            </div>
        </div>
    </div>
</section>

<section class="free-education modal-free-education">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="people">
                    <div class="item">
                        <div class="for-education">
                            <div class="hamburger is-active hamburger--spring js-hamburger">
                                <div class="hamburger-box">
                                    <div class="hamburger-inner"></div>
                                </div>
                            </div>
                            <img src="" alt="#" class="photo">
                            <div class="education-description">
                                <h3 class="name"></h3>
                                <ul>
                                    <li class="free-education-phone"><a href=""><i class="ic-phone"></i></a></li>
                                    <li class="free-education-email"><a href=""><i class="ic-email-outline"></i></a>
                                    </li>
                                </ul>
                                <a href="#" class="button sponsor" data-id="">стать спонсором</a>
                                <div class="like-button-modal"></div>
                                <!--                                <a class="like"><i class="ic-like"></i>123</a>-->

                            </div>
                            <div class="text">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
