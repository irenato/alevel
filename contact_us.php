<?php
/**
 * Template name: Contact-us page
 */

get_header();
?>

<!--START CONTENT-->
<section class="contacts-page">

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <h2 class="title">contacts<span>контакты</span></h2>
                    <div id="contact_map"></div>

                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ul class="top-contacts">
                    <li>e-mail:<a href="mailto:<?= get_option('admin_email') ?>"><?= get_option('admin_email') ?></a></li>
                    <li>телефоны:
                        <a href="tel:<?= get_option('phone1'); ?>"><?= get_option('phone1'); ?></a>
                        <a href="tel:<?= get_option('phone2'); ?>"><?= get_option('phone2'); ?></a>
                    </li>
                    <li>Адрес:<a href="<?= get_option('googlemaps') ?>" target="_blank"><?= get_option('address'); ?></a></li>
                </ul>
                <div class="clearfix"></div>
                <div>
                    <div class="course-administrator">
                        <img src="<?= get_option('admin_courses_avatar') ?>" alt="">
                        <h4 class="name"><?= get_option('admin_courses_fullname') ?></h4>
                        <h5 class="position">Администратор курсов</h5>
                    </div>
                    <form action="#" >
                        <a name="partner"></a>
                        <h3>Связаться с нами</h3>
                        <p>по вопросам работы курсов, сотрудничеству и обратной связи</p>
                        <div class="form">
                            <div class="input">
                                <input type="text" placeholder="Имя и Фамилия">
                                <label for="#"></label>
                            </div>
                            <div class="input">
                                <input type="email" placeholder="e-mail">
                                <label for="#"></label>
                            </div>
                            <div class="input">
                                <input type="tel" placeholder="Телефон">
                                <label for="#"></label>
                            </div>
                            <div class="input">
                                <input type="text" placeholder="Сообщение">
                                <label for="#"></label>
                            </div>
                            <input type="submit" value="отправить">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>



 <div class="container-fluid visible-xs">
        <div class="row">
            <div class=" col-xs-12 ">
                <div class="row">
                    <h2 class="title cont">contact us<span> контакты</span></h2>
                    <div class="contact ">
                        <ul>
                            <li>
                                <i class="ic-email-outline"></i>
                                <div>
                                    <h5>e-mail</h5>
                                    <a href="mailto:<?= get_option('admin_email') ?>"><?= get_option('admin_email') ?></a>
                                </div>
                            </li>
                            <li>
                                <i class="ic-phone"></i>
                                <div>
                                    <h5>телефоны</h5>
                                    <?php if (get_option('phone1')) : ?>
                                        <a href="tel:<?= get_option('phone1'); ?>"><?= get_option('phone1'); ?></a>
                                    <?php endif; ?>
                                    <?php if (get_option('phone2')) : ?>
                                        <a href="tel:<?= get_option('phone2'); ?>"><?= get_option('phone2'); ?></a>
                                    <?php endif; ?>
                                </div>
                            </li>
                            <li>
                                <i class="ic-map-marker"></i>
                                <div>
                                    <h5>Адрес</h5>
                                    <a href="https://www.google.com.ua/maps/place/%D0%BF%D0%BB%D0%BE%D1%89%D0%B0+%D0%9F%D0%B0%D0%B2%D0%BB%D1%96%D0%B2%D1%81%D1%8C%D0%BA%D0%B0,+6,+%D0%A5%D0%B0%D1%80%D0%BA%D1%96%D0%B2,+%D0%A5%D0%B0%D1%80%D0%BA%D1%96%D0%B2%D1%81%D1%8C%D0%BA%D0%B0+%D0%BE%D0%B1%D0%BB%D0%B0%D1%81%D1%82%D1%8C/@49.9874185,36.2285788,17z/data=!3m1!4b1!4m5!3m4!1s0x4127a0f6bbf24153:0xe835d1b1176f4218!8m2!3d49.9874185!4d36.2307675"
                                       target="_blank"><?= get_option('address'); ?></a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="contact_map" class="visible-xs"></div>
</section>

<?php get_footer(); ?>
