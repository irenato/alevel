$(document).ready(function () {
    $('body').on('click', 'a.confirm-it-now', function (e) {
        e.preventDefault();
        var app_id = $(this).attr('data-id'),
            send_data = $.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                data: {
                    'action': 'confirmApplicationFromStudents',
                    'id': app_id,
                },
            });
        send_data.done(function () {
            location.reload();
        })
    });

    $('body').on('click', 'a.delete-now', function (e) {
        e.preventDefault();
        var app_id = $(this).attr('data-id'),
            send_data = $.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                data: {
                    'action': 'deleteApplicationFromStudents',
                    'id': app_id,
                },
            });
        send_data.done(function () {
            location.reload();
        })
    });

    $('body').on('click', 'a.delete-review-now', function (e) {
        e.preventDefault();
        var app_id = $(this).attr('data-id'),
            send_data = $.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                data: {
                    'action': 'deleteReview',
                    'id': app_id,
                },
            });
        send_data.done(function () {
            location.reload();
        })
    });

    $('body').on('click', 'a.confirm-teacher-now', function (e) {
        e.preventDefault();
        var app_id = $(this).attr('data-id'),
            send_data = $.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                data: {
                    'action': 'confirmApplicationFromTeachers',
                    'id': app_id,
                },
            });
        send_data.done(function () {
            location.reload();
        })
    });

    $('body').on('click', 'a.delete-teacher-now', function (e) {
        e.preventDefault();
        var app_id = $(this).attr('data-id'),
            send_data = $.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                data: {
                    'action': 'deleteApplicationFromTeachers',
                    'id': app_id,
                },
            });
        send_data.done(function () {
            location.reload();
        })
    });
    $('body').on('click', 'a.delete-partner-now', function (e) {
        e.preventDefault();
        var app_id = $(this).attr('data-id'),
            send_data = $.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                data: {
                    'action': 'deletePartner',
                    'id': app_id,
                },
            });
        send_data.done(function () {
            location.reload();
        })
    });
    /*
     open doors
     */
    $('a.confirm-participants-now').click(function (e) {
        e.preventDefault();
        $.ajax({
            url: '/wp-admin/admin-ajax.php',
            type: 'POST',
            data: {
                'action': 'confirmParticipant',
                'id': $(this).attr('data-id'),
            },
            success: location.reload(),
        });
    })

    /*
    hub
     */
    $('a.confirm-hub-order').click(function (e) {
        e.preventDefault();
        $.ajax({
            url: '/wp-admin/admin-ajax.php',
            type: 'POST',
            data: {
                'action': 'confirmHubOrder',
                'id': $(this).attr('data-id'),
            },
            success: location.reload(),
        });
    })

    /*
     mc
     */
    $('a.confirm-mc-order').click(function (e) {
        e.preventDefault();
        $.ajax({
            url: '/wp-admin/admin-ajax.php',
            type: 'POST',
            data: {
                'action': 'confirmMCOrder',
                'id': $(this).attr('data-id'),
            },
            success: location.reload(),
        });
    })

})
