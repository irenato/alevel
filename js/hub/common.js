$(document).ready(function(){

    $("a[href^='#']").on("click", function (event) {
        event.preventDefault();
        var id = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1500);
    });    

    $('.gallery').slick({
        dots: false,
        infinite: true,
        arrows: true,
        speed: 400,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        responsive: [
            {
                breakpoint: 750,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: true,
                    arrows: false
                }
            }
        ]
    });

    $.datetimepicker.setLocale('ru');
    $('#datepicker').datetimepicker({
        timepicker:false,
        format:'d.m.Y'
    });

    $('#timepicker-from').datetimepicker({
        datepicker:false,
        format:'H:i',
        mask: true
    });

    $('#timepicker-to').datetimepicker({
        datepicker:false,
        format:'H:i',
        mask: true
    });

    $('#invite-to-hub form').on('click',function (e) {
        e.stopPropagation();
    });

    $('#invite-to-hub').on('click',function () {
        $('#invite-to-hub').hide();
    });

    $('#show_form1').on('click',function () {
        $('#invite-to-hub').show();
    });

    $('#show_form2').on('click',function () {
        $('#invite-to-hub').show();
    });

    $('#show_form3').on('click',function () {
        $('#invite-to-hub').show();
    });

    $('#show_form4').on('click',function () {
        $('#invite-to-hub').show();
    });

    $('.thanks-popup').on('click','button',function () {
        $('.thanks-popup').hide();
    });
    $('#mobile-menu-btn').on('click',function () {
        $('.mobile-main-menu').show();
    })
    
    
    
    $(".phone-mask").mask("+38?(999) 999-99-99");
});