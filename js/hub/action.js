/**
 * Created by renato on 02.04.17.
 */
$(document).ready(function () {
    $('form.new-order').submit(function () {
        var current_form = $(this);
        $(this).find('input').each(function () {
            var name = $(this).attr('name');
            switch (name) {
                case 'name':
                    checkName(this);
                    break;
                case 'phone':
                    checkPhone(this);
                    break;

                case 'email':
                    checkEmail(this);
                    break;

                default:
                    checkValue(this);
                    break;
            }
        })
        if ($(current_form).find('p.error-message:visible').length < 1) {
            $.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                data: {
                    'action': 'inviteHub',
                    'data': $(current_form).serialize()
                },
                success: checkResult,
            })
        }
        return false;
    })

    function checkResult(result) {
        if (result) {
            $(document).find('input:not[type="hidden"]').val('');
            $('#invite-to-hub').hide();
            $('.thanks-popup').show();
        }
    }
})

function checkEmail(that) {
    var rv_mail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if ($(that).val() !== '' && rv_mail.test($(that).val())) {
        $(that).closest('div.input-container').find('p.error-message').hide();
    } else {
        $(that).closest('div.input-container').find('p.error-message').show();
    }
}

function checkPhone(that) {
    var rv_name = /^\+38\(0[0-9]{2}\) [0-9]{3}\-[0-9]{2}\-[0-9]{2}?_?$/;

    if (rv_name.test($(that).val())) {
        $(that).closest('div.input-container').find('p.error-message').hide();
    } else {
        $(that).closest('div.input-container').find('p.error-message').show();
    }
}

function checkName(that) {
    var rv_name = /[a-zA-Zа-яА-я]+(\W+)?(\s+)?/;
    if ($(that).val().length > 2 && $(that).val() !== '' && rv_name.test($(that).val())) {
        $(that).closest('div.input-container').find('p.error-message').hide();
    } else {
        $(that).closest('div.input-container').find('p.error-message').show();
    }
}

function checkValue(that) {
    if ($(that).val().length != 0) {
        $(that).closest('div.input-container').find('p.error-message').hide();
    } else {
        $(that).closest('div.input-container').find('p.error-message').show();
    }
}