// $(document).ready(function () {
    function updateTimer() {
        future = Date.parse($('#has-date').attr('data-date'));
        now = new Date();
        diff = future - now;

        days = Math.floor(diff / (1000 * 60 * 60 * 24));
        hours = Math.floor(diff / (1000 * 60 * 60));
        mins = Math.floor(diff / (1000 * 60));
        secs = Math.floor(diff / 1000);

        d = days;
        h = hours - days * 24;
        m = mins - hours * 60;
        s = secs - mins * 60;

        document.getElementById("timer")
            .innerHTML =
            '<div>' + d + '/' +
            '<div>' + h + ':' +
            '<div>' + m + ':' +
            '<div>' + s + '';
    }

    setInterval('updateTimer()', 1000);

// })
