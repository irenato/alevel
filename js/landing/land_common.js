$(document).ready(function ($) {

    $(".owl-carousel").owlCarousel({
        autoPlay: 2000,
        loop: true,
        items: 4,
        dots: false,
        nav: true,
        navText: false,
        autoplay: true,
        responsive: {320: {items: 1}, 460: {items: 2}, 600: {items: 3}, 767: {items: 3}, 1199: {items: 4}}
    });
    $(function () {
        $('a[href*=#]:not([href=#])').click(function () {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({scrollTop: target.offset().top}, 1000);
                    return false;
                }
            }
        });
    });
    $('#play-video').on('click', function () {
        $('.video-front').fadeToggle("slow");
        var video = $('#videoPlayer')[0];
        video.pause();
        video.currentTime = 0;
        video.load();
        setTimeout(function () {
            video.muted = false;
        }, 1000);
    });
    $('#will-be-partner').on('click', function () {
        $('#modal-will-be-partners').css('display', 'flex');
    });

    // $('#sbm-partner').on('click', function () {
    //     $('#modal-will-be-partners').css('display', 'none');
    //     $('#modal-thanks-for-register').css('display', 'flex');
    // });
    $('#modal-will-be-partners').on('click', function () {
        $('#modal-will-be-partners').css('display', 'none');
    })
    $('#modal-thanks-for-register').on('click', function () {
        $('#modal-thanks-for-register').css('display', 'none');
    })

    $("#mouse").on("click", "a", function (event) {
        event.preventDefault();
        var id = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1000);
    });
    /*
     Маска для номера
     */
    $(".phone-masked").mask("+38?(999) 999-99-99");

    /*
     Выбор предмета
     */
    $('li.select-course').click(function () {
        $('div.checked-block').removeClass('errors');
        if ($(this).hasClass('active-curse')) {
            $(this).removeClass('active-curse');
        } else {
            $(this).closest('ul').find('li.select-course').removeClass('active-curse');
            $(this).addClass('active-curse');
        }
    })


    /*
     Регистрация
     */
    $('#register').on('submit', function () {
        $('p.not-valid').hide();
        $('div.checked-block').removeClass('errors');
        var user_name = $(this).find('input[name=username]'),
            email = $(this).find('input[name=email]'),
            phone = $(this).find('input[name=phone]'),
            green = $(this).find('ul.green-room li.active-curse'),
            orange = $(this).find('ul.orange-room li.active-curse'),
            blue = $(this).find('ul.blue-room li.active-curse');
        if (!checkName(user_name) || !checkEmail(email) || !checkPhone(phone) || !checkCourse(green, orange, blue)) {
            grecaptcha.reset();
        } else {
            $.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                data: {
                    'action': 'registerMe',
                    // 'captcha': grecaptcha.getResponse(),
                    'email': $(email).val(),
                    'user_name': $(user_name).val(),
                    'phone': $(phone).val(),
                    'green': green.length > 0 ? $(green).attr('data-id') : '',
                    'orange': orange.length > 0 ? $(orange).attr('data-id') : '',
                    'blue': blue.length > 0 ? $(blue).attr('data-id') : '',
                },
                success: checkResult,
            });
        }
        return false;
    })


    /*
     Регистрация (мастер-класс)
     */
    $('#register_mc').on('submit', function () {
        $('p.not-valid').hide();
        $('div.checked-block').removeClass('errors');
        if (checkName($(this).find('input[name=username]')) || checkEmail($(this).find('input[name=email]')) || checkPhone($(this).find('input[name=phone]'))) {
            $.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                data: $(this).serialize() + '&action=' + 'registerMeMC',
                success: function(){
                    $('#modal-thanks-for-register').css('display', 'flex');
                    $('#register_mc').find(":input:not([type=hidden]):not([type=submit])").val('');
                },
                error: function(){
                    $('p.not-valid').show();
                }
            });
        }
        return false;
    })

    function checkResultMC(responce){
            $('#modal-thanks-for-register').css('display', 'flex');
            $('#register_mc').find(":input:not([type=hidden]):not([type=submit])").val('');
    }


    $('#form-will-be-partners').on('submit', function () {
        var user_name = $(this).find('input[name=partner-name]'),
            email = $(this).find('input[name=partner-mail]'),
            phone = $(this).find('input[name=partner-phone]'),
            company_name = $(this).find('input[name=company-name]');
        if (checkName(user_name) && checkEmail(email) && checkPhone(phone) && checkLength(company_name)) {
            $.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                data: {
                    'action': 'bePartner',
                    'partner_name': $(user_name).val(),
                    'partner_mail': $(email).val(),
                    'partner_phone': $(phone).val(),
                    'company_name': $(company_name).val(),
                },
                success: thanksForPartner,
            });
        }
        return false;
    });

    function thanksForPartner() {
        $('#form-will-be-partners').find('input').val('');
        $('#confirmation-sub-text').text('');
        $('#confirmation-ps').text('');
        $('#confirmation-main-text').text('Благодарим. Мы обязательно свяжемся с Вами в ближаейшее время');
        $('#modal-will-be-partners').css('display', 'none');
        $('#modal-thanks-for-register').css('display', 'flex');
        setTimeout(function () {
            location.reload();
        }, 2000);
    }

    function showError(el) {
        $(el).addClass('error');
    }

    function checkResult(data) {
        if (data == 'recaptcha') {
            // grecaptcha.reset();
        } else if (data == 'ok!') {
            $('#modal-thanks-for-register').css('display', 'flex');
            $('input[name=username]').val('');
            $('input[name=phone]').val('');
            $('input[name=email]').val('');
        } else {
            // grecaptcha.reset();
            $('p.not-valid').show();
        }
    }

    function checkCourse(green, orange, blue) {
        if (green.length < 1 && orange.length < 1 && blue.length < 1) {
            $('div.checked-block').addClass('errors');
            return false;
        } else {
            $('div.checked-block').removeClass('errors');
            return true;
        }
    }

    function checkEmail(that) {
        var rv_mail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if ($(that).val() !== '' && rv_mail.test($(that).val())) {
            $(that).removeClass('errors');
            return true;
        } else {
            $(that).addClass('errors');
            return false;
        }
    }

    function checkPhone(that) {
        var rv_name = /^\+38\(0[0-9]{2}\) [0-9]{3}\-[0-9]{2}\-[0-9]{2}?_?$/;

        if (rv_name.test($(that).val())) {
            $(that).removeClass('errors');
            return true;
        } else {
            $(that).addClass('errors');
            return false;
        }
    }

    function checkName(that) {
        var rv_name = /[a-zA-Zа-яА-я]+(\W+)?(\s+)?/;
        if ($(that).val().length > 2 && $(that).val() !== '' && rv_name.test($(that).val())) {
            $(that).removeClass('errors');
            return true;
        } else {
            $(that).addClass('errors');
            return false;
        }
    }

    function checkLength(that) {
        if ($(that).val().length > 2) {
            $(that).removeClass('errors');
            return true;
        } else {
            $(that).addClass('errors');
            return false;
        }
    }

    $('#form-will-be-partners').on('click', function (e) {
        e.stopPropagation();
    });
    
    $('#main-menu-mobile').on('click',function () {
        $('#modal-menu-mobile').css('display','flex');
    });
    
    $('#modal-menu-mobile li a').on('click', function () {
        $('#modal-menu-mobile').css('display','none');
    });
    
    $('#modal-menu-mobile').on('click', function () {
        $('#modal-menu-mobile').css('display','none');
    });
});
