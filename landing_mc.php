<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 03.02.17
 * Time: 0:25
 */
/*
* Template name: landing (мастер-класс)
*/

get_header();
?>

    <header>
        <div class="wrapper">
            <ul>
                <li>
                    <a href="<?= get_home_url() ?>">
                        <img src="<?= get_template_directory_uri() ?>/images/landing/logo.png" alt="A-level">
                    </a>
                </li>
                <li><a href="<?= get_page_link() ?>#purpose">Цель</a></li>
                <li><a href="<?= get_page_link() ?>#why-we">Плюшки</a></li>
                <li><a href="<?= get_page_link() ?>#program">Ведущие</a></li>
                <li><a href="<?= get_page_link() ?>#partners">Партнеры</a></li>
                <li><a href="<?= get_page_link() ?>#contacts">Контакты</a></li>
            </ul>
            <ul class="mobile-header">
                <li><a href="/"><img src="<?= get_template_directory_uri() ?>/images/landing/logo.png"
                                     alt="A-level"></a></li>
                <li id="main-menu-mobile"></li>
            </ul>
        </div>
    </header>

    <section class="top-block">
        <div class="wrapper">
            <div class="circle lg">
                <h1><?= get_the_title() ?></h1>
                <?php include_once('settings/months.php');
                $date = get_field('event_date') ? explode('.', get_field('event_date')) : '';
                ?>
                <?php if ($date): ?>
                    <p><?= $date[2] ?> <?= $months[$date[1] * 1] ?>, <?= get_field('event_begin') ?></p>
                <?php endif; ?>
                <a href="<?= get_page_link() ?>#register" class="button" id="register-button">регистрация</a>
            </div>
            <img src="<?= get_field('top_block_baner') ?>" alt="<?= get_the_title(); ?>">
        </div>
    </section>

<?php $descriptions = get_field('event_target_description'); ?>
<?php if ($descriptions): ?>
    <section class="purpose" id="purpose">
        <div class="wrapper">
            <div class="description">
                <h1><?= get_field('event_target') ?></h1>

                <?php foreach ($descriptions as $description): ?>
                    <p>
                        <?= $description['descripton'] ?>
                    </p>
                <?php endforeach; ?>
            </div>
            <?php $images = get_field('event_target_images'); ?>
            <?php foreach ($images as $image): ?>
                <img src="<?= $image['image'] ?>" alt="A-Level">
            <?php endforeach; ?>
        </div>
    </section>
<?php endif; ?>

<?php $video = get_field('video'); ?>
<?php if ($video): ?>
    <section class="video">
        <div class="wrapper">
            <video id="videoPlayer" controls autoplay muted loop="loop">
                <source src="<?= $video ?>" type="video/mp4">
            </video>
            <div class="video-front">
                <p>Видеоотчет <br/> с последнего ивента</p>
                <button id="play-video"><img src="<?= get_template_directory_uri() ?>/images/landing/triangula.png"
                                             alt="play"></button>
            </div>
        </div>
    </section>
<?php endif; ?>

<?php $advantages = get_field('cookies_el'); ?>
<?php if ($advantages): ?>
    <section class="why-we" id="why-we">
        <div class="wrapper">
            <ul>
                <?php foreach ($advantages as $advantage): ?>
                    <li>
                        <img src="<?= $advantage['image'] ?>" alt="<?= $advantage['alt'] ?>">
                        <p><?= $advantage['description'] ?></p>
                    </li>
                <?php endforeach; ?>
            </ul>
            <h2>Не сомневайся, приходи!</h2>
            <a href="<?= get_page_link() ?>#register" class="button" id="register-button2">регистрация</a>
        </div>
    </section>
<?php endif; ?>

    <section class="timetable" id="program">
        <div class="wrapper">
            <h2><?= get_field('teachers_title') ?></h2>
            <?php $teachers = get_field('teachers'); ?>
            <?php if ($teachers): ?>
                <?php foreach ($teachers as $item): ?>
                    <div class="row">
                        <img src="<?= $item['image'] ?>" alt="<?= $item['teacher_name'] ?>">
                            <p class="name"><?= $item['teacher_name'] ?></p>
                            <p class="exp"><?= $item['description'] ?></p>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </section>

    <section class="register">
        <div class="wrapper">
            <img src="<?= get_template_directory_uri() ?>/images/landing/reg-img.png" alt="A-level">
            <form action="" id="register_mc" method="post">
                <h2>Регистрация</h2>
                <input type="text" name="username" placeholder="Фамилия и Имя *">
                <input type="email" name="email" placeholder="E-mail *">
                <input class="phone-masked" type="text" name="phone">
                <input type="hidden" name="post_id" value="<?= get_the_ID() ?>">
                <input type="hidden" name="link" value="<?= get_permalink() ?>">
                <p class="not-valid">Такой телефон или email уже зарегистрирован</p>
                <input type="submit" value="отправить">
            </form>
        </div>
    </section>

    <section class="partners" id="partners">
        <div class="wrapper">
            <h2>Партнеры</h2>
            <ul class="owl-carousel owl-theme owl-loaded">
                <?php $args = array(
                    'post_type' => 'partners',
                    'orderby' => 'ID',
                    'order' => 'ASC',
                    'posts_per_page' => -1); ?>
                <?php $i = 0; ?>
                <?php $post_partners = new WP_query($args); ?>
                <?php while ($post_partners->have_posts()) : $post_partners->the_post(); ?>
                    <?php ++$i; ?>
                    <?php if ($i % 2 != 0): ?>
                        <li>
                    <?php endif; ?>
                    <img src="<?= get_the_post_thumbnail_url(); ?>" alt="<?= get_the_title() ?>">
                    <?php if ($i % 2 == 0 || $i == $post_partners->post_count): ?>
                        </li>
                    <?php endif; ?>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            </ul>
            <p>Готовы стать партнером?</p>
            <button id="will-be-partner">свяжитесь с нами</button>
        </div>
    </section>
<?php
if (isset($_GET['path']) && !empty($_GET['path']))
    if (updateUserDataMC($_GET['path']))
        echo '<script src="' . get_template_directory_uri() . '/js/landing/show-congratulations.js"</script>';

get_footer();




