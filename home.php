<?php
get_header();
?>

<!--START CONTENT-->

<section class="inner-news">

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="video size">
                    	<div class="bg-filter-video"></div>
                        <video autoplay loop id="bgvid">
                            <source src="<?= get_template_directory_uri() ?>/images/alevelNew.mp4" type="video/mp4"/>
                        </video>
                        <img src="<?= get_template_directory_uri() ?>/images/landscape.png" alt="#"
                             class="landscape video-img">
                        <img src="<?= get_template_directory_uri() ?>/images/portrait.png" alt="#"
                             class="portrait video-img">
                        <div>
                            <h1 class="title">переходи на новый уровень жизни <br>вместе с <span>a-level</span></h1>
                            <a href="#enroll-in-a-course" class="button">записаться на курс</a>
                             <!--<a href="http://a-level.com.ua/open-doors/" class="button open-dors-mobile">День открытых дверей</a>-->
                            <div class="mouse">
                                <a href="#about-School"><i class="ic-mouse"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container" id="about-School">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2 class="title">school<span>о школе</span></h2>
                <span class="title">A</span>
                <div class="aboutSchoolCont">
                    <div class="aboutSchoolIn">
                        <?php $about_school = get_field('about_school', 6); ?>
                        <?php if ($about_school) : ?>
                            <?php foreach ($about_school as $about) : ?>
                                <p><?= $about['about_school_item'] ?></p>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
                <a href="<?= get_page_link(6) ?>" class="button">Подробнее</a>
            </div>
        </div>
    </div>

    <!-- Блок с курсами (начало) -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2 class="title">courses<span>идёт набор</span></h2>
                <div class="courses" id="courses">
                    <?php $courses = array(); ?>
                    <?php $args = array(
                        'offset' => 0,
                        'post_type' => 'courses',
                        'posts_per_page' => -1); ?>
                    <?php $post_courses = new WP_query($args); ?>
                    <?php while ($post_courses->have_posts()) : $post_courses->the_post(); ?>
                        <?php array_push($courses, get_the_title()); ?>
                        <div class="courses-item ">
                            <?php if (get_field('expected_count_of_students') <= get_field('current_count_of_students')) : ?>
                                <p class="places">Набор закрыт <span>   </span></p>
                            <?php else: ?>
                                <p class="places">Осталось
                                    мест: <span><?= get_field('expected_count_of_students') - get_field('current_count_of_students') ?>
                                        из <?= get_field('expected_count_of_students') ?></span></p>
                            <?php endif; ?>
                            <img src="<?= get_the_post_thumbnail_url() ?>" alt="<?php the_title(); ?>">
                            <h5 class="title">Курс</h5>
                            <div>
                                <?php $course_name = get_the_title(); ?>
                                <h3 class="title  <?= (mb_strlen($course_name, 'UTF-8') < 14) ? 'one-line' : '' ?>"><?= $course_name ?></h3>
                                <p class="Start">
<!--                                    Старт: <span>--><?//= get_field('course_start') ?><!--</span>-->
                                </p>
                                <p class="cost-of">Стоимость: <span><?= get_field('course_price') ?>
                                        грн/<?= get_field('period_of_paid') == 'per_course' ? 'курс' : 'месяц' ?></span>
                                </p>
                                <p class="date"><?= get_field('course_duration') . ' ' . get_field('course_schedule_days') ?></p>
                            </div>
                            <a href="<?php the_permalink() ?>" class="link"><i>подробнее</i></a>
                            <a href="<?php the_permalink() ?>" class="button">подробнее</a>
                        </div>
                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Блок с курсами (конец) -->


    <!-- Фома обратной связи(начало) -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2 class="title" id="enroll-in-a-course">Check in<span>записаться на курс</span>
                </h2>
                <form action="#" class="enroll-in-a-course">
                    <p>Я,</p>
                    <div class="input">
                        <input type="text" placeholder="Имя и Фамилия" class="student-name">
                        <label for="#"></label>
                    </div>
                    <p>, хочу записаться</p>
                    <p>в <span>A</span>-Level <b>Ukraine</b> на курc</p>
                    <div class="select">
                        <select name="#" id="#" class="custom-select sources" placeholder="Хочу курс">
                            <option value="#"></option>
                            <?php foreach ($courses as $course) : ; ?>
                                <option value="#"><?= $course; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <label for=""></label>
                    </div>
                    <p>Свяжитесь со мной по телефону</p>
                    <div class="input">
                        <input type="tel" class="phone-mask">
                        <label for="#"></label>
                    </div>
                    <p>или по e-mail</p>
                    <div class="input">
                        <input type="email" placeholder="e-mail" class="student-email">
                        <label for="#"></label>
                    </div>
                    <span class="text-danger errors-on-main-form" style="display: none;"></span>
                    <input type="submit" class="button wonna-study" value="отправить заявку">
                    <p class="min-text">*Вы принимаете политику конфиденциальности. Ваш адрес электронной почты и ФИО ни в коем случае не будут переданы другим пользователям и никогда не будут открыты для просмотра. Мы не будем передавать эти данные третьим лицам и/или любым другим способом сообщать эти данные третьим лицам.</p>
                </form>

                <form action="#" class="enroll-in-a-course-mobile">
                    <div class="input">
                        <input type="text" placeholder="Имя и Фамилия">
                        <label for="#"></label>
                    </div>
                    <div class="select">
                        <select name="#" id="#" class="custom-select sources" placeholder="Курс">
                            <option value="#"></option>
                            <?php foreach ($courses as $course) : ?>
                                <option value="#"><?= $course; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <label for=""></label>
                    </div>
                    <div class="input">
                        <input type="email" placeholder="E-mail">
                        <label for="#"></label>
                    </div>
                    <div class="input">
                        <input type="tel" placeholder="Телефон" class="phone-mask">
                        <label for="#"></label>
                    </div>
                    <input type="submit" class="button mobile-form" value="отправить">
                    <p class="min-text">*Вы принимаете политику конфиденциальности. Ваш адрес электронной почты и ФИО ни в коем случае не будут переданы другим пользователям и никогда не будут открыты для просмотра. Мы не будем передавать эти данные третьим лицам и/или любым другим способом сообщать эти данные третьим лицам.</p>
                </form>


            </div>
        </div>
    </div>
    <!-- Фома обратной связи(конец) -->

    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 hide-xs">
                <div class="change-professions">
                    <h3 class="title">Почему стоит<br> <span>менять</span> профессию?</h3>
                    <ul class="left-icon left">
                        <li>Неудобный график<i class="ic-pig-money-safe"></i></li>
                        <li>Низкий доход<i class="ic-clock"></i></li>
                        <li>Безпереспективно<i class="ic-thumb-down"></i></li>
                        <li>Нет вакансий<i class="ic-sadness-boy-face"></i></li>
                        <li>Никакого роста<i class="ic-graph"></i></li>
                    </ul>

                    <div class="slider">
                        <div class="item"><img
                                src="<?= get_template_directory_uri() ?>/images/mc.png" alt="#">
                        </div>
                        <div class="item"><img
                                src="<?= get_template_directory_uri() ?>/images/pol.png" alt="#">
                        </div>
                        <div class="item"><img
                                src="<?= get_template_directory_uri() ?>/images/student.png"
                                alt="#">
                        </div>
                        <div class="item"><img
                                src="<?= get_template_directory_uri() ?>/images/woman.png" alt="#">
                        </div>

                    </div>
                    <img src="<?= get_template_directory_uri() ?>/images/change-professions.png"
                         alt="#">
                    <ul class="right-icon right">
                        <li><i class="ic-notes"></i>Свободный график</li>
                        <li><i class="ic-time-is-money"></i>Высокий доход</li>
                        <li><i class="ic-diamond"></i>Наличие вакансий</li>
                        <li><i class="ic-medal"></i>Перспективно</li>
                        <li><i class="ic-line-graph"></i>Постоянный рост</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-lg-12 lol-md-12 col-sm-12 col-xs-12">

                <div class="chooses">
                    <h3 class="title">Присоединяйся к нашей команде</h3>
                    <div class="choose-item">
                        <img src="<?= get_template_directory_uri() ?>/images/Group.png" alt="">
                        <a class="button">стать преподом</a>
                    </div>
                    <div class="choose-item">
                        <img src="<?= get_template_directory_uri() ?>/images/icon_free_studies.jpg" alt="">
                        <a href="<?= get_page_link(206) ?>" class="button">учиться бесплатно</a>
                    </div>
                    <div class="choose-item">
                        <img src="<?= get_template_directory_uri() ?>/images/Group1.png" alt="">
                        <a class="button">стать партнером</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 hide-xs">
                <h2 class="title">teachers<span>наши преподы</span></h2>
                <div class="teachers" id="teachers">
                    <?php $args = array(
                        'offset' => 0,
                        'post_type' => 'teachers',
                        'posts_per_page' => 9); ?>
                    <?php $i = 0; ?>
                    <?php $post_teachers = new WP_query($args); ?>
                    <?php while ($post_teachers->have_posts()) : $post_teachers->the_post(); ?>
                        <?php $i++; ?>
                        <?php if ($i % 3 == 0)
                            $position = 'right';
                        elseif (($i - 1) % 3 == 0)
                            $position = 'left';
                        else
                            $position = 'center'; ?>
                        <div class="teacher <?= $position; ?> <?= get_field('first_letter') ?>"
                             title-name="<?= get_field('first_letter') ?>">
                            <div class="img" title-name-2="<?= get_field('first_letter') ?>">
                                <img src="<?= get_the_post_thumbnail_url() ?>"
                                     alt="<?php the_title(); ?>">
                            </div>

                            <h4 class="name"><?php the_title(); ?></h4>
                            <h5 class="course"><?= get_field('course') ?></h5>
                            <div class="hover">
                                <div class="description">
                                    <p><?= get_field('description') ?></p>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); ?>

                    <a href="<?= get_page_link(158) ?>" class="button">все преподы</a>
                </div>
            </div>
        </div>
    </div>

    <?php $args = array(
        'cat' => '4,6,1,5'
    ); ?>
    <?php $posts = get_posts($args); ?>
    <?php if ($posts) : ?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2 class="title">News &#38; events<span>новости</span></h2>
                <div class="news-slider">
                    <?php foreach ($posts as $post) : ?>
                        <?php setup_postdata($post); ?>
                        <div class="slider-item">
                            <div class="news-item mini"
                                 style="background-image: url(<?= get_the_post_thumbnail_url() ?>);">
                                <div class="news-item-descreption">
                                    <div>
                                        <h3 class="title"><?php the_title(); ?></h3>
                                        <p class="date"><?= get_field('workshop_date'); ?></p>
                                        <a href="<?php the_permalink(); ?>" class="more">подробнее <i
                                                class="ic-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>


    <div class="container bg">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 hide-xs">
                <div class="get-a-discount">
                    <h3 class="title" id="discount">Получи скидку</h3>
                    <ul class="discount">
                        <?php $args = array(
                            'numberposts' => 12, // тоже самое что posts_per_page
                            'offset' => 0,
                            'post_type' => 'discounts',
                            'orderby' => 'ID',
                            'posts_per_page' => 6); ?>
                        <?php $post_courses = new WP_query($args); ?>
                        <?php while ($post_courses->have_posts()) : $post_courses->the_post(); ?>
                            <li class="right">
                                <div>
                                    <p><?= get_field('discount_description') ?></p>
                                    <span><?= get_the_title() ?>%</span>
                                </div>
                            </li>
                        <?php endwhile; ?>
                        <?php wp_reset_postdata(); ?>
                    </ul>
                    <a href="#" class="button get-course">записаться на курс</a>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 hide-xs owrhid">
                <h2 class="title">reviews<span>отзывы студентов</span></h2>
                <div class="reviews" id="reviews">
                    <div class="owl-carousel owl-theme reviews-slider">
                        <?php $args = array(
                            'post_type' => 'reviews',
                            'orderby' => 'ID',
                            'order' => 'ASC',
                            'posts_per_page' => -1); ?>
                        <?php $post_courses = new WP_query($args); ?>
                        <?php while ($post_courses->have_posts()) : $post_courses->the_post(); ?>
                            <div class="item">
                                <div class="photo">
                                    <div>
                                        <span>
                                            <img src="<?= get_the_post_thumbnail_url() ?>" alt=""/>
                                        </span>
                                    </div>
                                </div>
                                <div class="comment">
                                    <h4 class="name"><?= get_the_title(); ?>
                                        <span><b><?= get_field('course_name') ?></b></span>
                                    </h4>

                                    <div class="scrollComment">
                                        <p class="text "><?= get_field('review') ?></p>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile; ?>
                        
                        <?php wp_reset_postdata(); ?>
                    </div>
                    <label for="button" class="button"><input type="submit" id="button" value="оставить отзыв"><i
                            class="ic-pencil"></i></label>
                </div>
            </div>
        </div>
    </div>


<!--    Partners -->
    <?php get_template_part( 'slider-partners' );  ?>

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                <div class="row">
                    <h2 class="title cont">contact us<span> контакты</span></h2>
                    <div class="contact ">
                        <ul>
                            <li>
                                <i class="ic-email-outline"></i>
                                <div>
                                    <h5>e-mail</h5>
                                    <a href="mailto:<?= get_option('admin_email') ?>"><?= get_option('admin_email') ?></a>
                                </div>
                            </li>
                            <li>
                                <i class="ic-phone"></i>
                                <div>
                                    <h5>телефоны</h5>
                                    <?php if (get_option('phone1')) : ?>
                                        <a href="tel:<?= get_option('phone1'); ?>"><?= get_option('phone1'); ?></a>
                                    <?php endif; ?>
                                    <?php if (get_option('phone2')) : ?>
                                        <a href="tel:<?= get_option('phone2'); ?>"><?= get_option('phone2'); ?></a>
                                    <?php endif; ?>
                                </div>
                            </li>
                            <li>
                                <i class="ic-map-marker"></i>
                                <div>
                                    <h5>Адрес</h5>
                                    <a href="https://www.google.com.ua/maps/place/43B,+%D0%B2%D1%83%D0%BB%D0%B8%D1%86%D1%8F+%D0%A1%D1%83%D0%BC%D1%81%D1%8C%D0%BA%D0%B0,+43%D0%91,+%D0%A5%D0%B0%D1%80%D0%BA%D1%96%D0%B2,+%D0%A5%D0%B0%D1%80%D0%BA%D1%96%D0%B2%D1%81%D1%8C%D0%BA%D0%B0+%D0%BE%D0%B1%D0%BB%D0%B0%D1%81%D1%82%D1%8C/@50.006387,36.2347861,18z/data=!3m1!4b1!4m5!3m4!1s0x4127a0de435d1657:0x92f43e0454946df2!8m2!3d50.0063853!4d36.2358804?hl=ru"
                                       target="_blank"><?= get_option('address'); ?></a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="contact_map"></div>
</section>

<!--START CONTENT-->
<?php get_footer(); ?>

