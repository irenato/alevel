<?php
/**
 * Template name: Free-education page
 */

get_header();
?>

<!--START CONTENT-->
<section class="free-education">

    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2 class="title">free education<span>бесплатное обучение</span></h2>
                <?php $post = get_post(206, ARRAY_A); ?>
                <p class="text"><?= $post['post_content']; ?></p>
                <?php $paged = get_query_var('paged') ? get_query_var('paged') : 1; ?>
                <?php $args = array(
                    'post_type' => 'free_educations',
                    'orderby' => 'ID desc',
                    'order' => 'ASC',
                    'paged' => $paged,
                    'posts_per_page' => 4); ?>
                <?php $i = 0; ?>
                <?php $post_teachers = new WP_query($args); ?>
                <?php if ($post_teachers->posts) : ?>
                    <?php while ($post_teachers->have_posts()) : $post_teachers->the_post(); ?>
                        <?php $i++; ?>
                        <?php if ($i % 2 != 0) : ?>
                            <ul class="people">
                        <?php endif; ?>
                        <li class="item">
                            <p class="date"><?= get_the_date('d.m'); ?><span><?= get_the_date('Y'); ?></span></p>
                            <div class="for-education">
                                <img src="<?= get_the_post_thumbnail_url($post_teachers->ID, 'large') ?>" alt="#"
                                     class="photo">
                                <div class="education-description">
                                    <h3 class="name"><?= get_the_title(); ?></h3>
                                    <ul>
                                        <li class="free-education-phone"><a
                                                href="tel:<?= get_field('free_education_phone') ?>"><i
                                                    class="ic-phone"></i><?= get_field('free_education_phone') ?></a>
                                        </li>
                                        <li class="free-education-email"><a
                                                href="mailto:<?= get_field('free_education_email') ?>"><i
                                                    class="ic-email-outline"></i><?= get_field('free_education_email') ?>
                                            </a>
                                        </li>
                                    </ul>
                                    <p class="text"><?= wp_trim_words(get_the_content(), 25, '...'); ?></p>
                                    <div class="all-content" style="display: none"><?= get_the_content() ?></div>
                                    <div class="like-button-area">
                                        <?= do_shortcode("[vd_likes id=$post_teachers->ID]"); ?>
<!--                                        --><?//= do_shortcode('[likebtn theme="heartcross" lang="ru" dislike_enabled="0" show_like_label="0" icon_like_show="0" icon_dislike_show="0" counter_type="subtract_dislikes" tooltip_enabled="0" popup_disabled="1" share_enabled="0" addthis_service_codes="vk,odnoklassniki_ru,twitter,facebook,preferred_1,preferred_2,preferred_3,compact"]'); ?>
                                    </div>
                                    <!--                                <a class="like"><i class="ic-like"></i>123</a>-->
                                    <a class="more" href="#" data-id="<?= $post->ID; ?>">подробнее <i
                                            class="ic-chevron-right"></i></a>
                                </
                                >
                            </div>
                        </li>
                        <?php if ($i % 2 == 0) : ?>
                            </ul>
                            <a href="#" class="button free-training">учиться бесплатно</a>
                        <?php endif; ?>

                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); ?>
                <?php else : ?>
                    <a href="#" class="button free-training">учиться бесплатно</a>
                <?php endif; ?>
                <ul class="pagination">
                    <?php
                    $big = 999999999;
                    $args = array(
                        'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
                        'format' => '?paged=%#%',
                        'current' => max(1, get_query_var('paged')),
                        'total' => $post_teachers->max_num_pages,
                        'prev_next' => false,
                        'before_page_number' => '<li>',
                        'after_page_number' => '</li>'
                    );

                    echo paginate_links($args); ?>

                </ul>
            </div>
        </div>
    </div>

</section>
<!--START CONTENT-->

<?php get_footer(); ?>
