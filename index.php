<?php

get_header(); ?>
    <!--START CONTENT-->
<?php
//check for main page
if ( is_front_page()  ) {
    // Include the featured content template.
    get_template_part( 'home' );
}else if(is_single()){
    get_template_part( 'single' );
}
?>
    <!--END CONTENT-->
<?php get_footer(); ?>