<?php

get_header();
?>

<?php
if (have_posts()) : while (have_posts()) :
the_post();
?>

    <!--START CONTENT-->
<section class="inner-news">

    <div class="container-fluid banner" style="background-image: url(<?= get_field('banner_image_link') ?>);">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="banner row">
                    <h1 class="title"><?= get_field('workshop_theme'); ?><span
                            class="date"><?= get_field('workshop_date'); ?></span></h1>
                    <div class="arrow-bottom"><a href="#bottom-scroll"><i class="ic-arrow-down"></i></a></div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="description" id="bottom-scroll">
                    <h3 class="title"><?php the_title(); ?></h3>
                    <p><?= get_the_content(); ?></p>
                    <?php $quote = get_field('workshop_quote'); ?>
                    <?php if ($quote) : ?>
                        <p class="text"><?= get_field('workshop_quote'); ?>
                            <span> <?= get_field('author_of_quote'); ?></span>
                        </p>
                    <?php endif; ?>
                    <p><?= get_field('workshop_description2'); ?></p>
                    <?php $subfields = get_field('workshop_parts'); ?>
                    <?php if ($subfields) : ?>
                        <p> Мы рассмотрим:</p>
                        <ul class="circle">
                            <?php foreach ($subfields as $subfield) : ?>
                                <li><?= $subfield['workshop_part'] ?></li>
                            <?php endforeach; ?>
                            <?php unset($subfields); ?>
                        </ul>
                    <?php endif; ?>
                    <?php $subfields = get_field('workshop_speakers'); ?>
                    <?php if ($subfields) : ?>
                        <p><b>Спикеры:</b></p>
                        <ul class="hyphen">
                            <?php foreach ($subfields as $subfield) : ?>
                                <li><?= $subfield['workshop_speaker'] ?>
                                    <?php if ($subfield['workshop_speaker_link']) : ?>
                                        <br> (<a
                                            href="<?= $subfield['workshop_speaker_link']; ?>"><?= $subfield['workshop_speaker_link']; ?></a>)
                                    <?php endif; ?>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                        <?php unset($subfields); ?>
                    <?php endif; ?>
                    <ul class="when">
                        <?php include_once('settings/months.php');
                        if (get_field('workshop_date')):
                            $date = explode('.', get_field('workshop_date'));
                            $workshop_date = mktime(0, 0, 0, $date[1], $date[0], $date[2]);
                            ?>
                            <li>
                                <span>Когда:</span> <?= date('j', $workshop_date) . '  ' . $months[date('n', $workshop_date)] ?>
                                в <?= get_field('workshop_time'); ?>
                            </li>
                        <?php endif; ?>
                        <li><span>Где:</span> <?= get_field('workshop_address') ?>
                        </li>
                        <?php if (get_field('workshop_price')): ?>
                            <li><span>Стоимость участия:</span> <?= get_field('workshop_price') ?> грн</li>
                        <?php endif; ?>
                    </ul>
                    <?php endwhile; ?>
                    <?php endif; ?>
                    <?php if (get_field('google_form_link')) : ?>
                        <a href="<?= get_field('google_form_link') ?>" target="_blank" class="button">регистрация</a>
                    <?php endif; ?>
                    <p class="hide-xs">Узнайте о мероприятиях <a href="<?= get_home_url(); ?>">A-level</a> больше в
                        группе <a
                            href="<?= get_option('vkontakte_url'); ?>">ВКонтакте</a> и <a
                            href="<?= get_option('facebook_url'); ?>">Фейсбуке</a></p>
                </div>

                <div class="sitebar hide-xs">
                    <form action="">
                        <img src="<?= get_template_directory_uri() ?>/images/mail-icon.png" alt="">
                        <p>Наши лучшие материалы <br>раз в неделю на ваш e-mail</p>
                        <input type="text" placeholder="Ваше имя">
                        <input type="email" placeholder="E-mail address">
                        <input type="submit" value="Подписаться">
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="container-fluid visible-xs">
        <div class="row">
            <div class=" col-xs-12 ">
                <div class="row">
                    <h2 class="title cont"> contact us <span> контакты </span></h2>
                    <div class="contact ">
                        <ul>
                            <li>
                                <i class="ic-email-outline"></i>
                                <div>
                                    <h5> e - mail</h5>
                                    <a href="mailto:<?= get_option('admin_email') ?>"><?= get_option('admin_email') ?></a>
                                </div>
                            </li>
                            <li>
                                <i class="ic-phone"></i>
                                <div>
                                    <h5>телефоны</h5>
                                    <?php if (get_option('phone1')) : ?>
                                        <a href="tel:<?= get_option('phone1'); ?>"><?= get_option('phone1'); ?></a>
                                    <?php endif; ?>
                                    <?php if (get_option('phone2')) : ?>
                                        <a href="tel:<?= get_option('phone2'); ?>"><?= get_option('phone2'); ?></a>
                                    <?php endif; ?>
                                </div>
                            </li>
                            <li>
                                <i class="ic-map-marker"></i>
                                <div>
                                    <h5>Адрес</h5>
                                    <a href="https://www.google.com.ua/maps/place/43B,+%D0%B2%D1%83%D0%BB%D0%B8%D1%86%D1%8F+%D0%A1%D1%83%D0%BC%D1%81%D1%8C%D0%BA%D0%B0,+43%D0%91,+%D0%A5%D0%B0%D1%80%D0%BA%D1%96%D0%B2,+%D0%A5%D0%B0%D1%80%D0%BA%D1%96%D0%B2%D1%81%D1%8C%D0%BA%D0%B0+%D0%BE%D0%B1%D0%BB%D0%B0%D1%81%D1%82%D1%8C/@50.006387,36.2347861,18z/data=!3m1!4b1!4m5!3m4!1s0x4127a0de435d1657:0x92f43e0454946df2!8m2!3d50.0063853!4d36.2358804?hl=ru"
                                       target="_blank">ул.<?= get_option('address'); ?></a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="contact_map" class="visible-xs"></div>

</section>

<?php get_footer(); ?>
