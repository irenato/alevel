<?php if(!is_page_template('landing.php')  && !is_page_template('landing_mc.php') && !is_page_template('hub.php')): ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" id="viewport"
          content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta property="og:title" content="<?= get_the_title() ?>"/>
    <meta property="og:image" content="<?= get_the_post_thumbnail_url() ?>"/>
    <meta property="og:url" content="<?= get_permalink() ?>"/>
    <meta property="og:type" content="website"/>
    <meta name="twitter:card" content="summary"/>  <!-- Тип окна -->
    <meta name="twitter:site" content="A-Level"/>
    <meta name="twitter:title" content="<?= get_the_title() ?>">
    <meta name="twitter:creator" content="A-Level"/>
    <meta name="twitter:image:src" content="<?= get_permalink() ?>"/>
    <meta name="twitter:domain" content="a-level.com"/>
    <title>
        <?= wp_get_document_title('|', true, 'right'); ?></title>

    <style>
        .new_preloader {
            display: flex;
            justify-content: center;
            align-items: center;
            position: fixed;
            width: 100vw;
            height: 100vh;
            z-index: 99999;
            background-color: rgba(255,255,255,1);
        } 
        @media (max-width:727px) { 
             .new_preloader {
                flex-direction: column;            
            }  
            .new_preloader img:first-child {
                width: 220px;
            }
            .new_preloader img:last-child {
                width: 80px;
            }
        }
    </style>    

    <script src="https://apis.google.com/js/platform.js" async defer>
        {
            lang: 'ru'
        }
    </script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-77235759-1', 'auto');
        ga('send', 'pageview');
    </script>
    
</head>
<body id="top">
<div id="fb-root"></div>
<div class="new_preloader">
        <img src="<?= get_option('logo') ?>" alt="#">
        <img src="<?= get_template_directory_uri() ?>/images/gears.svg" alt="Preloader image">
</div>
<?php wp_head(); ?>
<?php header('Access-Control-Allow-Origin: *'); ?>
<!--START HEADER-->
<header <?= is_home() ? '' : "class='internal-pages'" ?>>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="logo">
                    <a href="<?= get_home_url(); ?>">
                        <img src="<?= get_option('logo') ?>" alt="#">
                    </a>
                </div>
                <ul class="menu">
                    <li class="modal"><i class="ic-application"></i>заявка</li>
                    <li class="mnu">
                        <div class="hamburger hamburger--spring js-hamburger">
                            <div class="hamburger-box">
                                <div class="hamburger-inner"></div>
                            </div>
                        </div>
                        Меню
                    </li>
                    <li id="teens"><a href="http://a-level-teens.com.ua/" target="_blank">A-Level Teens</a></li>
                    <li id="hub"><a href="http://a-level.com.ua/hub" target="_blank">A-Level HUB</a></li>
                </ul>
                <?php if (is_home()) : ?>
                    <?php wp_nav_menu(array(
                        'theme_location' => '',
                        'menu' => '2',
                        'container' => false,
                        'container_class' => '',
                        'container_id' => '',
                        'menu_class' => 'menu-buttom',
                        'menu_id' => '',
                        'echo' => true,
                        'fallback_cb' => 'wp_page_menu',
                        'before' => '',
                        'after' => '',
                        'link_before' => '',
                        'link_after' => '',
                        'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                        'depth' => 0,
                        'walker' => '',
                    )); ?>
                <?php else : ?>
                    <?php if (checkNews())
                        $menu = 7;
                    else
                        $menu = 15; ?>
                    <?php wp_nav_menu(array(
                        'theme_location' => '',
                        'menu' => $menu,
                        'container' => false,
                        'container_class' => '',
                        'container_id' => '',
                        'menu_class' => 'menu-buttom',
                        'menu_id' => '',
                        'echo' => true,
                        'fallback_cb' => 'wp_page_menu',
                        'before' => '',
                        'after' => '',
                        'link_before' => '',
                        'link_after' => '',
                        'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                        'depth' => 0,
                        'walker' => '',
                    )); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</header>


<div class="submenu">

    <div class="container">
        <div class="hamburger is-active hamburger--spring js-hamburger">
            <div class="hamburger-box">
                <div class="hamburger-inner"></div>
            </div>
        </div>
        <div class="header-block-left">
            <h3 class="title"><a href="<?= get_home_url(); ?>">A-level ukraine</a></h3>
            <?php if (checkNews())
                $menu = 9;
            else
                $menu = 14;
            ?>
            <?php wp_nav_menu(array(
                'theme_location' => '',
                'menu' => $menu,
                'container' => false,
                'container_class' => '',
                'container_id' => '',
                'menu_class' => '',
                'menu_id' => '',
                'echo' => true,
                'fallback_cb' => 'wp_page_menu',
                'before' => '',
                'after' => '',
                'link_before' => '',
                'link_after' => '',
                'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                'depth' => 0,
                'walker' => '',
            )); ?>
        </div>


        <div class="header-block-center">
            <h3 class="title">Направления обучения</h3>
            <ul>
                <?php $args = array(
                    'numberposts' => 0, // тоже самое что posts_per_page
                    'offset' => 0,
                    'post_type' => 'courses',
                    'orderby' => 'ID',
                    'order' => 'ASC',
                    'posts_per_page' => 0); ?>
                <?php $post_courses = new WP_query($args); ?>
                <?php while ($post_courses->have_posts()) : $post_courses->the_post(); ?>
                    <li><a href="<?= get_the_permalink() ?>"><?= get_the_title() ?></a></li>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            </ul>
        </div>

        <div class="header-block-right">
            <h3 class="title">Контакты</h3>
            <ul class="contacts">
                <li>
                    <i class="ic-phone"></i>
                    <div>
                        <a href="tel:<?= get_option('phone1'); ?>"><?= get_option('phone1'); ?></a>
                        <a href="tel:<?= get_option('phone2'); ?>"><?= get_option('phone2'); ?></a>
                    </div>
                </li>
                <li>
                    <i class="ic-telegram"></i>
                    <a href="mailto:<?= get_option('admin_email'); ?>"><?= get_option('admin_email'); ?></a>
                </li>
                <li>
                    <i class="ic-map-marker"></i>
                    <div>
                        <a href="https://www.google.com.ua/maps/place/43B,+%D0%B2%D1%83%D0%BB%D0%B8%D1%86%D1%8F+%D0%A1%D1%83%D0%BC%D1%81%D1%8C%D0%BA%D0%B0,+43%D0%91,+%D0%A5%D0%B0%D1%80%D0%BA%D1%96%D0%B2,+%D0%A5%D0%B0%D1%80%D0%BA%D1%96%D0%B2%D1%81%D1%8C%D0%BA%D0%B0+%D0%BE%D0%B1%D0%BB%D0%B0%D1%81%D1%82%D1%8C/@50.006387,36.2347861,18z/data=!3m1!4b1!4m5!3m4!1s0x4127a0de435d1657:0x92f43e0454946df2!8m2!3d50.0063853!4d36.2358804?hl=ru"
                           target="_blank">г. <?= get_option('city'); ?> <span>ул, <?= get_option('address'); ?></span></a>
                    </div>
                </li>
            </ul>
        </div>
        
    </div>
</div>

<?php elseif(is_page_template('hub.php')): ?>
    <?php get_template_part( 'a-level-hub/hub-header' ); ?>
<?php else: ?>
    <?php get_template_part( 'landings/header-landing' );  ?>
<?php endif; ?>
<!--END HEADER-->

