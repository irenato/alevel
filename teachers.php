<?php
/**
 * Template name: Teachers page
 */

get_header();
?>

    <!--START CONTENT-->
    <section class="inner-teacher">

        <div class="container-fluid hide-xs">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="practice">
                            <div class="img">
                                <img src="<?= get_template_directory_uri() ?>/images/hexagon_team.png" alt=""
                                     class="photo">
                            </div>

                            <div class="container">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <p class="text">У нас преподают исключительно практики с большим опытом работы в
                                        своей сфере. Но для нас и этого мало. Перед началом работы, преподаватели
                                        проходят подготовительные курсы и ряд тестовых занятий, чтобы мы могли
                                        гарантировать нашим студентам комфортное образование.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="teachers">
                        <h2 class="title visible-xs">teachers<span>преподы</span></h2>
                        <?php $args = array(
                            'offset' => 0,
                            'post_type' => 'teachers ',
                            'posts_per_page' => -1); ?>
                        <?php $i = 0; ?>
                        <?php $post_teachers = new WP_query($args); ?>
                        <?php while ($post_teachers->have_posts()) : $post_teachers->the_post(); ?>
                            <?php $i++; ?>
                            <div class="teacher <?= get_field('first_letter') ?>" title-name="<?= get_field('first_letter') ?>">
                                <div class="img" title-name-2="<?= get_field('first_letter') ?>">
                                    <img src="<?= get_the_post_thumbnail_url($post_teachers->ID, 'thumbnail') ?>" alt="<?= get_the_title(); ?>">
                                </div>
                                <h4 class="name"><?= get_the_title(); ?></h4>
                                <h5 class="course"><?= get_field('course') ?></h5>
                                <div class="hover">
                                    <div class="description">
                                        <p><?= get_field('description') ?></p>
                                    </div>
                                </div>
                            </div>
                            <?php if ($i % 3 == 0 || $i == count($post_teachers->posts)) : ?>
                                <a href="#" class="button become-our-teachers">стать преподом</a>
                            <?php endif; ?>
                        <?php endwhile; ?>
                        <?php wp_reset_postdata(); ?>

                    </div>
                </div>
            </div>
        </div>

        <?php $courses = array(); ?>
        <?php $args = array(
            'numberposts' => 0, // тоже самое что posts_per_page
            'offset' => 0,
            'post_type' => 'courses',
            'orderby' => 'ID',
            'order' => 'ASC',
            'posts_per_page' => 0); ?>
        <?php $post_courses = new WP_query($args); ?>
        <?php while ($post_courses->have_posts()) : $post_courses->the_post(); ?>
            <?php array_push($courses, get_the_title()); ?>
        <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>

        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h2 class="title" id="enroll-in-a-course">join us<span>присоединяйся к нам</span></h2>
                    <form action="#" class="enroll-in-a-course">
                        <p>Я,</p>
                        <div class="input">
                            <input type="text" placeholder="Имя и Фамилия" class="student-name">
                            <label for="#"></label>
                        </div>
                        <p>, хочу записаться</p>
                        <p>в <span>A</span>-Level <b>Ukraine</b> на курc</p>
                        <div class="select">
                            <select name="#" id="#" class="custom-select sources" placeholder="Хочу курс">
                                <option value="#"></option>
                                <?php foreach ($courses as $course) : ; ?>
                                    <option value="#"><?= $course; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <label for=""></label>
                        </div>
                        <p>Свяжитесь со мной по телефону</p>
                        <div class="input">
                            <input type="tel" class="phone-mask">
                            <label for="#"></label>
                        </div>
                        <p>или по e-mail</p>
                        <div class="input">
                            <input type="email" placeholder="e-mail" class="student-email">
                            <label for="#"></label>
                        </div>
                        <span class="text-danger errors-on-main-form" style="display: none;"></span>
                        <input type="submit" class="button wonna-study" value="отправить заявку">
                    </form>


                    <form action="#" class="enroll-in-a-course-mobile">
                        <div class="input">
                            <input type="text" placeholder="Имя и Фамилия">
                            <label for="#"></label>
                        </div>
                        <div class="select">
                            <select name="#" id="#" class="custom-select sources" placeholder="Курс">
                                <option value="#"></option>
                                <?php foreach ($courses as $course) : ; ?>
                                    <option value="#"><?= $course; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <label for=""></label>
                        </div>
                        <div class="input">
                            <input type="email" placeholder="E-mail">
                            <label for="#"></label>
                        </div>
                        <div class="input">
                            <input type="tel" placeholder="Телефон">
                            <label for="#"></label>
                        </div>
                    </form>
                    <input type="submit" class="button mobile-form" value="отправить">
                </div>
            </div>
        </div>


        <!--    Partners -->
        <?php get_template_part( 'slider-partners' );  ?>


        <div class="container-fluid visible-xs">
            <div class="row">
                <div class=" col-xs-12 ">
                    <div class="row">
                        <h2 class="title cont">contact us<span> контакты</span></h2>
                        <div class="contact ">
                            <ul>
                                <li>
                                    <i class="ic-email-outline"></i>
                                    <div>
                                        <h5>e-mail</h5>
                                        <a href="mailto:<?= get_option('admin_email') ?>"><?= get_option('admin_email') ?></a>
                                    </div>
                                </li>
                                <li>
                                    <i class="ic-phone"></i>
                                    <div>
                                        <h5>телефоны</h5>
                                        <?php if (get_option('phone1')) : ?>
                                            <a href="tel:<?= get_option('phone1'); ?>"><?= get_option('phone1'); ?></a>
                                        <?php endif; ?>
                                        <?php if (get_option('phone2')) : ?>
                                            <a href="tel:<?= get_option('phone2'); ?>"><?= get_option('phone2'); ?></a>
                                        <?php endif; ?>
                                    </div>
                                </li>
                                <li>
                                    <i class="ic-map-marker"></i>
                                    <div>
                                        <h5>Адрес</h5>
                                        <a href="https://www.google.com.ua/maps/place/43B,+%D0%B2%D1%83%D0%BB%D0%B8%D1%86%D1%8F+%D0%A1%D1%83%D0%BC%D1%81%D1%8C%D0%BA%D0%B0,+43%D0%91,+%D0%A5%D0%B0%D1%80%D0%BA%D1%96%D0%B2,+%D0%A5%D0%B0%D1%80%D0%BA%D1%96%D0%B2%D1%81%D1%8C%D0%BA%D0%B0+%D0%BE%D0%B1%D0%BB%D0%B0%D1%81%D1%82%D1%8C/@50.006387,36.2347861,18z/data=!3m1!4b1!4m5!3m4!1s0x4127a0de435d1657:0x92f43e0454946df2!8m2!3d50.0063853!4d36.2358804?hl=ru"
                                           target="_blank">ул.<?= get_option('address'); ?></a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="contact_map" class="visible-xs"></div>
    </section>
    <!--START CONTENT-->

<?php
get_footer();
