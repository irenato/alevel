<?php
/**
 * Template name: Single-courses page
 */

get_header();
if (have_posts()) : while (have_posts()) :
the_post();
$post_id = $post->ID; ?>

<section class="courses">

    <div class="container" id="about-School" ng-app="app" ng-controller="RatingController as rating">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="title-rating">
                    <h3 class="title"><?php the_title(); ?></h3>
                    <div class="clearfix"></div>
                    <div>
                        <p>Сложность:</p>
                        <ul class='hexagon'>
                            <?php for ($i = 1; $i <= 5; $i++) : ?>
                                <li>
                                    <i class="ic-hexagon<?= $i <= (int)get_field('difficulty_level') ? '' : '-outline' ?>"></i>
                                </li>
                            <?php endfor; ?>
                        </ul>
                    </div>
                </div>

                <div class="aboutSchoolCont">
                    <div class="aboutSchoolIn">
                        <p><?= get_field('course_description') ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="when">
                    <h4 class="title">Когда</h4>
                    <ul>
                        <li>
                            <i class="ic-calendar"></i>
                            <p><?= get_field('course_duration') ?>
                                <span>
<!--                                    --><?//= get_field('lessons_start') ?>
                                </span>
                            </p>
                        </li>
                        <li>
                            <i class="ic-clock"></i>
                            <p><?= mb_strtoupper(get_field('course_schedule_days')) ?>
                                <span><?= get_field('course_schedule_time') ?></span></p>
                        </li>
                    </ul>
                </div>
                <div class="for-whom">
                    <h4 class="title">Для кого</h4>
                    <ul>
                        <?php $subfields = get_field('for_whom_this_course'); ?>
                        <?php if ($subfields) : ?>
                            <?php foreach ($subfields as $item) : ?>
                                <li><?= $item['for_whom_this_course_item'] ?></li>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 hide-xs">
                <div class="for-whom before-the-course">
                    <h4 class="title">До начала курса</h4>
                    <div class="group-set">
                        <?php $free_places = (int)get_field('expected_count_of_students') - (int)get_field('current_count_of_students'); ?>
                        <?php if ($free_places == 1) {
                            $string = 'место';
                        } elseif (1 < $free_places && $free_places < 5) {
                            $string = 'места';
                        } else {
                            $string = 'мест';
                        } ?>
                        <h3><?= $free_places ?><span>осталось <br> <?= $string ?></span></h3>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2 class="title">teachers<span>преподаватели курса</span></h2>

                <div class="teachers">
                    <?php $connected = new WP_Query(array(
                        'connected_type' => 'courses_to_teachers',
                        'connected_items' => get_queried_object(),
                        'nopaging' => true,
                    ));
                    $i = 0;
                    ?>
                    <?php if ($connected->have_posts()) : while ($connected->have_posts()) : $connected->the_post(); ?>
                        <?php $i++; ?>
                        <div class="teacher  <?= ($i == count($connected->posts) && $i % 2 !== 0) ? 'last' : ' ' ?> <?= get_field('first_letter') ?>"
                             title-name="<?= get_field('first_letter') ?>">
                            <div class="img" title-name-2="<?= get_field('first_letter') ?>">
                                <img src="<?= get_the_post_thumbnail_url() ?>" alt="<?= get_the_title() ?>">
                            </div>

                            <h4 class="name"><?= get_the_title() ?></h4>
                            <h5 class="course"><?= get_field('course') ?></h5>
                            <div class="hover">
                                <div class="description">
                                    <p><?= get_field('description') ?></p>
                                </div>
                            </div>
                        </div>

                    <?php endwhile; ?>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="course-program">
                    <a href="#" class="button top get-course">записаться на курс</a>

                    <h3 class="title">Программа курса</h3>
                    <?php $i = 0;
                    $course_programs = get_field('course_program', $post_id);
                    if ($course_programs) :
                        foreach ($course_programs as $course_program) : ?>
                            <?php
                            $course_info = '';
                            $i++; ?>
                            <div class="block">
                                <div class="block-left">
                                    <h2 class="month"><?= $course_program['course_interval'] ?></h2>
                                    <p><?= $course_program['course_interval_description'] ?></p>
                                </div>
                                <div class="block-right">
                                    <h5 class="title">практика</h5>
                                    <ul>
                                        <?php $practics = $course_program['course_interval_practice'] ?>
                                        <?php if ($practics) : ?>
                                            <?php foreach ($practics as $practic) : ?>
                                                <li><?= $practic['course_interval_practice_item'] ?></li>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </ul>


                                    <h5 class="title">результат</h5>
                                    <ul>
                                        <?php $results = $course_program['course_interval_result'] ?>
                                        <?php if ($results) : ?>
                                            <?php foreach ($results as $result) : ?>
                                                <li><?= $result['course_interval_result_item'] ?></li>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </ul>
                                    <?php $course_info .= "<h3 class='title'> Изучаемая теория за " . $course_program['course_interval'] . "</h3>";
                                    $course_info .= "<ul>";
                                    $theories = $course_program['course_interval_theory'];
                                    if ($theories) {
                                        foreach ($theories as $theory) {
                                            $course_info .= "<li>" . $theory['course_interval_theory_item'] . "</li>";
                                        }
                                        $course_info .= "</ul>";
                                    }
                                    ?>
                                    <a href="#" data-info="<?= $course_info ?>" class="button">вся теория
                                        за <?= $course_program['course_interval'] ?></a>
                                    <?php $course_info = ''; ?>
                                </div>
                            </div>
                            <div class="mobile-block visible-xs">
                                <h5 class="title"><?= $course_program['course_interval'] ?></h5>
                                <ul><?php if ($theories) :
                                        foreach ($theories as $theory) : ?>
                                            <li> <?= $theory['course_interval_theory_item'] ?> </li>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </ul>
                            </div>
                            <?php if ($i < count($course_programs)) : ?>
                                <ul class="icon">
                                    <li><i class="ic-hexagon-outline"></i></li>
                                    <li><i class="ic-hexagon-outline"></i></li>
                                    <li><i class="ic-hexagon-outline"></i></li>
                                </ul>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>

                    <div class="block-bottom">
                        <h4 class="title">Стоимость</h4>
                        <ul>
                            <li>
                                <i class="ic-money"></i>
                                <p><?= get_field('course_price', $post_id) ?>
                                    <span><?= get_field('period_of_paid', $post_id) == 'per_course' ? 'за весь курс' : 'в месяц' ?></span>
                                </p>
                            </li>
                            <li>
                                <i class="ic-people"></i>
                                <p><?= get_field('expected_count_of_students', $post_id) ?>
                                    человек<span>в группе</span></p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Фома обратной связи(начало) -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2 class="title" id="enroll-in-a-course">Check in<span>записаться на курс</span>
                </h2>
                <form action="#" class="enroll-in-a-course">
                    <p>Я,</p>
                    <div class="input">
                        <input type="text" placeholder="Имя и Фамилия" class="student-name">
                        <label for="#"></label>
                    </div>
                    <p>, хочу записаться</p>
                    <p>в <span>A</span>-Level <b>Ukraine</b> на курc</p>
                    <div class="select">
                        <select name="#" id="#" class="custom-select sources" placeholder="Хочу курс">
                            <option value="#"></option>
                            <?php createOptionsCourses(); ?>
                        </select>
                        <label for=""></label>
                    </div>
                    <p>Свяжитесь со мной по телефону</p>
                    <div class="input">
                        <input type="tel" class="phone-mask">
                        <label for="#"></label>
                    </div>
                    <p>или по e-mail</p>
                    <div class="input">
                        <input type="email" placeholder="e-mail" class="student-email">
                        <label for="#"></label>
                    </div>
                    <span class="text-danger errors-on-main-form" style="display: none;"></span>
                    <input type="submit" class="button wonna-study" value="отправить заявку">
                    <p class="min-text">*Вы принимаете политику конфиденциальности. Ваш адрес электронной почты и ФИО ни в коем случае не будут переданы другим пользователям и никогда не будут открыты для просмотра. Мы не будем передавать эти данные третьим лицам и/или любым другим способом сообщать эти данные третьим лицам.</p>
                </form>

                <form action="#" class="enroll-in-a-course-mobile">
                    <div class="input">
                        <input type="text" placeholder="Имя и Фамилия">
                        <label for="#"></label>
                    </div>
                    <div class="select">
                        <select name="#" id="#" class="custom-select sources" placeholder="Курс">
                            <option value="#"></option>
                            <?php createOptionsCourses(); ?>
                        </select>
                        <label for=""></label>
                    </div>
                    <div class="input">
                        <input type="email" placeholder="E-mail">
                        <label for="#"></label>
                    </div>
                    <div class="input">
                        <input type="tel" placeholder="Телефон" class="phone-mask">
                        <label for="#"></label>
                    </div>
                    <p class="min-text">*Вы принимаете политику конфиденциальности. Ваш адрес электронной почты и ФИО ни в коем случае не будут переданы другим пользователям и никогда не будут открыты для просмотра. Мы не будем передавать эти данные третьим лицам и/или любым другим способом сообщать эти данные третьим лицам.</p>
                </form>
                <input type="submit" class="button mobile-form" value="отправить">

            </div>
        </div>
    </div>
    <!-- Фома обратной связи(конец) -->

    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 hide-xs">
                <?php $course_address = new WP_Query(array(
                    'connected_type' => 'courses_to_addresses',
                    'connected_items' => 2705,
                    'nopaging' => true,
                )); ?>
                <?php if ($course_address->have_posts()) :
                while ($course_address->have_posts()) : $course_address->the_post(); ?>
                <div class="place-of-training"
                     style="background-image: url(<?= get_the_post_thumbnail_url(); ?>)">
                    <div class="description">
                        <h3 class="title">Место<br> обучения</h3>
                        <h4 class="title">A-level <span>Ukraine</span> <b><?= get_the_title(); ?></b></h4>

                        <a href="<?= get_field('google_api') ?>" target="_blank" class="button">на карте</a>
                        <?php endwhile;; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container-fluid visible-xs">
        <div class="row">
            <div class=" col-xs-12 ">
                <div class="row">
                    <h2 class="title cont">contact us<span> контакты</span></h2>
                    <div class="contact ">
                        <ul>
                            <li>
                                <i class="ic-email-outline"></i>
                                <div>
                                    <h5>e-mail</h5>
                                    <a href="mailto:<?= get_option('admin_email') ?>"><?= get_option('admin_email') ?></a>
                                </div>
                            </li>
                            <li>
                                <i class="ic-phone"></i>
                                <div>
                                    <h5>телефоны</h5>
                                    <?php if (get_option('phone1')) : ?>
                                        <a href="tel:<?= get_option('phone1'); ?>"><?= get_option('phone1'); ?></a>
                                    <?php endif; ?>
                                    <?php if (get_option('phone2')) : ?>
                                        <a href="tel:<?= get_option('phone2'); ?>"><?= get_option('phone2'); ?></a>
                                    <?php endif; ?>
                                </div>
                            </li>
                            <li>
                                <i class="ic-map-marker"></i>
                                <div>
                                    <h5>Адрес</h5>
                                    <a href="https://www.google.com.ua/maps/place/43B,+%D0%B2%D1%83%D0%BB%D0%B8%D1%86%D1%8F+%D0%A1%D1%83%D0%BC%D1%81%D1%8C%D0%BA%D0%B0,+43%D0%91,+%D0%A5%D0%B0%D1%80%D0%BA%D1%96%D0%B2,+%D0%A5%D0%B0%D1%80%D0%BA%D1%96%D0%B2%D1%81%D1%8C%D0%BA%D0%B0+%D0%BE%D0%B1%D0%BB%D0%B0%D1%81%D1%82%D1%8C/@50.006387,36.2347861,18z/data=!3m1!4b1!4m5!3m4!1s0x4127a0de435d1657:0x92f43e0454946df2!8m2!3d50.0063853!4d36.2358804?hl=ru"
                                       target="_blank">ул.<?= get_option('address'); ?></a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="contact_map" class="visible-xs"></div>

</section>

<?php endwhile; ?>
<?php endif; ?>
<section class="modal-container-curses">

    <div class="content">
        <div class="hamburger is-active hamburger--spring js-hamburger">
            <div class="hamburger-box">
                <div class="hamburger-inner"></div>
            </div>
        </div>
        <div class="course-info">
            <!--            <h3 class="title">Изучаемая теория за 1-й месяц</h3>-->
            <!--            <ul>-->
            <!--                <li>Урок 1: Интервьирование клиента;</li>-->
            <!--                <li>Урок 2: Проверка идеи;</li>-->
            <!--                <li>Урок 3: Анализ рынка;</li>-->
            <!--                <li>Урок 4: Cпособы монетизации продукта;</li>-->
            <!--                <li>Урок 5: Исследование ЦА;</li>-->
            <!--                <li>Урок 6: Создание карт эмпатии;</li>-->
            <!--                <li>Урок 7: Создание персонажей;</li>-->
            <!--                <li>Урок 8: Создание сценариев взаимодействия;</li>-->
            <!--                <li>Урок 9: Создание карты сценариев взаимодействия.</li>-->
            <!--            </ul>-->
        </div>
    </div>
</section>
<?php get_footer(); ?>

