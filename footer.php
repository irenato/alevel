<!--START FOOTER-->
<?php if(!is_page_template('landing.php') &&  !is_page_template('landing_mc.php') && !is_page_template('hub.php')): ?>
<footer data-coordinates="<?= get_option('coordinates') ?>">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="arrow-top"><a href="#top"><i class="ic-chevron-right"></i></a></div>


                <div class="footer-block-left">
                    <div class="logo"><a href="<?= get_home_url(); ?>"><img src="<?= get_template_directory_uri() ?>/images/logo.png" alt=""></a>
                    </div>
                    <h3 class="title">A-level ukraine</h3>
                    <?php if (checkNews())
                        $menu = 9;
                    else
                        $menu = 14;
                    ?>
                    <?php wp_nav_menu(array(
                        'theme_location' => '',
                        'menu' => $menu,
                        'container' => false,
                        'container_class' => '',
                        'container_id' => '',
                        'menu_class' => '',
                        'menu_id' => '',
                        'echo' => true,
                        'fallback_cb' => 'wp_page_menu',
                        'before' => '',
                        'after' => '',
                        'link_before' => '',
                        'link_after' => '',
                        'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                        'depth' => 0,
                        'walker' => '',
                    )); ?>
                </div>


                <div class="footer-block-center">
                    <h3 class="title">Направления обучения</h3>
                    <ul>
                        <?php $courses = array(); ?>
                        <?php $args = array(
                            'offset' => 0,
                            'post_type' => 'courses',
                            'orderby' => 'ID',
                            'order' => 'ASC',
                            'posts_per_page' => -1); ?>
                        <?php $post_courses = new WP_query($args); ?>
                        <?php while ($post_courses->have_posts()) : $post_courses->the_post(); ?>
                            <?php array_push($courses, get_the_title()); ?>
                            <li><a href="<?= get_the_permalink() ?>"><?= get_the_title() ?></a></li>
                        <?php endwhile; ?>
                        <?php wp_reset_postdata(); ?>
                    </ul>
                </div>


                <div class="footer-block-right">
                    <h3 class="title">Контакты</h3>
                    <ul class="contacts">
                        <li>
                            <i class="ic-phone"></i>
                            <div>
                                <?php if (get_option('phone1')) : ?>
                                    <a href="tel:<?= get_option('phone1'); ?>"><?= get_option('phone1'); ?></a>
                                <?php endif; ?>
                                <?php if (get_option('phone2')) : ?>
                                    <a href="tel:<?= get_option('phone2'); ?>"><?= get_option('phone2'); ?></a>
                                <?php endif; ?>
                            </div>
                        </li>
                        <li class="request-a-call">
                            <a href="#enroll-in-a-course">заказать звонок</a>
                        </li>
                        <li>
                            <i class="ic-telegram"></i>
                            <a href="mailto:<?= get_option('admin_email') ?>"><?= get_option('admin_email') ?></a>
                        </li>
                        <li>
                            <i class="ic-map-marker"></i>
                            <div>
                                <a href="<?= get_option('googlemaps') ?>"
                                   target="_blank">г. <?= get_option('city'); ?>
                                    <span><?= get_option('address'); ?></span></a>
                            </div>
                        </li>
                    </ul>
                    <h3 class="title">Мы в соц.сетях</h3>
                    <ul class="soc">                       
                        <?php if (get_option('facebook_url')) : ?>
                            <li><a href="<?= get_option('facebook_url'); ?>" target="_blank"><i class="ic-facebook"></i></a>
                            </li>
                        <?php endif; ?>
                        <?php if (get_option('pinterest_url')) : ?>
                            <li><a href="<?= get_option('pinterest_url'); ?>" target="_blank"><i
                                        class="ic-pinterest"></i></a></li>
                        <?php endif; ?>
                        <?php if (get_option('behance_url')) : ?>
                            <li><a href="<?= get_option('behance_url'); ?>" target="_blank"><i
                                        class="ic-behance"></i></a></li>
                        <?php endif; ?>
                    </ul>
                </div>


                <h4 class="title">Оставьте свой e-mail,что бы получать самые интересные новости мира IT</h4>

                <!--<?= do_shortcode('[mc4wp_form id="194"]'); ?>-->


            </div>
        </div>
    </div>
</footer>
<!--END FOOTER-->

<?php include('modals.php'); ?>

<?php wp_footer(); ?>

<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 882483008;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/882483008/?guid=ON&amp;script=0"/>
    </div>
</noscript>
 <script type="text/javascript">
        $(window).on('load', function () {
            var $preloader = $('.new_preloader');           
            $preloader.delay(500).fadeOut('slow');
        });
    </script>

</body>
</html>
<?php elseif(is_page_template('hub.php')): ?>
    <?php get_template_part( 'a-level-hub/hub-footer' ); ?>
<?php else: ?>
    <?php get_template_part( 'landings/footer-landing' );  ?>
<?php endif; ?>
