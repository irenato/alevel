<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 01.04.17
 * Time: 22:09
 */
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= wp_get_document_title('|', true, 'right'); ?></title>
    <?php wp_head(); ?>
</head>
<body>
<!-- Здесь пишем код -->

<header>
    <div class="wrapper">
        <div class="row">
            <a href="/" class="logo">
                <img src="<?= get_template_directory_uri() ?>/images/hub/logo.png" alt="A-level-Hub logo">
            </a>
            <ul class="header-menu mobile-hidden">
                <li><a href="#about_us">о нас</a></li>
                <li><a href="#scroll_galery">галерея</a></li>
                <li><a href="#cost_scroll">стоимость</a></li>
                <li><a href="#contacts">контакты</a></li>
            </ul>
            <div class="header-contacts mobile-hidden">
                <ul class="phone-list">
                    <?php $phones = get_field('phones') ?>
                    <?php if ($phones): ?>
                        <?php foreach ($phones as $item): ?>
                            <li><?= $item['phone'] ?></li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
                <a href="#invite-to-hub-scroll" class="invite-btn">записаться на экскурсию</a>
            </div>
            <button class="mobile-visible" id="mobile-menu-btn">меню <i class="fa fa-bars" aria-hidden="true"></i>
            </button>
        </div>
    </div>
</header>