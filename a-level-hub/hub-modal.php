<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 01.04.17
 * Time: 22:09
 */

?>

<div class="hidden">

    <div class="thanks-popup">
        <div class="wrapper">
            <div class="con">
                <img src="<?= get_template_directory_uri() ?>/images/hub/thnks-img.png" alt="A-level">
                <p>спасибо</p>
                <p>ваша заявка принята, ожидайте ответ</p>
                <button>вернуться на сайт</button>
            </div>
        </div>
    </div>

    <section class="invite-to-hub" id="invite-to-hub">
        <div class="wrapper">
            <div class="row">
                <h2>Заполните форму</h2>
                <form action="/" method="post" class="new-order">
                    <div class="row-inputs">
                        <div class="input-container">
                            <input type="text" name="name" placeholder="Имя и Фамилия">
                            <p class="error-message">Введите имя и фамилию</p>
                        </div>
                        <div class="input-container">
                            <input type="email" name="email" placeholder="E-Mail">
                            <p class="error-message">Email введен не верно</p>
                        </div>
                        <div class="input-container">
                            <input type="text" name="phone" placeholder="Телефон">
                            <p class="error-message">Телефон введен неверно</p>
                        </div>
                    </div>
                    <div class="row-inputs">
                        <div class="input-container">
                            <input id="datepicker" type="text" name="date" placeholder="Выберите дату">
                            <p class="error-message">Выбранная дата неверна</p>
                        </div>
                        <div class="input-container">
                            <div class="timepicker">
                                c <input id="timepicker-from" name="time-from" type="text"> - до <input id="timepicker-to" name="time-to" type="text">
                            </div>
                            <p class="error-message">Это поле не может быть пустым</p>
                        </div>
                    </div>
                    <button type="submit" class="submit-invite-form">Отправить заявку</button>
                </form>
            </div>
        </div>
    </section>

    <div class="mobile-main-menu">
        <div class="wrapper">
            <ul>
                <li><a href="#about-us"><i class="fa fa-home" aria-hidden="true"></i> О нас</a></li>
                <li><a href="#courses"><i class="fa fa-graduation-cap" aria-hidden="true"></i> Галерея</a></li>
                <li><a href="#cost"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Стоимость</a></li>
                <li><a href="#contacts"><i class="fa fa-map-marker" aria-hidden="true"></i> Контакты</a></li>
            </ul>
        </div>
    </div>


</div>
