<?php

include('settings/admin.php');
include('settings/custom_posts.php');
include('settings/remove_menu_items.php');
include('settings/custom_menus.php');
include('settings/ajax_functions.php');
include('settings/admin_students.php');
include('settings/admin_reviews.php');
include('settings/admin_teachers.php');
include('settings/admin_partners.php');
include('settings/open_doors_admin.php');
include('settings/scripts_and_styles.php');
include('settings/open_doors_functions.php');
include('settings/hub_admin.php');
include('settings/admin_mc.php');
//include('lib/PHPExcel/Classes/PHPExcel.php');

add_theme_support('post-thumbnails');

function themeslug_theme_customizer($wp_customize)
{
    $wp_customize->add_section('themeslug_logo_section', array(
        'title' => __('Logo', 'themeslug'),
        'priority' => 30,
        'description' => 'Upload a logo to replace the default site name and description in the header',
    ));

    $wp_customize->add_setting('themeslug_logo');
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'themeslug_logo', array(
        'label' => __('Logo', 'themeslug'),
        'section' => 'themeslug_logo_section',
        'settings' => 'themeslug_logo',
    )));
}

add_action('customize_register', 'themeslug_theme_customizer');

function createOptionsCourses()
{
    $args = array(
        'numberposts' => 0,
        'offset' => 0,
        'post_type' => 'courses',
        'posts_per_page' => -1);
    $post_courses = new WP_query($args);
    while ($post_courses->have_posts()) : $post_courses->the_post(); ?>
        <option value="#"><?= get_the_title(); ?></option>
    <?php endwhile;
}

function checkNews()
{
    $args = array(
        'cat' => '4,6,1,5'
    );
    $posts = get_posts($args);
    if ($posts)
        return true;
    return false;
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
