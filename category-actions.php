<?php

get_header();

$paged = get_query_var('paged') ? get_query_var('paged') : 1;

?>


    <section class="inner-news">


        <div class="container-fluid banner hide-xs">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="banner row">
                        <div class="slider-news">
                            <?php $args = array(
                                'cat' => '4'
                            ); ?>
                            <?php $posts = get_posts($args); ?>
                            <?php foreach ($posts as $post) : ?>
                                <?php setup_postdata($post); ?>
                                <div class="item">
                                    <h1 class="title"><?= get_field('workshop_theme') ?>
                                        <span class="date"><?= get_field('workshop_date'); ?></span>
                                    </h1>
                                </div>
                            <?php endforeach; ?>
                            <?php wp_reset_postdata(); ?>
                        </div>
                        <div class="arrow-bottom"><a href="#bottom-scroll"><i class="ic-arrow-down"></i></a></div>
                    </div>
                </div>
            </div>
        </div>


        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <?php wp_nav_menu(array(
                            'theme_location' => '',
                            'menu' => '8',
                            'container' => false,
                            'container_class' => '',
                            'container_id' => '',
                            'menu_class' => 'menu-cat',
                            'menu_id' => '',
                            'echo' => true,
                            'fallback_cb' => 'wp_page_menu',
                            'before' => '',
                            'after' => '',
                            'link_before' => '',
                            'link_after' => '',
                            'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                            'depth' => 0,
                            'walker' => '',
                        )); ?>
                        <div class="clearfix"></div>

                        <?php $args = array(
                            'cat' => '4',
                            'orderby' => 'ID desc',
                            'paged' => $paged,
                            'posts_per_page' => 5
                        ); ?>
                        <?php $posts_all_news = new WP_query($args); ?>
                        <?php while ($posts_all_news->have_posts()) : $posts_all_news->the_post(); ?>
                            <a href="<?php the_permalink(); ?>">
                                <div class="news-item  <?= get_field('workshops_mini') == 1 ? 'mini' : ''; ?>"
                                     style="background-image: url(<?= get_the_post_thumbnail_url(); ?>);">
                                    <div class="news-item-descreption">
                                        <img src="<?= get_template_directory_uri() ?>/images/cube.png" alt="#">
                                        <div>
                                            <h3 class="title"><?php the_title(); ?></h3>
                                            <p class="date"><?= get_field('workshop_date'); ?></p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <?php endwhile; ?>
                        <?php wp_reset_postdata(); ?>

                        <ul class="pagination">
                            <?php
                            $big = 999999999;
                            $args = array(
                                'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
                                'format' => '?paged=%#%',
                                'current' => $paged,
                                'total' => $posts_all_news->max_num_pages,
                                'prev_next' => false,
                                'prev_text' => __('«'),
                                'next_text' => __('»'),
                                'before_page_number' => '<li>',
                                'after_page_number' => '</li>'
                            ); ?>

                            <?= paginate_links($args); ?>

                        </ul>

                    </div>
                </div>
            </div>
        </div>


        <div class="container-fluid visible-xs">
            <div class="row">
                <div class=" col-xs-12 ">
                    <div class="row">
                        <h2 class="title cont">contact us<span> контакты</span></h2>
                        <div class="contact ">
                            <ul>
                                <li>
                                    <i class="ic-email-outline"></i>
                                    <div>
                                        <h5>e-mail</h5>
                                        <a href="mailto:<?= get_option('admin_email') ?>"><?= get_option('admin_email') ?></a>
                                    </div>
                                </li>
                                <li>
                                    <i class="ic-phone"></i>
                                    <div>
                                        <h5>телефоны</h5>
                                        <?php if (get_option('phone1')) : ?>
                                            <a href="tel:<?= get_option('phone1'); ?>"><?= get_option('phone1'); ?></a>
                                        <?php endif; ?>
                                        <?php if (get_option('phone2')) : ?>
                                            <a href="tel:<?= get_option('phone2'); ?>"><?= get_option('phone2'); ?></a>
                                        <?php endif; ?>
                                    </div>
                                </li>
                                <li>
                                    <i class="ic-map-marker"></i>
                                    <div>
                                        <h5>Адрес</h5>
                                        <a href="https://www.google.com.ua/maps/place/43B,+%D0%B2%D1%83%D0%BB%D0%B8%D1%86%D1%8F+%D0%A1%D1%83%D0%BC%D1%81%D1%8C%D0%BA%D0%B0,+43%D0%91,+%D0%A5%D0%B0%D1%80%D0%BA%D1%96%D0%B2,+%D0%A5%D0%B0%D1%80%D0%BA%D1%96%D0%B2%D1%81%D1%8C%D0%BA%D0%B0+%D0%BE%D0%B1%D0%BB%D0%B0%D1%81%D1%82%D1%8C/@50.006387,36.2347861,18z/data=!3m1!4b1!4m5!3m4!1s0x4127a0de435d1657:0x92f43e0454946df2!8m2!3d50.0063853!4d36.2358804?hl=ru"
                                           target="_blank">ул.<?= get_option('address'); ?></a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="contact_map" class="visible-xs"></div>
    </section>

<?php
get_footer();
