<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 18.06.17
 * Time: 1:32
 */

/**
 * Template name: rendering
 */

get_header();

?>
    <!--START CONTENT-->
    <section class="inner-news">
        <!-- Фома обратной связи(начало) -->
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h2 class="title" id="enroll-in-a-course">Check in<span>записаться на курс</span>
                    </h2>
                    <form action="#" class="enroll-in-a-course">
                        <p>Я,</p>
                        <div class="input">
                            <input type="text" placeholder="Имя и Фамилия" class="student-name">
                            <label for="#"></label>
                        </div>
                        <p>, хочу записаться</p>
                        <p>в <span>A</span>-Level <b>Ukraine</b> на курc</p>
                        <div class="select">
                            <select name="#" id="#" class="custom-select sources" placeholder="Хочу курс">
                                <option value="#"></option>
                                <?php $courses = array(); ?>
                                <?php $args = array(
                                    'offset' => 0,
                                    'post_type' => 'courses',
                                    'posts_per_page' => -1); ?>
                                <?php $post_courses = new WP_query($args); ?>
                                <?php while ($post_courses->have_posts()) : $post_courses->the_post(); ?>
                                    <?php array_push($courses, get_the_title()); ?>
                                    <option value="#"><?= get_the_title(); ?></option>
                                <?php endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                            </select>
                            <label for=""></label>
                        </div>
                        <p>Свяжитесь со мной по телефону</p>
                        <div class="input">
                            <input type="tel" class="phone-mask">
                            <label for="#"></label>
                        </div>
                        <p>или по e-mail</p>
                        <div class="input">
                            <input type="email" placeholder="e-mail" class="student-email">
                            <label for="#"></label>
                        </div>
                        <span class="text-danger errors-on-main-form" style="display: none;"></span>
                        <input type="submit" class="button wonna-study" value="отправить заявку">
                        <p class="min-text">*Вы принимаете политику конфиденциальности. Ваш адрес электронной почты и ФИО ни в коем случае не будут переданы другим пользователям и никогда не будут открыты для просмотра. Мы не будем передавать эти данные третьим лицам и/или любым другим способом сообщать эти данные третьим лицам.</p>
                    </form>

                    <form action="#" class="enroll-in-a-course-mobile">
                        <div class="input">
                            <input type="text" placeholder="Имя и Фамилия">
                            <label for="#"></label>
                        </div>
                        <div class="select">
                            <select name="#" id="#" class="custom-select sources" placeholder="Курс">
                                <option value="#"></option>
                                <?php foreach ($courses as $course) : ?>
                                    <option value="#"><?= $course; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <label for=""></label>
                        </div>
                        <div class="input">
                            <input type="email" placeholder="E-mail">
                            <label for="#"></label>
                        </div>
                        <div class="input">
                            <input type="tel" placeholder="Телефон" class="phone-mask">
                            <label for="#"></label>
                        </div>
                        <input type="submit" class="button mobile-form" value="отправить">
                        <p class="min-text">*Вы принимаете политику конфиденциальности. Ваш адрес электронной почты и ФИО ни в коем случае не будут переданы другим пользователям и никогда не будут открыты для просмотра. Мы не будем передавать эти данные третьим лицам и/или любым другим способом сообщать эти данные третьим лицам.</p>
                    </form>


                </div>
            </div>
        </div>
        <!-- Фома обратной связи(конец) -->
    </section>

<?php
get_footer();
