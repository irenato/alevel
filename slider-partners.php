<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 hide-xs">
            <div class="our-partners">
                <h3 class="title">Наши партнеры</h3>


                <div class="slider-partners">
                    <?php $args = array(
                        'post_type' => 'partners',
                        'orderby' => 'ID',
                        'order' => 'ASC',
                        'posts_per_page' => -1); ?>
                    <?php $i = 0; ?>
                    <?php $post_partners = new WP_query($args); ?>
                    <?php $count_of_partners = count($post_partners->posts); ?>
                    <?php while ($post_partners->have_posts()) : $post_partners->the_post(); ?>
                        <?php $i++; ?>
                        <?php $link_to_partners = get_field('link'); ?>
                        <?php if ($i % 2 != 0) : ?>
                            <div class="item">
                            <a <?= $link_to_partners ? "href=" . $link_to_partners . "" : ''; ?>
                               style="background-image: url(<?= get_the_post_thumbnail_url(); ?>);"></a>
                        <?php else : ?>
                            <a <?= $link_to_partners ? "href=" . $link_to_partners . "" : ''; ?>
                               style="background-image: url(<?= get_the_post_thumbnail_url(); ?>);"></a>
                            </div>
                        <?php endif; ?>
                    <?php endwhile; ?>

                    <?php if ($i % 2 != 0) : ?>
                </div>
                <?php endif; ?>
                <?php wp_reset_postdata(); ?>

            </div>
            <a href="#" class="become-our-partners">твоя компания тут</a>
        </div>
    </div>
</div>
</div>