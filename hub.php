<?php
/**
 * Template name: A-level Hub
 */

get_header();

if (have_posts()) :
    while (have_posts()) :
        the_post();

        ?>

        <section class="top-block-hub"
                 style="background-image: url('<?= get_template_directory_uri() ?>/images/hub/top-img.png')">
            <div class="wrapper">
                <h1>
                    <?= get_the_content(); ?>
                </h1>

                <a href="#invite-to-hub-scroll" class="invite-btn">записаться на экскурсию</a>

                <a href="#scroll_galery" class="mouse">
                    <span>галерея HUBa</span>
                    <i class="ic-mouse"></i>
                </a>
            </div>
        </section>
        <?php $advantages = get_field('advantages_block_item'); ?>
        <?php if ($advantages): ?>
        <section class="description" id="about_us">
            <div class="wrapper">
                <h1><?= get_field('advantages_block_title'); ?></h1>

                <p><?= get_field('advantages_block_description'); ?></p>

                <div class="advantages-block">
                    <?php foreach ($advantages as $item): ?>
                        <div class="advantages-item">
                            <img src="<?= $item['advantages_block_item_image'] ?>"
                                 alt="<?= $item['advantages_block_item_alt'] ?>">
                            <p><?= $item['advantages_block_item_description'] ?></p>
                        </div>
                    <?php endforeach ?>

                </div>
            </div>
        </section>
    <?php endif; ?>
        <?php $gallery = get_field('gallery'); ?>
        <?php if ($gallery): ?>
        <section class="gallery" id="scroll_galery">
            <?php foreach ($gallery as $item): ?>
                <img src="<?= $item['image'] ?>" alt="<?= $item['alt'] ?>">
            <?php endforeach; ?>
        </section>
    <?php endif ?>

        <section class="cost" id="cost_scroll">
            <div class="wrapper">

                <h1><?= get_field('price_block_title') ?></h1>
                <p><?= get_field('price_block_description') ?></p>

                <div class="row">
                    <div class="col">

                        <div class="day-of-week">
                            <span><?= get_field('price_left_block_title') ?></span>
                        </div>
                        <?php $price_items = get_field('price_left_block_item') ?>
                        <?php if ($price_items): ?>
                            <?php foreach ($price_items as $item): ?>
                                <div class="time-and-cost">
                                    <img src="<?= $item['image'] ?>" alt="<?= $item['alt'] ?>">
                                    <div>
                                        <p class="time"><?= $item['interval'] ?></p>
                                        <p class="cost"><?= $item['price'] ?><span><?= $item['period'] ?></span></p>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>

                        <button id="show_form1">отправить заявку</button>
                    </div>

                    <div class="col">
                        <?php $price_items = get_field('price_central_block_item') ?>
                        <?php if ($price_items): ?>
                            <?php foreach ($price_items as $item): ?>
                                <div class="about-class">
                                    <img src="<?= $item['image'] ?>" alt="<?= $item['alt'] ?>">
                                    <p><?= $item['title'] ?> </p>
                                    <p><?= $item['description'] ?></p>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>

                    <div class="col">
                        <div class="day-of-week">
                            <span><?= get_field('price_right_block_title') ?></span>
                        </div>
                        <?php $price_items = get_field('price_right_block_item') ?>
                        <?php if ($price_items): ?>
                            <?php foreach ($price_items as $item): ?>
                                <div class="time-and-cost">
                                    <img src="<?= $item['image'] ?>" alt="<?= $item['alt'] ?>">
                                    <div>
                                        <p class="time"><?= $item['interval'] ?></p>
                                        <p class="cost"><?= $item['price'] ?><span><?= $item['period'] ?></span></p>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <button id="show_form2">отправить заявку</button>
                    </div>
                </div>

            </div>
        </section>


        <section class="chose-us"
                 style="background-image: url('<?= get_field('choose_us_block_image') ?>')">
            <div class="wrapper">
                <h1><?= get_field('choose_us_block_title') ?></h1>
                <button id="show_form4">Отправить заявку</button>
                <a href="">Подробней</a>
            </div>
        </section>


        <section class="call-us" id="invite-to-hub-scroll">
            <div class="wrapper">
                <h1><?= get_field('invite_block_title') ?></h1>
                <p><?= get_field('invite_block_description') ?></p>
                <button id="show_form3">Отправить заявку</button>
                <p>или позвоните нам</p>
                <a href="tel:<?= str_replace(array('(', '-', ')'), '', get_field('invite_block_phone')) ?>"
                   type="phone">
                    <?= get_field('invite_block_phone') ?>
                </a>
            </div>
        </section>


        <section class="map" id="contacts"
                 style="background-image: url('<?= get_field('addres_block_image') ?>')">
            <div class="wrapper">
                <a href="<?= get_field('addres_block_gmaps_link') ?>" target="_blank">
                    <?= get_field('addres_block_title') ?>
                </a>
                <!--<button>https://vk.com</button>-->
            </div>
        </section>


        <footer>
            <ul>
                <li><a href="#about-us">о нас</a></li>
                <li><a href="#scroll_galery">галерея</a></li>
                <li><a href="#scroll_cost">стоимость</a></li>
                <li><a href="#contacts">контакты</a></li>
            </ul>
            <ul>              
                <?php if (get_field('link_fb')): ?>
                    <li><a class="social" href="<?= get_field('link_fb') ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <?php endif ?>
            </ul>
        </footer>

    <?php endwhile;
endif; ?>

<?php get_template_part('a-level-hub/hub-modal'); ?>


<?php get_footer(); ?>
