<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 02.04.17
 * Time: 16:07
 */

function countNewHubOrders()
{
    global $wpdb;
    $count_applications = $wpdb->get_var("SELECT COUNT(id) FROM wp_alevel_hub WHERE status=0;");
    if ($count_applications)
        return $count_applications;
    else
        return 0;
}

function allHubOrders(){
global $wpdb;
$users_data = $wpdb->get_results("SELECT * FROM `wp_alevel_hub` ORDER BY `created_at` DESC");
?>
<table class="wp-list-table widefat fixed striped pages">
    <tr>
        <th class="manage-column column-author" style="width: 2%">
            id
        </th>
        <th class="manage-column" style="width: 7%">
            имя
        </th>
        <th class="manage-column" style="width: 6%">
            телефон
        </th>
        <th class="manage-column" style="width: 9%">
            email
        </th>
        <th class="manage-column" style="width: 5%">
            дата
        </th>
        <th class="manage-column" style="width: 5%">
            время (начало)
        </th>
        <th class="manage-column" style="width: 5%">
            время (конец)
        </th>
        <th class="manage-column" style="width: 5%">
            статус заявки
        </th>
    </tr>
    <?php foreach ($users_data as $user): ?>
        <tr>
            <td class="manage-column column-author" style="width: 5%">
                <?= $user->id; ?>
            </td>
            <td class="manage-column">
                <?= $user->user_name; ?>
            </td>
            <td class="manage-column">
                <?= $user->user_phone; ?>
            </td>
            <td class="manage-column">
                <?= $user->user_email; ?>
            </td>
            <td class="manage-column">
                <?= $user->date; ?>
            </td>
            <td class="manage-column">
                <?= $user->time_from; ?>
            </td>
            <td class="manage-column">
                <?= $user->time_to; ?>
            </td>
            <td class="manage-column">
                <?= $user->status == 0 ? 'new' : 'confirmed'; ?>
            </td>
        </tr>
    <?php endforeach; ?>
</table>
<?
}

function newHubOrders(){
    global $wpdb;
    $users_data = $wpdb->get_results("SELECT * FROM `wp_alevel_hub` WHERE `status`=0 ORDER BY `created_at` DESC");
    ?>
    <table class="wp-list-table widefat fixed striped pages">
        <tr>
            <th class="manage-column column-author" style="width: 2%">
                id
            </th>
            <th class="manage-column" style="width: 7%">
                имя
            </th>
            <th class="manage-column" style="width: 6%">
                телефон
            </th>
            <th class="manage-column" style="width: 9%">
                email
            </th>
            <th class="manage-column" style="width: 5%">
                дата
            </th>
            <th class="manage-column" style="width: 5%">
                время (начало)
            </th>
            <th class="manage-column" style="width: 5%">
                время (конец)
            </th>
            <th class="manage-column" style="width: 5%">
                статус заявки
            </th>
            <th class="manage-column" style="width: 5%">
                действие
            </th>
        </tr>
        <?php foreach ($users_data as $user): ?>
            <tr>
                <td class="manage-column column-author" style="width: 5%">
                    <?= $user->id; ?>
                </td>
                <td class="manage-column">
                    <?= $user->user_name; ?>
                </td>
                <td class="manage-column">
                    <?= $user->user_phone; ?>
                </td>
                <td class="manage-column">
                    <?= $user->user_email; ?>
                </td>
                <td class="manage-column">
                    <?= $user->date; ?>
                </td>
                <td class="manage-column">
                    <?= $user->time_from; ?>
                </td>
                <td class="manage-column">
                    <?= $user->time_to; ?>
                </td>
                <td class="manage-column">
                    <?= $user->status == 0 ? 'new' : 'confirmed'; ?>
                </td>
                <td class="manage-column">
                    <a class="confirm-hub-order" data-id="<?= $user->id; ?>" href="#">подтвердить</a>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <?
}

function confirmedHubOrders(){
    global $wpdb;
    $users_data = $wpdb->get_results("SELECT * FROM `wp_alevel_hub` WHERE `status`=1 ORDER BY `created_at` DESC");
    ?>
    <table class="wp-list-table widefat fixed striped pages">
        <tr>
            <th class="manage-column column-author" style="width: 2%">
                id
            </th>
            <th class="manage-column" style="width: 7%">
                имя
            </th>
            <th class="manage-column" style="width: 6%">
                телефон
            </th>
            <th class="manage-column" style="width: 9%">
                email
            </th>
            <th class="manage-column" style="width: 5%">
                дата
            </th>
            <th class="manage-column" style="width: 5%">
                время (начало)
            </th>
            <th class="manage-column" style="width: 5%">
                время (конец)
            </th>
            <th class="manage-column" style="width: 5%">
                статус заявки
            </th>
        </tr>
        <?php foreach ($users_data as $user): ?>
            <tr>
                <td class="manage-column column-author" style="width: 5%">
                    <?= $user->id; ?>
                </td>
                <td class="manage-column">
                    <?= $user->user_name; ?>
                </td>
                <td class="manage-column">
                    <?= $user->user_phone; ?>
                </td>
                <td class="manage-column">
                    <?= $user->user_email; ?>
                </td>
                <td class="manage-column">
                    <?= $user->date; ?>
                </td>
                <td class="manage-column">
                    <?= $user->time_from; ?>
                </td>
                <td class="manage-column">
                    <?= $user->time_to; ?>
                </td>
                <td class="manage-column">
                    <?= $user->status == 0 ? 'new' : 'confirmed'; ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <?
}