<?php

add_action('wp_ajax_nopriv_addReview', 'addReview');
add_action('wp_ajax_addReview', 'addReview');

function addReview()
{
    global $wpdb;
    $name = stripcslashes(trim($_POST['user_name']));
    $email = stripcslashes(trim($_POST['user_mail']));
    $review_course = stripcslashes(trim($_POST['review_course']));
    $review = stripcslashes(trim($_POST['review']));
//    $table_reviews = 'wp_new_reviews';
//    $fields['user_name'] = $name;
//    $fields['user_email'] = strtolower($email);
//    $fields['user_link'] = $user_link;
//    $fields['review'] = $review;
//    $fields['date'] = time();
    $fields = array( // подготовим массив с полями поста, ключ это название поля, значение - его значение
        'post_type' => 'reviews', // нужно указать какой тип постов добавляем, у нас это my_custom_post_type
        'post_title' => $name, // заголовок поста
        'post_status' => 'pending',
    );
    $post_id = wp_insert_post($fields); // добавляем пост в базу и получаем его id
    if ($_FILES[0]) { // если основное фото было загружено
        $attach_id_img = media_handle_upload('0', $post_id); // добавляем картинку в медиабиблиотеку и получаем её id
        update_post_meta($post_id, '_thumbnail_id', $attach_id_img); // привязываем миниатюру к посту
    }
    if (update_post_meta($post_id, 'review', $review) && update_post_meta($post_id, 'review_email', $email) && update_post_meta($post_id, 'course_name', $review_course)) {
//    if ($wpdb->insert($table_reviews, $fields, '')) {
        print('done!');
        die();
    } else {
        print('error!');
        die();
    }
}

add_action('wp_ajax_nopriv_deleteReview', 'deleteReview');
add_action('wp_ajax_deleteReview', 'deleteReview');

function deleteReview()
{
    global $wpdb;
    $id = stripcslashes(trim($_POST['id']));
    $table = 'wp_new_reviews';
    $where['id'] = $id;
    return $wpdb->delete($table, $where);
    die();
}

add_action('wp_ajax_nopriv_beStudent', 'beStudent');
add_action('wp_ajax_beStudent', 'beStudent');

function beStudent()
{
    global $wpdb;
    $name = stripcslashes(trim($_POST['user_name']));
    $email = stripcslashes(trim($_POST['user_email']));
    $phone = stripcslashes(trim($_POST['user_phone']));
    $user_course = stripcslashes(trim($_POST['user_course']));
//    $headers = 'From: ' . $email . '.' . '<' . $email . '>';
//    $to = 'a.level.ua@gmail.com';
//    $mail_subject = 'Новая заявка на обучение ' . $name . "\r\n";
//    if (isset($_POST['format']) && $_POST['format'] == 'free') {
//        $mail_text = 'Бесплатное обучение' . "\r\n";
//        $mail_text .= 'Название курса :' . $user_course . "\r\n";
//    } else {
//        $mail_text = 'Название курса :' . $user_course . "\r\n";
//    }
//    $mail_text .= 'Контактные данные ' . $name . "\r\n";
//    $mail_text .= 'Электронная почта: ' . $email . "\r\n";
//    $mail_text .= 'Контактный телефон: ' . $phone . "\r\n";
    $table = 'wp_students_list';
    $fields['user_name'] = $name;
    $fields['user_email'] = strtolower($email);
    $fields['user_phone'] = $phone;
    $fields['user_course'] = $user_course;
//    if (isset($_POST['format']) && $_POST['format'] == 'free')
//        $fields['format'] = '0';
    $fields['date'] = time();
    if ($wpdb->insert($table, $fields, '')) {
//    if (wp_mail($to, $mail_subject, $mail_text, $headers) && $wpdb->insert($table, $fields, '')) {
        print('done!');
        die();
    } else {
        print('error!');
        die();
    }
}

add_action('wp_ajax_nopriv_beTeacher', 'beTeacher');
add_action('wp_ajax_beTeacher', 'beTeacher');

function beTeacher()
{
    global $wpdb;
    $name = stripcslashes(trim($_POST['user_name']));
    $email = stripcslashes(trim($_POST['user_email']));
    $phone = stripcslashes(trim($_POST['user_phone']));
    $user_course = stripcslashes(trim($_POST['user_course']));
    $headers = 'From: ' . $email . '.' . '<' . $email . '>';
    $to = 'a.level.ua@gmail.com';
    $mail_subject = 'Новая заявка (преподователь) ' . $name . "\r\n";
    $mail_text = 'Название курса :' . $user_course . "\r\n";
    $mail_text .= 'Контактные данные ' . $name . "\r\n";
    $mail_text .= 'Электронная почта: ' . $email . "\r\n";
    $mail_text .= 'Контактный телефон: ' . $phone . "\r\n";
    $table = 'wp_teachers_list';
    $fields['user_name'] = $name;
    $fields['user_email'] = strtolower($email);
    $fields['user_phone'] = $phone;
    $fields['user_course'] = $user_course;
    $fields['date'] = time();
    if (wp_mail($to, $mail_subject, $mail_text, $headers) && $wpdb->insert($table, $fields, '')) {
        print('done!');
        die();
    } else {
        print('error!');
        die();
    }
}


add_action('wp_ajax_nopriv_confirmApplicationFromStudents', 'confirmApplicationFromStudents');
add_action('wp_ajax_confirmApplicationFromStudents', 'confirmApplicationFromStudents');

function confirmApplicationFromStudents()
{
    global $wpdb;
    $id = stripcslashes(trim($_POST['id']));
    $table = 'wp_students_list';
    $field['status'] = 1;
    $where['id'] = $id;
    return $wpdb->update($table, $field, $where);
    die();
}

add_action('wp_ajax_nopriv_deleteApplicationFromStudents', 'deleteApplicationFromStudents');
add_action('wp_ajax_deleteApplicationFromStudents', 'deleteApplicationFromStudents');

function deleteApplicationFromStudents()
{
    global $wpdb;
    $id = stripcslashes(trim($_POST['id']));
    $table = 'wp_students_list';
    $where['id'] = $id;
    return $wpdb->delete($table, $where);
    die();
}

add_action('wp_ajax_nopriv_confirmApplicationFromTeachers', 'confirmApplicationFromTeachers');
add_action('wp_ajax_confirmApplicationFromTeachers', 'confirmApplicationFromTeachers');

function confirmApplicationFromTeachers()
{
    global $wpdb;
    $id = stripcslashes(trim($_POST['id']));
    $table = 'wp_teachers_list';
    $field['status'] = 1;
    $where['id'] = $id;
    return $wpdb->update($table, $field, $where);
    die();
}

add_action('wp_ajax_nopriv_deleteApplicationFromTeachers', 'deleteApplicationFromTeachers');
add_action('wp_ajax_deleteApplicationFromTeachers', 'deleteApplicationFromTeachers');

function deleteApplicationFromTeachers()
{
    global $wpdb;
    $id = stripcslashes(trim($_POST['id']));
    $table = 'wp_teachers_list';
    $where['id'] = $id;
    return $wpdb->delete($table, $where);
    die();
}

add_action('wp_ajax_nopriv_bePartner', 'bePartner');
add_action('wp_ajax_bePartner', 'bePartner');

function bePartner()
{
    global $wpdb;
    $name = stripcslashes(trim($_POST['partner_name']));
    $email = stripcslashes(trim($_POST['partner_mail']));
    $phone = stripcslashes(trim($_POST['partner_phone']));
    $company_name = stripcslashes(trim($_POST['company_name']));
    $headers = 'From: ' . $email . '.' . '<' . $email . '>';
    $to = 'a.level.ua@gmail.com';
    $mail_subject = 'Новая заявка (партнер) ' . $name . "\r\n";
    $mail_text = 'Название компании :' . $company_name . "\r\n";
    $mail_text .= 'Контактные данные ' . $name . "\r\n";
    $mail_text .= 'Электронная почта: ' . $email . "\r\n";
    $mail_text .= 'Контактный телефон: ' . $phone . "\r\n";
    $table = 'wp_partners_list';
    $fields['user_name'] = $name;
    $fields['user_email'] = strtolower($email);
    $fields['user_phone'] = $phone;
    $fields['company_name'] = $company_name;
    $fields['date'] = time();
    if (wp_mail($to, $mail_subject, $mail_text, $headers) && $wpdb->insert($table, $fields, '')) {
        print('done!');
        die();
    } else {
        print('error!');
        die();
    }
}

add_action('wp_ajax_nopriv_deletePartner', 'deletePartner');
add_action('wp_ajax_deletePartner', 'deletePartner');

function deletePartner()
{
    global $wpdb;
    $id = stripcslashes(trim($_POST['id']));
    $table = 'wp_partners_list';
    $where['id'] = $id;
    return $wpdb->delete($table, $where);
    die();
}

add_action('wp_ajax_nopriv_addStory', 'addStory');
add_action('wp_ajax_addStory', 'addStory');

function addStory()
{
    global $wpdb;
    $name = stripcslashes(trim($_POST['user_name']));
    $email = stripcslashes(trim($_POST['user_mail']));
    $phone = stripcslashes(trim($_POST['user_phone']));
    $story = stripcslashes(trim($_POST['story']));
    $fields = array( // подготовим массив с полями поста, ключ это название поля, значение - его значение
        'post_type' => 'free_educations', // нужно указать какой тип постов добавляем, у нас это my_custom_post_type
        'post_title' => $name, // заголовок поста
        'post_content' => $story, // заголовок поста
        'post_status' => 'pending',
    );
    $post_id = wp_insert_post($fields); // добавляем пост в базу и получаем его id
    if ($_FILES[0]) { // если основное фото было загружено
        $attach_id_img = media_handle_upload('0', $post_id); // добавляем картинку в медиабиблиотеку и получаем её id
        update_post_meta($post_id, '_thumbnail_id', $attach_id_img); // привязываем миниатюру к посту
    }
    if (update_post_meta($post_id, 'free_education_phone', $phone) && update_post_meta($post_id, 'free_education_email', $email)) {
//    if ($wpdb->insert($table_reviews, $fields, '')) {
        print('done!');
        die();
    } else {
        print('error!');
        die();
    }
}

add_action('wp_ajax_nopriv_addSponsor', 'addSponsor');
add_action('wp_ajax_addSponsor', 'addSponsor');

function addSponsor()
{
    $name = stripcslashes(trim($_POST['user_name']));
    $email = stripcslashes(trim($_POST['user_mail']));
    $data_id = stripcslashes(trim($_POST['id']));
    $fields = array( // подготовим массив с полями поста, ключ это название поля, значение - его значение
        'post_type' => 'sponsors', // нужно указать какой тип постов добавляем, у нас это my_custom_post_type
        'post_title' => $name, // заголовок поста
        'post_status' => 'pending',
    );

    $post_id = wp_insert_post($fields);
    if (update_post_meta($post_id, 'email', $email) && p2p_type('sponsors_to_free_educations')->connect($post_id, $data_id, array(
            'date' => current_time('mysql')
        ))
    ) {
        print('done!');
        die();
    } else {
        print('error!');
        die();
    }
}

/**
 * hub
 */
add_action('wp_ajax_nopriv_inviteHub', 'inviteHub');
add_action('wp_ajax_inviteHub', 'inviteHub');

function inviteHub()
{
    global $wpdb;
    $data = array();
    parse_str($_POST['data'], $data);
    $headers = 'From: ' . $data['email'] . '.' . '<' . $data['email'] . '>';
    $to = get_option('admin_email');
    $mail_subject = 'Новая заявка (hub) ' . $data['name'] . "\r\n";
    $mail_text = 'Дата: ' . $data['date'] . "\r\n";
    $mail_text .= 'Время с: ' . $data['time-from'] . " до " . $data['time-to'] . "\r\n";
    $mail_text .= 'Контактные данные ' . $data['name'] . "\r\n";
    $mail_text .= 'Электронная почта: ' . $data['email'] . "\r\n";
    $mail_text .= 'Контактный телефон: ' . $data['phone'] . "\r\n";
    wp_mail($to, $mail_subject, $mail_text, $headers);
    $fields = array(
        'user_name' => $data['name'],
        'user_email' => $data['email'],
        'user_phone' => str_replace(array('(', ' ', '-', ')'), '', $data['phone']),
        'date' => $data['date'],
        'time_from' => $data['time-from'],
        'time_to' => $data['time-to'],
    );
    return $wpdb->insert('wp_alevel_hub', $fields, '');
    die();
}
/*
 * hub
 */
function confirmHubOrder()
{
    global $wpdb;
    $table = 'wp_alevel_hub';
    $field['status'] = 1;
    $where['id'] = stripcslashes(trim($_POST['id']));
    return $wpdb->update($table, $field, $where);
    die();
}

add_action('wp_ajax_nopriv_confirmHubOrder', 'confirmHubOrder');
add_action('wp_ajax_confirmHubOrder', 'confirmHubOrder');

/*
 * master-class
 */
function confirmMCOrder()
{
    global $wpdb;
    $table = $wpdb->prefix . 'alevel_mc';
    $field['status'] = 1;
    $where['id'] = stripcslashes(trim($_POST['id']));
    return $wpdb->update($table, $field, $where);
    die();
}

add_action('wp_ajax_nopriv_confirmMCOrder', 'confirmMCOrder');
add_action('wp_ajax_confirmMCOrder', 'confirmMCOrder');

function registerMeMC()
{
    global $wpdb;
    $fields['username'] = addslashes($_POST['username']);
    $fields['email'] = strtolower(addslashes($_POST['email']));
    $fields['phone'] = strtolower(addslashes(str_replace(array('(', ' ', ')', '-'), '', $_POST['phone'])));
    $fields['random_string'] = generateRandomString(12);
    $fields['post_id'] = (int)$_POST['post_id'];
    $mail_text = get_field('mail_text', $fields['post_id']) . ": <a href='" . $_POST['link'] . "?path=" . $fields['random_string'] . "'>" . get_the_title($fields['post_id']) . "</a>";
    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html;' . "\r\n";
    $headers .= 'From: ' . get_the_title($fields['post_id']) . '.' . '<' . get_field('event_email', $fields['post_id']) . '>';
    $mail_subject = get_the_title($fields['post_id']) . ' (Регистрация)';
    wp_send_json($wpdb->insert($wpdb->prefix . 'alevel_mc', $fields, '') && wp_mail($fields['email'], $mail_subject, $mail_text, $headers));
}

add_action('wp_ajax_nopriv_registerMeMC', 'registerMeMC');
add_action('wp_ajax_registerMeMC', 'registerMeMC');

 