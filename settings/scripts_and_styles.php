<?php

function a_level_styles()
{
    if (is_page_template('landing.php') || is_page_template('landing_mc.php')) {
        wp_enqueue_style('a_level-owl.carousel', get_template_directory_uri() . '/css/owl.carousel.css', '', '', 'all');
        wp_enqueue_style('a_level-landing_fonts.min', get_template_directory_uri() . '/css/landing/css/fonts.min.css', '', '', 'all');
        wp_enqueue_style('a_level-landing_header.min', get_template_directory_uri() . '/css/landing/css/header.min.css', '', '', 'all');
        wp_enqueue_style('a_level-landing_main.min', get_template_directory_uri() . '/css/landing/css/main.min.css', '', '', 'all');
        wp_enqueue_style('a_level-landing_font-awesome.min', get_template_directory_uri() . '/css/css/landing/font-awesome.min.css', '', '', 'all');
        wp_enqueue_style('a_level-landing_lvl', get_template_directory_uri() . '/css/landing/css/lvl.css', '', '', 'all');
    } elseif (is_page_template('hub.php')) {
        wp_enqueue_style('a_level_hub-font-awesome.min', get_template_directory_uri() . '/css/hub/font-awesome.min.css', '', '', 'all');
        wp_enqueue_style('a_level_hub-header.min', get_template_directory_uri() . '/css/hub/header.min.css', '', '', 'all');
        wp_enqueue_style('a_level_hub-lvl', get_template_directory_uri() . '/css/hub/lvl.css', '', '', 'all');
        wp_enqueue_style('a_level_hub-fonts', get_template_directory_uri() . '/css/hub/fonts.min.css', '', '', 'all');
        wp_enqueue_style('a_level_hub-smart-grid.min', get_template_directory_uri() . '/css/hub/smart-grid.min.css', '', '', 'all');
        wp_enqueue_style('a_level_hub-slick', get_template_directory_uri() . '/libs/slick/slick.css', '', '', 'all');
        wp_enqueue_style('a_level_hub-slick-theme', get_template_directory_uri() . '/libs/slick/slick-theme.css', '', '', 'all');
        wp_enqueue_style('a_level_hub-jquery.datetimepicker', get_template_directory_uri() . '/libs/datetimepicker/jquery.datetimepicker.css', '', '', 'all');
        wp_enqueue_style('a_level_hub-main', get_template_directory_uri() . '/css/hub/main.min.css', '', '', 'all');
    } else {
        wp_enqueue_style('a_level-bootstrap', get_template_directory_uri() . '/css/bootstrap-grid.css', '', '', 'all');
        wp_enqueue_style('a_level-lvl', get_template_directory_uri() . '/css/lvl.css', '', '', 'all');
        wp_enqueue_style('a_level-fonts', get_template_directory_uri() . '/css/fonts.css', '', '', 'all');
        wp_enqueue_style('a_level-owl.carousel', get_template_directory_uri() . '/css/owl.carousel.css', '', '', 'all');
        wp_enqueue_style('a_level-style', get_template_directory_uri() . '/css/style.css', '', '', 'all');
        wp_enqueue_style('a_level-animate', get_template_directory_uri() . '/css/animate.min.css', '', '', 'all');
    }
}

add_action('wp_enqueue_scripts', 'a_level_styles');

function a_level_scripts()
{
    wp_deregister_script('jquery');
    wp_register_script('jquery', get_template_directory_uri() . '/js/jquery-2.1.4.min.js');
    wp_enqueue_script('jquery');
    wp_enqueue_script('a_level-jquery.nicescroll', get_template_directory_uri() . '/js/jquery.nicescroll.js', false, '', true);
    wp_enqueue_script('a_level-owl.carousel.min', get_template_directory_uri() . '/js/owl.carousel.min.js', false, '', true);        
    wp_enqueue_script('a_level-jquery.maskedinput', get_template_directory_uri() . '/js/jquery.maskedinput-1.3.js', false, '', false);
    
    if (is_page_template('landing.php') || is_page_template('landing_mc.php')) {
        wp_enqueue_script('a_level-recapcha', 'https://www.google.com/recaptcha/api.js?hl=ru', false, '', false);
        wp_enqueue_script('a_level-jquery.maskedinput', get_template_directory_uri() . '/js/jquery.maskedinput-1.3.js', false, '', false);
        wp_enqueue_script('a_level-landing_common', get_template_directory_uri() . '/js/landing/land_common.js', false, '', false);
        wp_localize_script('a_level-landing_common', 'alevel_ajax', array(
            'ajax_url' => admin_url('admin-ajax.php')
        ));
    } elseif (is_page_template('hub.php')) {
        wp_enqueue_script('a_level-hub_slick', get_template_directory_uri() . '/libs/slick/slick.min.js', false, '', true);
        wp_enqueue_script('a_level-hub_datetimepicker_full', get_template_directory_uri() . '/libs/datetimepicker/jquery.datetimepicker.full.min.js', false, '', true);
        wp_enqueue_script('a_level-hub_common', get_template_directory_uri() . '/js/hub/common.js', false, '', true);
        wp_enqueue_script('a_level-action', get_template_directory_uri() . '/js/hub/action.js', false, '', false);
        wp_localize_script('a_level-action', 'alevel_ajax', array(
            'ajax_url' => admin_url('admin-ajax.php')
        ));
    } else {
        wp_enqueue_script('a_level-bootstrap.min', get_template_directory_uri() . '/js/bootstrap.min.js', false, '', true);
        wp_enqueue_script('a_level-amazonaws', get_template_directory_uri() . '/js/TweenMax.js?', false, '', true);
        wp_enqueue_script('a_level-maps.googleapis.com', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDoFR4O2S0D5Uebjtxa9hCQ3bx2oiIhZz4&amp;libraries=places', false, '', true);
        wp_enqueue_script('a_level-animate-css', get_template_directory_uri() . '/js/animate-css.js', false, '', true);
        wp_enqueue_script('a_level-waypoints.min', get_template_directory_uri() . '/js/waypoints.min.js', false, '', true);
        wp_enqueue_script('a_level-common', get_template_directory_uri() . '/js/common.js', false, '', true);
        wp_enqueue_script('a_level-fb', get_template_directory_uri() . '/js/fb.js', false, '', false);
        wp_enqueue_script('a_level-capcha', 'https://www.google.com/recaptcha/api.js', false, '', false);
        wp_enqueue_script('a_level-jquery.maskedinput', get_template_directory_uri() . '/js/jquery.maskedinput-1.3.js', false, '', false);
        wp_enqueue_script('a_level-action', get_template_directory_uri() . '/js/action.js', false, '', false);
        wp_localize_script('a_level-action', 'alevel_ajax', array(
            'ajax_url' => admin_url('admin-ajax.php')
        ));
    }

}

add_action('wp_enqueue_scripts', 'a_level_scripts');

function scriptForAdmin()
{
    wp_enqueue_script('admin_action', get_template_directory_uri() . '/js/admin_action.js', array(), '', true);
}

add_action('admin_enqueue_scripts', 'scriptForAdmin');

function jqForEvents()
{
    wp_register_script('jquery3', get_template_directory_uri() . '/js/jquery-2.2.3.min.js', false, '', false);
    wp_enqueue_script('jquery3');
}

add_action('admin_enqueue_scripts', 'jqForEvents');