<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 04.02.17
 * Time: 23:22
 * 6LdiXRQUAAAAAAwkFuX35Tc8N-yp367IbYuwB05b
 */

/**
 * @param $url
 * @return mixed
 */
function getCurlData($url)
{

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_TIMEOUT, 10);
    curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
    $curlData = curl_exec($curl);
    curl_close($curl);
    return $curlData;
}

/**
 * @return string
 */
function checkRecaptcha($recaptcha)
{
    if (!empty($recaptcha)) {
        $google_url = "https://www.google.com/recaptcha/api/siteverify";
        $secret = '6LdiXRQUAAAAAAwkFuX35Tc8N-yp367IbYuwB05b';
        $ip = $_SERVER['REMOTE_ADDR'];
        $url = $google_url . "?secret=" . $secret . "&response=" . $recaptcha . "&remoteip=" . $ip;
        $res = getCurlData($url);
        $res = json_decode($res, true);
        if ($res['success']) {
            return true;
        } else {
            return false;
        }

    } else {
        return false;
    }
}

function registerMe()
{
//    if(!checkRecaptcha($_POST['captcha'])){
//        echo "recaptcha";
//        die();
//    }else{
    global $wpdb;
    $fields['username'] = addslashes($_POST['user_name']);
    $fields['email'] = strtolower(addslashes($_POST['email']));
    $fields['phone'] = strtolower(addslashes(str_replace(array('(', ' ', ')', '-'), '', $_POST['phone'])));
    $fields['event_id'] = addslashes($_POST['green']);
    $fields['event_id2'] = addslashes($_POST['orange']);
    $fields['event_id3'] = addslashes($_POST['blue']);
    $fields['random_string'] = generateRandomString(12);
    if ($wpdb->insert('wp_event_participants', $fields, '')) {
        $mail_text = get_field('mail_text', 528) . ": <a href='http://a-level.com.ua/open-doors?path=" . $fields['random_string'] . "'>" . get_the_title(528) . "</a>";
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html;' . "\r\n";
        $headers .= 'From: ' . get_the_title(528) . '.' . '<' . get_field('event_email', 528) . '>';
        $mail_subject = get_the_title(528) . ' (Регистрация)';
        wp_mail($fields['email'], $mail_subject, $mail_text, $headers);
        echo 'ok!';
        die();
    } else {
        echo 'error!';
        die();
    }
//    }

}

add_action('wp_ajax_nopriv_registerMe', 'registerMe');
add_action('wp_ajax_registerMe', 'registerMe');


function confirmParticipant()
{
    global $wpdb;
    $table = 'wp_event_participants';
    $field['status'] = 1;
    $where['id'] = stripcslashes(trim($_POST['id']));
    return $wpdb->update($table, $field, $where);
    die();
}

add_action('wp_ajax_nopriv_confirmParticipant', 'confirmParticipant');
add_action('wp_ajax_confirmParticipant', 'confirmParticipant');

function updateUserData($param)
{
    global $wpdb;
    $table = 'wp_event_participants';
    $field['email_confirmed'] = 1;
    $where['random_string'] = stripcslashes(trim($param));
    return $wpdb->update($table, $field, $where);
}

function updateUserDataMC($param)
{
    global $wpdb;
    $table = $wpdb->prefix . 'alevel_mc';
    $field['email_confirmed'] = 1;
    $where['random_string'] = stripcslashes(trim($param));
    return $wpdb->update($table, $field, $where);
}