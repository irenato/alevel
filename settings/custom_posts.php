<?php

//------------------------------------------
// преподователи
//------------------------------------------
add_action('init', 'sections_register');
function sections_register()
{
    register_post_type('teachers',
        array(
            'label' => __('Учителя'),
            'singular_label' => 'teacher',
            'public' => true,
            'show_ui' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'rewrite' => true,
            'menu_position' => 5,
            'menu_icon' => 'dashicons-admin-users',
            'supports' => array(
                'title',
                'thumbnail',
            ),
        )
    );
}

//------------------------------------------
// курсы
//------------------------------------------
add_action('init', 'sections_register1');
function sections_register1()
{
    register_post_type('courses',
        array(
            'label' => __('Курсы'),
            'singular_label' => 'course',
            'public' => true,
            'show_ui' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'rewrite' => true,
            'menu_position' => 5,
            'menu_icon' => 'dashicons-format-aside',
            'supports' => array(
                'title',
                'thumbnail',
                'editor',
            ),
        )
    );
}

//------------------------------------------
// партнеры
//------------------------------------------
add_action('init', 'register_partners');
function register_partners()
{
    register_post_type('partners',
        array(
            'label' => __('Партнеры'),
            'singular_label' => 'partner',
            'public' => true,
            'show_ui' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'rewrite' => true,
            'menu_position' => 5,
            'menu_icon' => 'dashicons-businessman',
            'supports' => array(
                'title',
                'thumbnail',
            ),
        )
    );
}

//------------------------------------------
// скидки
//------------------------------------------
add_action('init', 'register_discounts');
function register_discounts()
{
    register_post_type('discounts',
        array(
            'label' => __('Скидки'),
            'singular_label' => 'discount',
            'public' => true,
            'show_ui' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'rewrite' => true,
            'menu_position' => 5,
            'menu_icon' => 'dashicons-smiley',
            'supports' => array(
                'title',
            ),
        )
    );
}

//------------------------------------------
// отзывы
//------------------------------------------
add_action('init', 'register_reviews');
function register_reviews()
{
    register_post_type('reviews',
        array(
            'label' => __('Отзывы'),
            'singular_label' => 'review ',
            'public' => true,
            'show_ui' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'rewrite' => true,
            'menu_position' => 5,
            'menu_icon' => 'dashicons-cart',
            'supports' => array(
                'title',
                'thumbnail',
            ),
        )
    );
}

//------------------------------------------
// связывание курс - учитель
//------------------------------------------
if (function_exists("_p2p_load")) {
    function vp_post_to_post()
    {
        p2p_register_connection_type(array(
            'name' => 'courses_to_teachers',
            'from' => 'courses',
            'to' => 'teachers',
            'sortable' => 'any'
        ));
    }

    add_action('p2p_init', 'vp_post_to_post');
}

//------------------------------------------
// блок курсов
//------------------------------------------
add_action('init', 'register_addresses');
function register_addresses()
{
    register_post_type('addresses',
        array(
            'label' => __('Адреса курсов'),
            'singular_label' => 'address ',
            'public' => true,
            'show_ui' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'rewrite' => true,
            'menu_position' => 20,
            'menu_icon' => 'dashicons-location',
            'supports' => array(
                'title',
                'thumbnail',
            ),
        )
    );
}

//------------------------------------------
// связывание курс - адрес
//------------------------------------------
if (function_exists("_p2p_load")) {
    function vp_post_to_post2()
    {
        p2p_register_connection_type(array(
            'name' => 'courses_to_addresses',
            'from' => 'courses',
            'to' => 'addresses'
        ));
    }

    add_action('p2p_init', 'vp_post_to_post2');
}

//------------------------------------------
// блок курсов
//------------------------------------------
add_action('init', 'register_advertising');
function register_advertising()
{
        register_post_type('advertising',
        array(
            'label' => __('Рекламный блок'),
            'singular_label' => 'adver ',
            'public' => true,
            'show_ui' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'rewrite' => true,
            'menu_position' => 65,
            'menu_icon' => 'dashicons-paperclip',
            'supports' => array(
                'title',
                'thumbnail',
            ),
        )
    );
}

//------------------------------------------
// курсы
//------------------------------------------
add_action('init', 'register_free_education');
function register_free_education()
{
    register_post_type('free_educations',
        array(
            'label' => __('Бесплатное образование'),
            'singular_label' => 'free_education',
            'public' => true,
            'show_ui' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'rewrite' => true,
            'menu_position' => 5,
            'menu_icon' => 'dashicons-format-aside',
            'supports' => array(
                'title',
                'thumbnail',
                'editor',
            ),
        )
    );
}

add_action('init', 'register_sponsor');
function register_sponsor()
{
    register_post_type('sponsors',
        array(
            'label' => __('Спонсоры'),
            'singular_label' => 'sponsor',
            'public' => true,
            'show_ui' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'rewrite' => true,
            'menu_position' => 5,
            'menu_icon' => 'dashicons-format-aside',
            'supports' => array(
                'title',
            ),
        )
    );
}

//------------------------------------------
// связывание ученик(бесплатное образование) - спонсор
//------------------------------------------
if (function_exists("_p2p_load")) {
    function vp_post_to_post3()
    {
        p2p_register_connection_type(array(
            'name' => 'sponsors_to_free_educations',
            'from' => 'sponsors',
            'to' => 'free_educations'
        ));
    }

    add_action('p2p_init', 'vp_post_to_post3');
}