<?php

function showReviews()
{
    global $wpdb;
    $reviews = $wpdb->get_results("SELECT `id`, `user_name`, `user_email`, `user_link`, `review`, `date`, `status` FROM wp_new_reviews ORDER BY `id` DESC");
    ?>

    <table class="wp-list-table widefat fixed striped pages">
        <tr>
            <th class="manage-column column-author">
                id
            </th>
            <th class="manage-column">
                имя
            </th>
            <th class="manage-column">
                email
            </th>
            <th class="manage-column">
                линк
            </th>
            <th class="manage-column">
                комментарий
            </th>
            <th class="manage-column">
                дата
            </th>
            <th class="manage-column">
                действие
            </th>
        </tr>
        <?php if ($reviews): ?>
            <?php foreach ($reviews as $review) : ?>
                <tr>
                    <td class="manage-column column-author">
                        <?= $review->id; ?>
                    </td>
                    <td class="manage-column">
                        <?= $review->user_name; ?>
                    </td>
                    <td class="manage-column">
                        <?= $review->user_email; ?>
                    </td>
                    <td class="manage-column">
                        <a href="<?= $review->user_link; ?>"><?= $review->user_link; ?></a>
                    </td>
                    <td class="manage-column">
                        <?= $review->review; ?>
                    </td>
                    <td class="manage-column">
                        <?= date('d-m-Y H:i:s', $review->date); ?>
                    </td>
                    <td class="manage-column">
                        <a class="delete-review-now" data-id="<?= $review->id; ?>" href="#">удалить</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </table>

    <?php
}