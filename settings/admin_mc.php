<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 17.06.17
 * Time: 19:16
 */

function countNewMCOrders()
{
    global $wpdb;
    $count_applications = $wpdb->get_var("SELECT COUNT(id) FROM `" . $wpdb->prefix . "alevel_mc` WHERE status=0;");
    return $count_applications ? $count_applications : 0;
}

function allMCOrders(){
    global $wpdb;
    $users_data = $wpdb->get_results("SELECT * FROM `" . $wpdb->prefix . "alevel_mc` ORDER BY `created_at` DESC");
    ?>
    <form action="/wp-admin/admin.php?page=theme-panel_mc" method="post" style="float: right; margin-top: 25px">
        <input type="submit" class="button button-primary" name="get_MC_XLS" value="Скачать в XLS">
    </form>
    <table class="wp-list-table widefat fixed striped pages">
        <tr>
            <th class="manage-column column-author" style="width: 2%">
                id
            </th>
            <th class="manage-column" style="width: 7%">
                мастер-класс
            </th>
            <th class="manage-column" style="width: 7%">
                имя
            </th>
            <th class="manage-column" style="width: 6%">
                телефон
            </th>
            <th class="manage-column" style="width: 9%">
                email
            </th>
            <th class="manage-column" style="width: 5%">
                дата
            </th>
            <th class="manage-column" style="width: 5%">
                email подтвежден
            </th>
            <th class="manage-column" style="width: 5%">
                статус заявки
            </th>
        </tr>
        <?php foreach ($users_data as $user): ?>
            <tr>
                <td class="manage-column column-author" style="width: 5%">
                    <?= $user->id; ?>
                </td>
                <td class="manage-column">
                    <?= get_the_title($user->post_id); ?>
                </td>
                <td class="manage-column">
                    <?= $user->username; ?>
                </td>
                <td class="manage-column">
                    <?= $user->phone; ?>
                </td>
                <td class="manage-column">
                    <?= $user->email; ?>
                </td>
                <td class="manage-column">
                    <?= date('d-m-Y H:i:s', strtotime($user->created_at)); ?>
                </td>
                <td class="manage-column">
                    <?= $user->email_confirmed == 0 ? 'email не подтвежден' : 'email подтвежден'; ?>
                </td>
                <td class="manage-column">
                    <?= $user->status == 0 ? 'new' : 'confirmed'; ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <?
}

function newMCOrders(){
    global $wpdb;
    $users_data = $wpdb->get_results("SELECT * FROM `" . $wpdb->prefix . "alevel_mc` WHERE `status`=0 ORDER BY `created_at` DESC");
    ?>
    <table class="wp-list-table widefat fixed striped pages">
        <tr>
            <th class="manage-column column-author" style="width: 2%">
                id
            </th>
            <th class="manage-column" style="width: 7%">
                мастер-класс
            </th>
            <th class="manage-column" style="width: 7%">
                имя
            </th>
            <th class="manage-column" style="width: 6%">
                телефон
            </th>
            <th class="manage-column" style="width: 9%">
                email
            </th>
            <th class="manage-column" style="width: 5%">
                дата
            </th>
            <th class="manage-column" style="width: 5%">
                email подтвежден
            </th>
            <th class="manage-column" style="width: 5%">
                статус заявки
            </th>
            <th class="manage-column" style="width: 5%">
                действие
            </th>
        </tr>
        <?php foreach ($users_data as $user): ?>
            <tr>
                <td class="manage-column column-author" style="width: 5%">
                    <?= $user->id; ?>
                </td>
                <td class="manage-column">
                    <?= get_the_title($user->post_id); ?>
                </td>
                <td class="manage-column">
                    <?= $user->username; ?>
                </td>
                <td class="manage-column">
                    <?= $user->phone; ?>
                </td>
                <td class="manage-column">
                    <?= $user->email; ?>
                </td>
                <td class="manage-column">
                    <?= date('d-m-Y H:i:s', strtotime($user->created_at)); ?>
                </td>
                <td class="manage-column">
                    <?= $user->email_confirmed == 0 ? 'email не подтвежден' : 'email подтвежден'; ?>
                </td>
                <td class="manage-column">
                    <?= $user->status == 0 ? 'new' : 'confirmed'; ?>
                </td>
                <td class="manage-column">
                    <a class="confirm-mc-order" data-id="<?= $user->id; ?>" href="#">подтвердить</a>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <?
}

function confirmedMCOrders(){
    global $wpdb;
    $users_data = $wpdb->get_results("SELECT * FROM `" . $wpdb->prefix . "alevel_mc` WHERE `status`=1 ORDER BY `created_at` DESC");
    ?>
    <table class="wp-list-table widefat fixed striped pages">
        <tr>
            <th class="manage-column column-author" style="width: 2%">
                id
            </th>
            <th class="manage-column" style="width: 7%">
                мастер-класс
            </th>
            <th class="manage-column" style="width: 7%">
                имя
            </th>
            <th class="manage-column" style="width: 6%">
                телефон
            </th>
            <th class="manage-column" style="width: 9%">
                email
            </th>
            <th class="manage-column" style="width: 5%">
                дата
            </th>
            <th class="manage-column" style="width: 5%">
                email подтвежден
            </th>
            <th class="manage-column" style="width: 5%">
                статус заявки
            </th>
        </tr>
        <?php foreach ($users_data as $user): ?>
            <tr>
                <td class="manage-column column-author" style="width: 5%">
                    <?= $user->id; ?>
                </td>
                <td class="manage-column">
                    <?= get_the_title($user->post_id); ?>
                </td>
                <td class="manage-column">
                    <?= $user->username; ?>
                </td>
                <td class="manage-column">
                    <?= $user->phone; ?>
                </td>
                <td class="manage-column">
                    <?= $user->email; ?>
                </td>
                <td class="manage-column">
                    <?= date('d-m-Y H:i:s', strtotime($user->created_at)); ?>
                </td>
                <td class="manage-column">
                    <?= $user->email_confirmed == 0 ? 'email не подтвежден' : 'email подтвежден'; ?>
                </td>
                <td class="manage-column">
                    <?= $user->status == 0 ? 'new' : 'confirmed'; ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <?
}

function createMC_XLS()
{
    global $wpdb;
    if (isset($_POST['get_MC_XLS'])) {
        require_once dirname(__FILE__) . '/../lib/PHPExcel.php';
        $user_data = $wpdb->get_results("SELECT * FROM `" . $wpdb->prefix . "alevel_mc` ORDER BY `id` DESC");
        $event_names = array();
        $xls = new PHPExcel();
        $xls->getProperties()
            ->setCreator("user")
            ->setLastModifiedBy("user")
            ->setTitle("Office 2007 XLSX Test Document")
            ->setSubject("Office 2007 XLSX Test Document")
            ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
            ->setKeywords("office 2007 openxml php")
            ->setCategory("Test result file");
        $xls->setActiveSheetIndex(0);
        $rowCount = 1;
        $cell_definition = array(
            'A' => 'id',
            'B' => 'имя пользователя',
            'C' => 'телефон',
            'D' => 'email',
            'E' => 'мастер-класс',
            'F' => 'дата регистрации',
            'G' => 'статус',
        );
        foreach ($cell_definition as $column => $value)
            $xls->getActiveSheet()->setCellValue("{$column}1", $value);
        foreach ($user_data as $user){
            ++$rowCount;
            $xls->getActiveSheet()->setCellValue('A' . $rowCount, $user->id);
            $xls->getActiveSheet()->setCellValue('B' . $rowCount, $user->username);
            $xls->getActiveSheet()->setCellValue('C' . $rowCount, $user->phone);
            $xls->getActiveSheet()->setCellValue('D' . $rowCount, $user->email);
            $xls->getActiveSheet()->setCellValue('E' . $rowCount, get_the_title($user->post_id));
            $xls->getActiveSheet()->setCellValue('F' . $rowCount, date('d-m-Y H:i:s', strtotime($user->created_at)));
            $xls->getActiveSheet()->setCellValue('G' . $rowCount, $user->status == 0 ? 'new' : 'confirmed');
        }
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="master-class-' . date('d-m-Y') . '.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($xls, 'Excel2007');
        $objWriter->save('php://output');
        exit;
    }
}

add_action('admin_init', 'createMC_XLS');