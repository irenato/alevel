<?php

function selectAllApplicationsFromTeachers()
{
    global $wpdb;
    $applications = $wpdb->get_results("SELECT `id`, `user_name`, `user_phone`, `user_email`, `user_phone`, `user_course`, `date`, `status` FROM wp_teachers_list ORDER BY `id` DESC ");
    ?>

    <table class="wp-list-table widefat fixed striped pages">
        <tr>
            <th class="manage-column column-author">
                id
            </th>
            <th class="manage-column">
                имя
            </th>
            <th class="manage-column">
                email
            </th>
            <th class="manage-column">
                телефон
            </th>
            <th class="manage-column">
                название курса
            </th>
            <th class="manage-column">
                дата
            </th>
            <th class="manage-column">
                статус заявки
            </th>
        </tr>
        <?php if ($applications): ?>
            <?php foreach ($applications as $application) : ?>
                <tr>
                    <td class="manage-column column-author">
                        <?= $application->id; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->user_name; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->user_phone; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->user_email; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->user_course; ?>
                    </td>
                    <td class="manage-column">
                        <?= date('d-m-Y H:i:s', $application->date); ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->status == 0 ? 'new' : 'confirmed'; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </table>

    <?php
}

function selectNewApplicationsFromTeachers()
{
    global $wpdb;
    $applications = $wpdb->get_results("SELECT `id`, `user_name`, `user_phone`, `user_email`, `user_phone`, `user_course`, `date`, `status` FROM wp_teachers_list WHERE `status`=0 ORDER BY `id` DESC");
    ?>

    <table class="wp-list-table widefat fixed striped pages">
        <tr>
            <th class="manage-column column-author">
                id
            </th>
            <th class="manage-column">
                имя
            </th>
            <th class="manage-column">
                email
            </th>
            <th class="manage-column">
                телефон
            </th>
            <th class="manage-column">
                название курса
            </th>
            <th class="manage-column">
                дата
            </th>
            <th class="manage-column">
               действие
            </th>
        </tr>
        <?php if ($applications): ?>
            <?php foreach ($applications as $application) : ?>
                <tr>
                    <td class="manage-column column-author">
                        <?= $application->id; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->user_name; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->user_phone; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->user_email; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->user_course; ?>
                    </td>
                    <td class="manage-column">
                        <?= date('d-m-Y H:i:s', $application->date); ?>
                    </td>
                    <td class="manage-column">
                        <a class="confirm-teacher-now" data-id="<?= $application->id; ?>" href="#">подтвердить</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </table>

    <?php
}

function selectConfirmedApplicationsFromTeachers()
{
    global $wpdb;
    $applications = $wpdb->get_results("SELECT `id`, `user_name`, `user_phone`, `user_email`, `user_phone`, `user_course`, `date`, `status` FROM wp_teachers_list  WHERE `status`=1 ORDER BY `id` DESC");
    ?>

    <table class="wp-list-table widefat fixed striped pages">
        <tr>
            <th class="manage-column column-author">
                id
            </th>
            <th class="manage-column">
                имя
            </th>
            <th class="manage-column">
                email
            </th>
            <th class="manage-column">
                телефон
            </th>
            <th class="manage-column">
                название курса
            </th>
            <th class="manage-column">
                дата
            </th>
            <th class="manage-column">
                действие
            </th>
        </tr>
        <?php if ($applications): ?>
            <?php foreach ($applications as $application) : ?>
                <tr>
                    <td class="manage-column column-author">
                        <?= $application->id; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->user_name; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->user_phone; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->user_email; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->user_course; ?>
                    </td>
                    <td class="manage-column">
                        <?= date('d-m-Y H:i:s', $application->date); ?>
                    </td>
                    <td class="manage-column">
                        <a class="delete-teacher-now" data-id="<?= $application->id; ?>" href="#">удалить</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </table>

    <?php
}