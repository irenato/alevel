<?php
function showPartners()
{
    global $wpdb;
    $companies = $wpdb->get_results("SELECT `id`, `user_name`, `user_email`, `user_phone`, `company_name`, `date`, `status` FROM wp_partners_list ORDER BY `id` DESC");
    ?>

    <table class="wp-list-table widefat fixed striped pages">
        <tr>
            <th class="manage-column column-author">
                id
            </th>
            <th class="manage-column">
                компания
            </th>
            <th class="manage-column">
                контактное лицо
            </th>
            <th class="manage-column">
                email
            </th>
            <th class="manage-column">
                телефон
            </th>
            <th class="manage-column">
                дата
            </th>
            <th class="manage-column">
                действие
            </th>
        </tr>
        <?php if ($companies): ?>
            <?php foreach ($companies as $company) : ?>
                <tr>
                    <td class="manage-column column-author">
                        <?= $company->id; ?>
                    </td>
                    <td class="manage-column">
                        <?= $company->company_name; ?>
                    </td>
                    <td class="manage-column">
                        <?= $company->user_name; ?>
                    </td>
                    <td class="manage-column">
                        <?= $company->user_email; ?>
                    </td>
                    <td class="manage-column">
                        <?= $company->user_phone; ?>
                    </td>
                    <td class="manage-column">
                        <?= date('d-m-Y H:i:s', $company->date); ?>
                    </td>
                    <td class="manage-column">
                        <a class="delete-partner-now" data-id="<?= $company->id; ?>" href="#">удалить</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </table>

    <?php
}