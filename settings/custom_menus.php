<?php

function theme_register_nav_menu1() {
    register_nav_menu( 'primary', 'Верхнее на главной' );
}
add_action( 'after_setup_theme', 'theme_register_nav_menu1' );

function theme_register_nav_menu2() {
    register_nav_menu( 'other', 'Верхнее на остальных' );
}
add_action( 'after_setup_theme', 'theme_register_nav_menu2' );

function theme_register_nav_menu3() {
    register_nav_menu( 'category', 'Категории постов' );
}
add_action( 'after_setup_theme', 'theme_register_nav_menu3' );

function theme_register_nav_menu4() {
    register_nav_menu( 'buttom', 'Нижнее меню' );
}
add_action( 'after_setup_theme', 'theme_register_nav_menu4' );

function theme_register_nav_menu5() {
    register_nav_menu( 'buttom_courses', 'Нижнее меню направления обучения' );
}
add_action( 'after_setup_theme', 'theme_register_nav_menu5' );
