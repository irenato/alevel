<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 05.02.17
 * Time: 10:54
 */

function manageEvents()
{
    global $wpdb;
    if (isset($_POST['event'])) {
        $table = 'wp_alevel_events_schedule';
        $fields['name'] = addslashes($_POST['event']['name']);
        $where['id'] = addslashes($_POST['event']['id']);
        $wpdb->update($table, $fields, $where);
    }

    $events = $wpdb->get_results("SELECT `id`, `name`, `colour` FROM `wp_alevel_events_schedule` ORDER BY `id`");
    ?>

    <table class="wp-list-table widefat fixed striped pages">
        <tr>
            <th class="manage-column column-author">
                id
            </th>
            <th class="manage-column">
                поток
            </th>
            <th class="manage-column">
                название
            </th>
            <th class="manage-column">
                действие
            </th>
        </tr>
        <?php if ($events): ?>
            <?php foreach ($events as $event) : ?>
                <form method="post" action="/wp-admin/admin.php?page=my-custom-submenu-page991">
                    <tr>
                        <td class="manage-column column-author">
                            <input type="hidden" name="event[id]" value="<?= $event->id; ?>">
                            <?= $event->id; ?>
                        </td>
                        <td class="manage-column">
                            <?= $event->colour; ?>
                        </td>
                        <td class="manage-column">
                            <input type="text" name="event[name]" value="<?= $event->name; ?>">
                        </td>
                        <th class="manage-column">
                            <input type="submit" class="button button-primary" value="обновить">
                        </th>
                    </tr>
                </form>
            <?php endforeach; ?>
        <?php endif; ?>
    </table>

    <?php
}

function allParticipants()
{
    global $wpdb;
    $users_data = $wpdb->get_results("SELECT * FROM `wp_event_participants` ORDER BY `id` DESC");
    $all_events = $wpdb->get_results("SELECT `id`, `name` FROM `wp_alevel_events_schedule` ORDER BY `id`", 'ARRAY_A');
    $event_names = array();
    foreach ($all_events as $event) {
        $event_names[$event['id']] = $event['name'];
    }
    ?>
    <form action="/wp-admin/admin.php?page=theme-panel99" method="post" style="float: right; margin-top: 25px">
        <input type="submit" class="button button-primary" name="getXLS" value="Скачать в XLS">
    </form>
    <table class="wp-list-table widefat fixed striped pages">
        <tr>
            <th class="manage-column column-author" style="width: 2%">
                id
            </th>
            <th class="manage-column" style="width: 7%">
                имя
            </th>
            <th class="manage-column" style="width: 6%">
                телефон
            </th>
            <th class="manage-column" style="width: 9%">
                email
            </th>
            <th class="manage-column" style="width: 5%">
                зеленый
            </th>
            <th class="manage-column" style="width: 5%">
                оранжевый
            </th>
            <th class="manage-column" style="width: 5%">
                синий
            </th>
            <th class="manage-column" style="width: 5%">
                дата
            </th>
            <th class="manage-column" style="width: 5%">
                email подтвержден
            </th>
            <th class="manage-column" style="width: 5%">
                статус заявки
            </th>
        </tr>
        <?php foreach ($users_data as $user): ?>
            <tr>
                <td class="manage-column column-author" style="width: 5%">
                    <?= $user->id; ?>
                </td>
                <td class="manage-column">
                    <?= $user->username; ?>
                </td>
                <td class="manage-column">
                    <?= $user->phone; ?>
                </td>
                <td class="manage-column">
                    <?= $user->email; ?>
                </td>
                <td class="manage-column">
                    <?= $user->event_id ? $event_names[$user->event_id] : ' - '; ?>
                </td>
                <td class="manage-column">
                    <?= $user->event_id2 ? $event_names[$user->event_id2] : ' - '; ?>
                </td>
                <td class="manage-column">
                    <?= $user->event_id3 ? $event_names[$user->event_id3] : ' - '; ?>
                </td>
                <td class="manage-column">
                    <?= date('d-m-Y H:i:s', strtotime($user->created_at)); ?>
                </td>
                <td class="manage-column">
                    <?= $user->email_confirmed == 0 ? 'no' : 'yes'; ?>
                </td>
                <td class="manage-column">
                    <?= $user->status == 0 ? 'new' : 'confirmed'; ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <?php

}

function newParticipants()
{
    global $wpdb;
    $users_data = $wpdb->get_results("SELECT * FROM `wp_event_participants` WHERE `status`=0 ORDER BY `id` DESC");
    $all_events = $wpdb->get_results("SELECT `id`, `name` FROM `wp_alevel_events_schedule` ORDER BY `id`", 'ARRAY_A');
    $event_names = array();
    foreach ($all_events as $event) {
        $event_names[$event['id']] = $event['name'];
    }

    ?>

    <table class="wp-list-table widefat fixed striped pages">
        <tr>
            <th class="manage-column column-author" style="width: 2%">
                id
            </th>
            <th class="manage-column" style="width: 7%">
                имя
            </th>
            <th class="manage-column" style="width: 7%">
                телефон
            </th>
            <th class="manage-column" style="width: 7%">
                email
            </th>
            <th class="manage-column" style="width: 5%">
                зеленый
            </th>
            <th class="manage-column" style="width: 5%">
                оранжевый
            </th>
            <th class="manage-column" style="width: 5%">
                синий
            </th>
            <th class="manage-column" style="width: 5%">
                дата
            </th>
            <th class="manage-column" style="width: 5%">
                email подтвержден
            </th>
            <th class="manage-column" style="width: 3%">
                статус
            </th>
            <th class="manage-column" style="width: 3%">
                действие
            </th>
        </tr>
        <?php foreach ($users_data as $user): ?>
            <tr>
                <td class="manage-column column-author">
                    <?= $user->id; ?>
                </td>
                <td class="manage-column">
                    <?= $user->username; ?>
                </td>
                <td class="manage-column">
                    <?= $user->phone; ?>
                </td>
                <td class="manage-column">
                    <?= $user->email; ?>
                </td>
                <td class="manage-column">
                    <?= $user->event_id ? $event_names[$user->event_id] : ' - '; ?>
                </td>
                <td class="manage-column">
                    <?= $user->event_id2 ? $event_names[$user->event_id2] : ' - '; ?>
                </td>
                <td class="manage-column">
                    <?= $user->event_id3 ? $event_names[$user->event_id3] : ' - '; ?>
                </td>
                <td class="manage-column">
                    <?= date('d-m-Y H:i:s', strtotime($user->created_at)); ?>
                </td>
                <td class="manage-column">
                    <?= $user->email_confirmed == 0 ? 'no' : 'yes'; ?>
                </td>
                <td class="manage-column">
                    <?= $user->status == 0 ? 'new' : 'confirmed'; ?>
                </td>
                <td class="manage-column">
                    <a class="confirm-participants-now" data-id="<?= $user->id; ?>" href="#">подтвердить</a>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <?php

}

function selectEvents()
{
    global $wpdb;
    $events = $wpdb->get_results("SELECT `id`, `name`, `colour` FROM `wp_alevel_events_schedule` ORDER BY `id`",
        'ARRAY_A');

    return $events;
}

function countNewParticipants()
{
    global $wpdb;
    $count_applications = $wpdb->get_var("SELECT COUNT(id) FROM wp_event_participants WHERE status=0;");
    if ($count_applications)
        return $count_applications;
    else
        return 0;
}

function createNewXLS($events)
{
    global $wpdb;
    if (isset($_POST['getXLS'])) {
        require_once dirname(__FILE__) . '/../lib/PHPExcel.php';
        $user_data = $wpdb->get_results("SELECT * FROM `wp_event_participants` ORDER BY `id` DESC");
        $all_events = $wpdb->get_results("SELECT `id`, `name` FROM `wp_alevel_events_schedule` ORDER BY `id`", 'ARRAY_A');
        $event_names = array();
        foreach ($all_events as $event) {
            $event_names[$event['id']] = $event['name'];
        }
        $xls = new PHPExcel();
        $xls->getProperties()
            ->setCreator("user")
            ->setLastModifiedBy("user")
            ->setTitle("Office 2007 XLSX Test Document")
            ->setSubject("Office 2007 XLSX Test Document")
            ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
            ->setKeywords("office 2007 openxml php")
            ->setCategory("Test result file");
        $xls->setActiveSheetIndex(0);
        $rowCount = 1;
        $cell_definition = array(
            'A' => 'id',
            'B' => 'Имя пользователя',
            'C' => 'Телефон',
            'D' => 'Email',
            'E' => 'зеленый',
            'F' => 'оранжевый',
            'G' => 'синий',
            'H' => 'дата регистрации',
            'I' => 'статус',
        );
        foreach ($cell_definition as $column => $value)
            $xls->getActiveSheet()->setCellValue("{$column}1", $value);
        foreach ($user_data as $user){
            ++$rowCount;
            $xls->getActiveSheet()->setCellValue('A' . $rowCount, $user->id);
            $xls->getActiveSheet()->setCellValue('B' . $rowCount, $user->username);
            $xls->getActiveSheet()->setCellValue('C' . $rowCount, $user->phone);
            $xls->getActiveSheet()->setCellValue('D' . $rowCount, $user->email);
            $xls->getActiveSheet()->setCellValue('E' . $rowCount, $user->event_id ? $event_names[$user->event_id] : ' - ');
            $xls->getActiveSheet()->setCellValue('F' . $rowCount, $user->event_id2 ? $event_names[$user->event_id2] : ' - ');
            $xls->getActiveSheet()->setCellValue('G' . $rowCount, $user->event_id3 ? $event_names[$user->event_id3] : ' - ');
            $xls->getActiveSheet()->setCellValue('H' . $rowCount, date('d-m-Y H:i:s', strtotime($user->created_at)));
            $xls->getActiveSheet()->setCellValue('I' . $rowCount, $user->status == 0 ? 'new' : 'confirmed');
        }
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="open-doors-' . date('d-m-Y') . '.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($xls, 'Excel2007');
        $objWriter->save('php://output');
        exit;
    }
}

add_action('admin_init', 'createNewXLS');

