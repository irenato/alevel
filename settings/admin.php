<?php


function theme_settings_page()
{

    echo '<div class="wrap">';
    echo '<h1>Настройки темы</h1>';
    echo '<form method="post" action="options.php" enctype="multipart/form-data">';
    settings_fields("section");
    do_settings_sections("theme-options");
    submit_button();
    echo '</form>';
    echo '</div>';
}

function display_phone1_element()
{
    echo '<input type="text" name="phone1" id="phone1" value="' . get_option('phone1') . '"/>';
}

function display_phone2_element()
{
    echo '<input type="text" name="phone2" id="phone2" value="' . get_option('phone2') . '"/>';
}

function display_admin_email_element()
{
    echo '<input type="text" name="admin_email" id="admin_email" value="' . get_option('admin_email') . '"/>';
}

function display_city_element()
{
    echo '<input type="text" name="city" id="city" value="' . get_option('city') . '"/>';
}


function display_address_element()
{
    echo '<input type="text" name="address" id="address" value="' . get_option('address') . '"/>';
}

function display_coordinates_element()
{
    echo '<input type="text" name="coordinates" id="coordinates" value="' . get_option('coordinates') . '"/>';
}

function display_googlemaps_element()
{
    echo '<input type="text" name="googlemaps" id="googlemaps" value="' . get_option('googlemaps') . '"/>';
}

function display_vkontakte_element()
{
    echo '<input type="text" name="vkontakte_url" id="vkontakte_url" value="' . get_option('vkontakte_url') . '"/>';
}

function display_facebook_element()
{
    echo '<input type="text" name="facebook_url" id="facebook_url" value="' . get_option('facebook_url') . '"/>';
}

function display_pinterest_element()
{
    echo '<input type="text" name="pinterest_url" id="pinterest_url" value="' . get_option('pinterest_url') . '"/>';
}

function display_behance_element()
{
    echo '<input type="text" name="behance_url" id="behance_url" value="' . get_option('behance_url') . '"/>';
}

function display_admin_courses_fullname()
{
    echo '<input type="text" name="admin_courses_fullname" id="admin_courses_fullname" value="' . get_option('admin_courses_fullname') . '"/>';
}

function logo_display()
{
    echo '<input type="file" name="logo"/>';
    echo get_option('logo');
}

function handle_logo_upload()
{

    if (!empty($_FILES["logo"]["tmp_name"])) {
        $urls = wp_handle_upload($_FILES["logo"], array('test_form' => FALSE));
        $temp = $urls["url"];
        return $temp;
    }

    return get_option('logo');
}

function admin_courses_avatar_display()
{
    echo '<input type="file" name="admin_courses_avatar"/>';
    echo get_option('admin_courses_avatar');
}

function handle_admin_courses_avatar_upload()
{
    if (!empty($_FILES["admin_courses_avatar"]["tmp_name"])) {
        $urls = wp_handle_upload($_FILES["admin_courses_avatar"], array('test_form' => FALSE));
        $temp = $urls["url"];
        return $temp;
    }
    return get_option('admin_courses_avatar');

}

//function auditory_display()
//{
//    echo '<input type="file" name="auditory"/>';
//    echo get_option('auditory');
//}
//
//function auditory_upload()
//{
//    global $option;
//
//    if (!empty($_FILES["auditory"]["tmp_name"])) {
//        $urls = wp_handle_upload($_FILES["auditory"], array('test_form' => FALSE));
//        $temp = $urls["url"];
//        return $temp;
//    }
//
//    return $option;
//}

function display_theme_panel_fields()
{
    add_settings_section("section", "", null, "theme-options");
    add_settings_field("logo", "Логотип", "logo_display", "theme-options", "section");
    add_settings_field("phone1", "Контактный номер телефона", "display_phone1_element", "theme-options", "section");
    add_settings_field("phone2", "Контактный номер телефона", "display_phone2_element", "theme-options", "section");
    add_settings_field("admin_email", "E-mail основной", "display_admin_email_element", "theme-options", "section");
    add_settings_field("city", "Город", "display_city_element", "theme-options", "section");
    add_settings_field("address", "Адрес", "display_address_element", "theme-options", "section");
    add_settings_field("coordinates", "Координаты", "display_coordinates_element", "theme-options", "section");
    add_settings_field("googlemaps", "Ссылка на google maps", "display_googlemaps_element", "theme-options", "section");
    add_settings_field("vkontakte_url", "Ссылка на группу Вконтакте", "display_vkontakte_element", "theme-options", "section");
    add_settings_field("facebook_url", "Ссылка на групу в Facebook", "display_facebook_element", "theme-options", "section");
    add_settings_field("pinterest_url", "Ссылка на групу в Pinterest", "display_pinterest_element", "theme-options", "section");
    add_settings_field("behance_url", "Ссылка на групу в Behance", "display_behance_element", "theme-options", "section");
    add_settings_field("admin_courses_fullname", "И.Ф. администратора курсов", "display_admin_courses_fullname", "theme-options", "section");
    add_settings_field("admin_courses_avatar", "Аватар администратора курсов", "admin_courses_avatar_display", "theme-options", "section");
//    add_settings_field("auditory", "Фото аудитории", "auditory_display", "theme-options", "section");

    register_setting("section", "logo", "handle_logo_upload");
    register_setting("section", "phone1");
    register_setting("section", "phone2");
    register_setting("section", "admin_email");
    register_setting("section", "city");
    register_setting("section", "address");
    register_setting("section", "coordinates");
    register_setting("section", "googlemaps");
    register_setting("section", "vkontakte_url");
    register_setting("section", "facebook_url");
    register_setting("section", "pinterest_url");
    register_setting("section", "behance_url");
    register_setting("section", "admin_courses_fullname");
    register_setting("section", "admin_courses_avatar", "handle_admin_courses_avatar_upload");
//    register_setting("section", "auditory", "auditory_upload");
}

add_action("admin_init", "display_theme_panel_fields");

function add_theme_menu_item()
{
    add_menu_page("Контактные данные", "Контактные данные", "manage_options", "theme-panel", "theme_settings_page", null, 99);
}

add_action("admin_menu", "add_theme_menu_item");

function add_theme_menu_advers()
{
    add_menu_page("Заявки (студенты)", "Заявки (студенты)", "manage_options", "theme-panel2", "selectAllApplicationsFromStudents", 'dashicons-carrot', 4);
}


add_action("admin_menu", "add_theme_menu_advers");

function countNewApplications()
{
    global $wpdb;
    $count_applications = $wpdb->get_var("SELECT COUNT(id) FROM wp_students_list WHERE status=0;");
    if ($count_applications)
        return $count_applications;
    else
        return 0;
}

function register_my_custom_submenu_page()
{
    add_submenu_page('theme-panel2', 'Новые заявки', 'Новые заявки (' . countNewApplications() . ')', 'manage_options', 'my-custom-submenu-page1', 'selectNewApplicationsFromStudents');
}

add_action('admin_menu', 'register_my_custom_submenu_page');

function register_my_custom_submenu_page2()
{
    add_submenu_page('theme-panel2', 'Обработанные заявки', 'Обработанные заявки', 'manage_options', 'my-custom-submenu-page2', 'selectConfirmedApplicationsFromStudents');
}

add_action('admin_menu', 'register_my_custom_submenu_page2');




function add_theme_menu_advers2()
{
    add_menu_page("Заявки (студенты)", "Заявки (преподы)", "manage_options", "theme-panel4", "selectAllApplicationsFromTeachers", 'dashicons-megaphone', 4);
}


add_action("admin_menu", "add_theme_menu_advers2");

function countNewApplications2()
{
    global $wpdb;
    $count_applications = $wpdb->get_var("SELECT COUNT(id) FROM wp_teachers_list WHERE status=0;");
    if ($count_applications)
        return $count_applications;
    else
        return 0;
}

function register_my_custom_submenu_page3()
{
    add_submenu_page('theme-panel4', 'Новые заявки', 'Новые заявки (' . countNewApplications2() . ')', 'manage_options', 'my-custom-submenu-page3', 'selectNewApplicationsFromTeachers');
}

add_action('admin_menu', 'register_my_custom_submenu_page3');

function register_my_custom_submenu_page4()
{
    add_submenu_page('theme-panel4', 'Обработанные заявки', 'Обработанные заявки', 'manage_options', 'my-custom-submenu-page4', 'selectConfirmedApplicationsFromTeachers');
}

add_action('admin_menu', 'register_my_custom_submenu_page4');

//function countNewReviews()
//{
//    global $wpdb;
//    $count_applications = $wpdb->get_var("SELECT COUNT(id) FROM wp_new_reviews WHERE status=0;");
//    if ($count_applications)
//        return $count_applications;
//    else
//        return 0;
//}
//
//function add_theme_menu_reviews()
//{
//    add_menu_page("Добавленные отзывы", "Добавленные отзывы (" .  countNewReviews() . ")", "manage_options", "theme-panel3", "showReviews", 'dashicons-editor-quote', 4);
//}
//
//add_action("admin_menu", "add_theme_menu_reviews");

function countPartners()
{
    global $wpdb;
    $count_applications = $wpdb->get_var("SELECT COUNT(id) FROM wp_partners_list;");
    if ($count_applications)
        return $count_applications;
    else
        return 0;
}

function add_theme_menu_partners()
{
    add_menu_page("Новые партнеры", "Новые партнеры (" .  countPartners() . ")", "manage_options", "theme-panel5", "showPartners", 'dashicons-phone', 4);
}

add_action("admin_menu", "add_theme_menu_partners");

/*
 * День открытых дверей
 */
function add_theme_open_doors()
{
    add_menu_page("Заявки", "День открытых дверей", "manage_options", "theme-panel99", "allParticipants", 'dashicons-groups', 1);
}

add_action("admin_menu", "add_theme_open_doors");


function register_my_custom_submenu_page_991()
{
    add_submenu_page('theme-panel99', 'Новые заявки', 'Новые заявки (' . countNewParticipants() . ')', 'manage_options', 'my-custom-submenu-page992', 'newParticipants');
}

add_action('admin_menu', 'register_my_custom_submenu_page_991');

function register_my_custom_submenu_page_99()
{
    add_submenu_page('theme-panel99', 'События', 'События', 'manage_options', 'my-custom-submenu-page991', 'manageEvents');
}

add_action('admin_menu', 'register_my_custom_submenu_page_99');

/*
 * hub
 */
function add_theme_hub()
{
    add_menu_page("Заявки", "Hub", "manage_options", "theme-panel321", "allHubOrders", 'dashicons-analytics', 2);
}

add_action("admin_menu", "add_theme_hub");

function register_my_custom_submenu_page_3211()
{
    add_submenu_page('theme-panel321', 'Новые заявки', 'Новые заявки (' . countNewHubOrders() . ')', 'manage_options', 'my-custom-submenu-page3211', 'newHubOrders');
}

add_action('admin_menu', 'register_my_custom_submenu_page_3211');

function register_my_custom_submenu_page_3212()
{
    add_submenu_page('theme-panel321', 'Подтвержденные заявки', 'Подтвержденные заявки', 'manage_options', 'my-custom-submenu-page3212', 'confirmedHubOrders');
}

add_action('admin_menu', 'register_my_custom_submenu_page_3212');

/*
 * master-classes
 */
function add_theme_mc()
{
    add_menu_page("Заявки", "Мастер-классы", "manage_options", "theme-panel_mc", "allMCOrders", 'dashicons-universal-access', 2);
}

add_action("admin_menu", "add_theme_mc");

function register_my_custom_submenu_page_mc_1()
{
    add_submenu_page('theme-panel_mc', 'Новые заявки', 'Новые заявки (' . countNewMCOrders() . ')', 'manage_options', 'my-custom-submenu-page_mc_1', 'newMCOrders');
}

add_action('admin_menu', 'register_my_custom_submenu_page_mc_1');

function register_my_custom_submenu_page_mc_2()
{
    add_submenu_page('theme-panel_mc', 'Подтвержденные заявки', 'Подтвержденные заявки', 'manage_options', 'my-custom-submenu-page_mc_2', 'confirmedMCOrders');
}

add_action('admin_menu', 'register_my_custom_submenu_page_mc_2');


