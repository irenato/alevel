<?php
/**
 * Template name: About-us page
 */

get_header();
?>

<section class="about-us">

    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2 class="title school">school<span>о школе</span></h2>
                <span class="title">A</span>
                <div class="aboutSchoolCont ">
                <div class="school aboutSchoolIn">
                    <?php $about_school = get_field('about_school'); ?>
                    <?php if ($about_school) : ?>
                        <?php foreach ($about_school as $about) : ?>
                            <p><?= $about['about_school_item'] ?></p>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 hide-xs">
                <div class="you-can">
                    <h3 class="title"><?= get_field('title_1') ?></h3>
                    <p class="text"><?= get_field('description_1') ?></p>
                    <h3 class="title"><?= get_field('title_2') ?></h3>
                    <ul>
                        <?php $subfields = get_field('description_2'); ?>
                        <?php if ($subfields) : ?>
                            <?php foreach ($subfields as $subfield) : ?>
                                <li><p><?= $subfield['description_subfield'] ?></p></li>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">
            <div class="col-lg-12 lol-md-12 col-sm-12 col-xs-12">

                <div class="chooses">
                    <h3 class="title">Присоединяйся к нашей команде</h3>
                    <div class="choose-item">
                        <img src="<?= get_template_directory_uri() ?>/images/Group.png" alt="">
                        <a class="button">стать преподом</a>
                    </div>
                    <div class="choose-item">
                        <img src="<?= get_template_directory_uri() ?>/images/icon_free_studies.jpg" alt="">
                        <a href="<?= get_page_link(206) ?>" class="button">учиться бесплатно</a>
                    </div>
                    <div class="choose-item">
                        <img src="<?= get_template_directory_uri() ?>/images/Group1.png" alt="">
                        <a class="button">стать партнером</a>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2 class="title check-in">Check in<span>записаться на курс</span></h2>
                <form action="#" class="enroll-in-a-course">
                    <p>Я,</p>
                    <div class="input">
                        <input type="text" placeholder="Имя и Фамилия" class="student-name">
                        <label for="#"></label>
                    </div>
                    <p>, хочу записаться</p>
                    <p>в <span>A</span>-Level <b>Ukraine</b> на курc</p>
                    <div class="select">
                        <select name="#" id="#" class="custom-select sources" placeholder="Хочу курс">
                            <option value="#"></option>
                           <?php createOptionsCourses(); ?>
                        </select>
                        <label for=""></label>
                    </div>
                    <p>Свяжитесь со мной по телефону</p>
                    <div class="input">
                        <input type="tel" class="phone-mask">
                        <label for="#"></label>
                    </div>
                    <p>или по e-mail</p>
                    <div class="input">
                        <input type="email" placeholder="e-mail" class="student-email">
                        <label for="#"></label>
                    </div>
                    <span class="text-danger errors-on-main-form" style="display: none;"></span>
                    <input type="submit" class="button wonna-study" value="отправить заявку">
                    <p class="min-text">*Вы принимаете политику конфиденциальности. Ваш адрес электронной почты и ФИО ни в коем случае не будут переданы другим пользователям и никогда не будут открыты для просмотра. Мы не будем передавать эти данные третьим лицам и/или любым другим способом сообщать эти данные третьим лицам.</p>
                </form>
                
                
                
                
                <form action="#" class="enroll-in-a-course-mobile">
					<div class="input">
						<input type="text" placeholder="Имя и Фамилия">
						<label for="#"></label>
					</div>
					 <div class="select">
                        <select name="#" id="#"  class="custom-select sources" placeholder="Курс">
                            <option value="#"></option>
                            <?php createOptionsCourses(); ?>
                        </select>
                        <label for=""></label>
                    </div>
                    <div class="input">
						<input type="email" placeholder="E-mail">
						<label for="#"></label>
					</div>
					<div class="input">
						<input type="tel" placeholder="Телефон">
						<label for="#"></label>
					</div>
					<input type="submit" class="button mobile-form" value="отправить">
					<p class="min-text">*Вы принимаете политику конфиденциальности. Ваш адрес электронной почты и ФИО ни в коем случае не будут переданы другим пользователям и никогда не будут открыты для просмотра. Мы не будем передавать эти данные третьим лицам и/или любым другим способом сообщать эти данные третьим лицам.</p>
				</form>
				
            </div>
        </div>
    </div>
    
    
    
    <div class="container">
        <div class="row">
            <div class=" col-xs-12 visible-xs">
                <div class="you-can mobile">
                    <h3 class="title"><?= get_field('title_1') ?></h3>
                    <p class="text"><?= get_field('description_1') ?></p>
                    <h3 class="title"><?= get_field('title_2') ?></h3>
                    <ul>
                        <?php $subfields = get_field('description_2'); ?>
                        <?php if ($subfields) : ?>
                            <?php foreach ($subfields as $subfield) : ?>
                                <li><p><?= $subfield['description_subfield'] ?></p></li>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>



    <!--    Partners -->
    <?php get_template_part( 'slider-partners' );  ?>
    
    
    <div class="container-fluid visible-xs">
        <div class="row">
            <div class=" col-xs-12 ">
                <div class="row">
                    <h2 class="title cont">contact us<span> контакты</span></h2>
                    <div class="contact ">
                        <ul>
                            <li>
                                <i class="ic-email-outline"></i>
                                <div>
                                    <h5>e-mail</h5>
                                    <a href="mailto:<?= get_option('admin_email') ?>"><?= get_option('admin_email') ?></a>
                                </div>
                            </li>
                            <li>
                                <i class="ic-phone"></i>
                                <div>
                                    <h5>телефоны</h5>
                                    <?php if (get_option('phone1')) : ?>
                                        <a href="tel:<?= get_option('phone1'); ?>"><?= get_option('phone1'); ?></a>
                                    <?php endif; ?>
                                    <?php if (get_option('phone2')) : ?>
                                        <a href="tel:<?= get_option('phone2'); ?>"><?= get_option('phone2'); ?></a>
                                    <?php endif; ?>
                                </div>
                            </li>
                            <li>
                                <i class="ic-map-marker"></i>
                                <div>
                                    <h5>Адрес</h5>
                                    <a href="https://www.google.com.ua/maps/place/43B,+%D0%B2%D1%83%D0%BB%D0%B8%D1%86%D1%8F+%D0%A1%D1%83%D0%BC%D1%81%D1%8C%D0%BA%D0%B0,+43%D0%91,+%D0%A5%D0%B0%D1%80%D0%BA%D1%96%D0%B2,+%D0%A5%D0%B0%D1%80%D0%BA%D1%96%D0%B2%D1%81%D1%8C%D0%BA%D0%B0+%D0%BE%D0%B1%D0%BB%D0%B0%D1%81%D1%82%D1%8C/@50.006387,36.2347861,18z/data=!3m1!4b1!4m5!3m4!1s0x4127a0de435d1657:0x92f43e0454946df2!8m2!3d50.0063853!4d36.2358804?hl=ru"
                                       target="_blank">ул.<?= get_option('address'); ?></a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="contact_map" class="visible-xs"></div>

</section>

<?php get_footer(); ?>
